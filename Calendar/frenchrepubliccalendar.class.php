<?php
/**
* Converti le timestamp en différents types de calendrier
*/
class FrenchRepublicCalendar {
/****************************************************************
*	   CALENDRIER REPUBLIQUE FRANCAISE							*
* Src : http://fr2.php.net/manual/fr/function.jdtofrench.php 	*
* $this->frenchrepublic(int $timestamp) return 21 Floréal CCXXII*
*****************************************************************/
	private function RepFrench_Month($mo) { #Converti en mois républicain
		$arMo = array("Vendémiaire",
						  "Brumaire",
						  "Frimaire",
						  "Nivôse",
						  "Pluviôse",
						  "Ventôse",
						  "Germinal",
						  "Floréal",
						  "Prairial",
						  "Messidor",
						  "Thermidor",
						  "Fructidor",
						  "Sansculottide") ;
		if($mo < count($arMo)+1)
			return $arMo[$mo-1] ;
	}
	private function RepFrench_decrom($dec){ # Converti en chiffres romains
		   $digits=array(
			   1 => "I",
			   4 => "IV",
			   5 => "V",
			   9 => "IX",
			   10 => "X",
			   40 => "XL",
			   50 => "L",
			   90 => "XC",
			   100 => "C",
			   400 => "CD",
			   500 => "D",
			   900 => "CM",
			   1000 => "M"
		   );
		   krsort($digits);
		   $retval="";
		   foreach($digits as $key => $value){
			   while($dec>=$key){
				   $dec-=$key;
				   $retval.=$value;
			   }
		   }
		   return $retval;
	}
		public function frenchrepublic($time) { # Converti l'unix en calendrier républicain
		$juliandate = cal_to_jd(CAL_GREGORIAN, date('m', $time), date('d', $time), date('Y', $time));
		if ($juliandate > 2380945) {
			// jdtofrench () only accepts dates until september 1806
			$gregorian_date = jdtogregorian ($juliandate);
			$temp = explode ('/', $gregorian_date);
			$year = $temp[2];
			$juliandate = gregoriantojd($temp[0], $temp[1], 1805);
			$republican_date = jdtofrench($juliandate);
			$republican_date = explode ('/', $republican_date);
			$diff = $year - 1805;
			$republican_date[2] = $republican_date[2] + $diff;
		} else {
			$republican_date = jdtofrench($juliandate);
		}
		return $republican_date[1].' '.$this->RepFrench_Month($republican_date[0]).' '.$this->RepFrench_decrom($republican_date[2]);
	}
}