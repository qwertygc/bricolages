<?php
function toromandigit($dec){
	$digits = array(1 => "I",4 => "IV",5 => "V",9 => "IX",10 => "X",40 => "XL",50 => "L",90 => "XC",100 => "C",400 => "CD",500 => "D",900 => "CM",1000 => "M");
	krsort($digits);
	$retval="";
	foreach($digits as $key => $value){
		while($dec>=$key){
			$dec-=$key;
			$retval.=$value;
		}
	}
	return $retval;
}
function roman_calendar($time) {
	// https://gist.github.com/Akshay-Srivatsan/f8e94f9d8bcc587abf208664eb8c1c7e
	$months_accusative = array('', 'Ianuarias', 'Februarias', 'Martias', 'Apriles', 'Maias', 'Iunias', 'Iulias', 'Augustas', 'Septembres', 'Octobres', 'Novembres', 'Decembres', 'Ianuarias');
	$months_ablative = array('', 'Ianuariis', 'Februariis', 'Martiis', 'Aprilibus', 'Maiis', 'Iuniis', 'Iuliis', 'Augustis', 'Septembribus', 'Octobribus', 'Novembribus', 'Decembribus', 'Ianuariis');
	$numbers_accusative = array('nihil', 'primum', 'secundum', 'tertium', 'quartum', 'quintum', 'sextum', 'septimum', 'octavum', 'nonum', 'decimum', 'undecimum', 'duodecimum', 'tertium decimum', 'quartum decimum', 'quintum decimum', 'sextum decimum', 'septimum decimum', 'duodevicesimum', 'undevicesimum', 'vicesimum');
	$year = date('Y', $time);
	$month = date('m', $time);
	$day = date('d', $time);
	$lastDayOfMonth = date("t", strtotime($year.'-'.$month.'-'.$day));
	$roman_year = toromandigit($year).' A.D.';
	$kalends = 1;
	$nones = 5;
	$ides = 13;
	if($month != 3 || $month != 5 || $month != 7 || $month != 10) {
		$nones = 7;
		$ides = 15;
	}
	if($day == $kalends) {
		return array(
				'Kalendis '.$months_ablative[$month].' '.$roman_year,
				'Kal. '.$months_ablative[$month].' '.$roman_year);
	}
	if($day > $kalends && $day <($nones -1)) {
		return array (
				'ante diem'.$numbers_accusative[$nones - $day +1].' Nonas '.$months_accusative[$month].' '.$roman_year,
				'a.d. '.toromandigit($nones - $day+1).' Non. '.$months_accusative[$month].' '.$roman_year);
	}
	if($day == $nones -1) {
		return array (
				'pridie Nonas '.$months_accusative[$month].' '.$roman_year,
				'prid. Non. '.$months_accusative[$month].' '.$roman_year);
	}
	if($day == $nones) {
		return array (
				'Nonis '.$months_ablative[$month].' '.$roman_year,
				'Non. '.$months_ablative[$month].' '.$roman_year);
	}
	 if ($day > $nones && $day < ($ides - 1)) {
		return array (
				'ante diem '.$numbers_accusative[$ides - $day +1].' Idus '.$months_accusative[$month].' '.$roman_year,
				'a.d. '.toromandigit($ides - $day +1).' Id. '.$months_accusative[$month].' '.$roman_year);
	}

	 if ($day == $ides - 1) {
        return array (
				'pridie Idus '. $months_accusative[$month].' '.$roman_year,
				'');
    }
  if ($day == $ides) {
        return array (
				'Idibus '.$months_ablative[$month].' '.$roman_year,
				'Id. '.$months_ablative[$month].' '.$roman_year);
    }
 if ($day > $ides && $day < $lastDayOfMonth) {
        return array (
				'ante diem '. $numbers_accusative[($lastDayOfMonth + 1) - $day + 1].' Kalendas '. $months_accusative[$month + 1].' '.$roman_year,
				'a.d. '.toromandigit(($lastDayOfMonth+1) - $day+1). ' Kal. '. $months_accusative[$month + 1].' '.$roman_year);
    }
    if ($day == $lastDayOfMonth) {
        return array (
				'pridie Kalendas '.$months_accusative[$month + 1].' '.$roman_year,
				'prid. Kal. '.$months_accusative[$month + 1].' '.$roman_year);
    }
}
