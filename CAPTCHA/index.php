<?php
session_start();
function stripAccents($string) {
	$string = html_entity_decode($string);
	$string = strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY-');
	return $string;
}
function captcha() {
	$array = array(
		"Quelle est la capitale de l'Italie ?"=>"rome",
		"Qui a écrit Candide ?" => "voltaire",
		"Qui a écrit L’Étranger ? (nom de famille)" => "camus",
		"Qui a écrit Madame Bovary ? (nom de famille)" => "Flaubert",
		"Qui a écrit 1984 ? (nom de famille)" => "Orwell",
		"Qui a écrit À la recherche du temps perdu ? (nom de famille)" => "Proust",
		"Qui a écrit Hamlet ? (nom de famille)" => "Shakespeare",
		"Qui a écrit Le Rouge et le Noir ? (nom de famille)" => "Stendhal",
		"Qui a écrit Guerre et Paix ? (nom de famille)" => "Tolstoï",
		"Qui a écrit L’Iiade et l'Odyssée ?" => "Homère",
		"Qui a écrit L’Énéide ?" => "Virgile",
		"Combien possède l’UE de membres en 2014 ?" =>"28", 
		"Sous quel nom est connu Jean-Pière Poquelin ?" =>"Molière",
		"Sous quel nom est connu François-Marie Arouet ?" =>"Voltaire",
		"Quelle est l’année de la signature du traité de Maastricht concernant la construction de l’Union Européenne ?" =>"1992",
		"Quelle est l’année de la signature du traité de Rome concernant la construction de la Communauté économique européenne ?" =>"1957",
		"Quelle est l’année de la chute du mur de Berlin ?" =>"1989",
		"Quelle est la réponse à la vie, l’univers et tout le reste ?" =>"42",
		"En quel année fut guillotiné Louis XVI ?" =>"1793",
		"De quelle dynastie fut issue Louis XVI ?" =>"bourbons",
		"Qui pense que « science sans conscience n’est que ruine de l’âme » ? (nom de famille)" =>"Rabelais",
		"Qui pense que « Rien ne se perd, rien ne se crée, tout se transforme » ? (nom de famille)" =>"Lavoisier",
		"Qui décrit la démocratie comme « le gouvernement du peuple, par le peuple et pour le peuple » ? (nom de famille)" =>"Lincoln",
		"Dans la mythologie grecque, qui était le dieu de la prudence guerrière ?" =>"athena",
		"Dans la mythologie romaine, qui était le dieu de la prudence guerrière ?" =>"minerve",
		"Dans la mythologie grecque, qui était le dieu des morts ?" =>"hades",
		"Dans la mythologie romaine, qui était le dieu des morts ?" =>"pluton",
		"Dans la mythologie grecque, qui était le dieu des mers et des tempêtes ?" =>"Poseidon",
		"Dans la mythologie romaine, qui était le dieu des mers et des tempêtes ?" =>"neptune",
		"Dans la mythologie grecque, qui a donné le feu, et donc la connaissance, aux hommes ?" =>"Promethee",
	);
	$_SESSION['captcha'] = array();
	$_SESSION['captcha']['question'] = array_rand($array);
	$_SESSION['captcha']['answer'] = $array[$_SESSION['captcha']['question']];
	return 	$_SESSION['captcha']['question'];
	
}
	if(isset($_POST['captcha'])) {
		if(stripAccents(strtolower(trim($_POST['captcha']))) == stripAccents(strtolower(trim($_SESSION['captcha']['answer'])))) {
			echo "Vous pouvez poster";
		}
		else {
			echo "bot spotted";
		}
		unset($_SESSION['captcha']);
	}
?>
<form method="post">
<label for="captcha"><?php echo captcha(); ?></label><input type="text" name="captcha">
<input type="submit" name="submit">
</form>
