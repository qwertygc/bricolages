<?php
function str2hex($string) {
  $hex = "";
  for ($i = 0; $i < strlen($string); $i++) {
    $hex .= (strlen(dechex(ord($string[$i]))) < 2) ?
    "0" . dechex(ord($string[$i])) : dechex(ord($string[$i]));
  }
  return $hex;
}
function hexaValue($string){
		return substr(str2hex($string), 0, 6);
    }
function randomcolor() {
	$input = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");
	$rand_keys = array_rand($input, 6);
	return $input[$rand_keys[0]].$input[$rand_keys[1]].$input[$rand_keys[2]].$input[$rand_keys[3]].$input[$rand_keys[4]].$input[$rand_keys[5]];
}
function aff($item) {
	return '<a href="?h='.$item.'"><span class="color-sample"style="background:#'.$item.'"></span></a>';
}
function text($t) {
	// $t = trim(preg_replace(array('~[:;!?]|[.,](?![0-9])|\'s~', '~\s+~'), array('', ' '), $t));
	$tag = explode(' ', $t);
	$nombre_tag = count ($tag);
	for ($numero = 0; $numero < $nombre_tag; $numero++) {
		$tag[$numero];
	}
	return $tag;
}
?>
<html>
<head>
<style>
.color-sample {
width:100px; 
height:100px;
margin:0px;
float:left;
text-align:center;
}
</style>
</head>
<body>
<a href="?source=1">Voir source</a>
 <a href="javascript:window.history.back()"><</a>
<form method="post">
<textarea name="t"></textarea>
<input type="submit"/>
</form>
<?php
if(isset($_POST['t'])) {
	foreach(array_unique(text($_POST['t'])) as $t) {
		if($t !="0" AND (strlen($t) == 6)) {
			echo aff(hexaValue($t));
		}
	}
}
if(isset($_GET['h'])) {
	echo '<style>.color-sample {background:#'.$_GET['h'].';height: 100vh; line-height: 100vh; width:100vh;}</style>';
	echo '<span class="color-sample">#'.$_GET['h'].'</p>';
}
if (isset($_GET['source']))
{
    highlight_string(file_get_contents($_SERVER['SCRIPT_FILENAME']));
    exit();
}
?>
</body>
</html>