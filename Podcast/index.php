<?php
if(!isset($_GET['iframe'])) {
	error_reporting(E_ALL);
}
else {
	error_reporting(0);
}
session_start();
$itunescat = [
	'',
	'Arts',
	'Business',
	'Comedy',
	'Education',
	'Fiction',
	'Government',
	'History',
	'Health & Fitness',
	'Kids & Family',
	'Leisure',
	'Music',
	'News',
	'Religion & Spirituality',
	'Science',
	'Society & Culture',
	'Sports',
	'Technology',
	'True Crime',
	'TV & Film'
];
$transcript_format = [
	'text/plain',
	'text/html',
	'application/srt',
	'application/json'
];
function redirect($url) {
	echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	die;
}

if(file_exists('config.php')) {
	include 'config.php';
}
else {
	define('XMLFILE', 'podcast.xml');
	define('PWD', 'demo');
	define('DIR', 'data/');
	define('ROOT', 'http://localhost:8000/data/');
}
?>  

<!doctype html>
<html lang="fr">
	<head>
	<title>Podcast Editor</title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css" integrity="sha256-gsmEoJAws/Kd3CjuOQzLie5Q3yshhvmo7YNtBG7aaEY=" crossorigin="anonymous">
	<style>
	label {
	display:block;
	}
	fielset {
		border: 1px solid #333;
	}
	</style>
	</head>
	<body class="container-fluid">
	<?php if(!isset($_GET['iframe'])): ?>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		 <ul class="navbar-nav mr-auto">
		 <?php
		 	echo '<li class="nav-item"><a class="navbar-brand" href="?">Podcast</a></li>';
		 	if(isset($_SESSION['login']) AND $_SESSION['login'] == true) {
			 	echo '<li class="nav-item"><a class="nav-link" href="?do=list">Gestion des épisodes</a></li>';
			 	echo '<li class="nav-item"><a class="nav-link" href="?do=files">Gestion des fichiers</a></li>';
				echo '<li class="nav-item"><a class="nav-link" href="?do=global">Configuration</a></li>';
				echo '<li class="nav-item"><a class="nav-link" href="?do=doc">Documentation</a></li>';
				echo '<li class="nav-item"><a class="nav-link" href="?do=login&logout=true">Déconnexion</a></li>';
			}
			else {
				echo '<li class="nav-item"><a class="nav-link" href="?do=login">Connexion</a></li>';
			}
		?>
		</ul>
	</nav>
	<?php endif; ?>
<?php
if(!file_exists(DIR)) {mkdir(DIR);}
if(!file_exists(XMLFILE)) {copy('podcast-sample.xml', XMLFILE);}
$xml =simplexml_load_file(XMLFILE);
$itunes = $xml->registerXPathNamespace('itunes', 'http://www.itunes.com/dtds/podcast-1.0.dtd');
$podcastindex = $xml->registerXPathNamespace('podcast', 'https://github.com/Podcastindex-org/podcast-namespace/blob/main/docs/1.0.md');
$googleplay = $xml->registerXPathNamespace('googleplay', 'http://www.google.com/schemas/play-podcasts/1.0');

if(isset($_GET['do'])) {
	if(in_array($_GET['do'], array('global', 'list', 'add', 'doc', 'files'))) {
		if($_SESSION['login'] == true) {
		}
		else {
			redirect('index.php');
		}
	}
	switch($_GET['do']) {
		case 'global':
			$global_title = $xml->channel->title;
			$global_description = $xml->channel->description;
			$global_language = $xml->channel->language;
			$global_link = $xml->channel->link;
			$global_image = $xml->xpath("//itunes:image")[0]['href'];
			$global_author = $xml->xpath("//itunes:author")[0];
			$global_email = $xml->xpath("//itunes:owner/itunes:email")[0];
			$global_cat =  $xml->xpath("//itunes:category")[0]['text'];
			$global_explicit =  ($xml->xpath("//itunes:explicit")[0] == 'true') ? 'checked' : '';
			$global_locked = ($xml->xpath("//podcast:locked")[0] == 'yes') ? 'checked' : '';
			$global_funding = [
				'url' 	=> $xml->xpath("//podcast:funding")[0]['url'],
				'text'	=> $xml->xpath("//podcast:funding")[0]
			];
			echo '<form method="post" action="?do=global" class="form-group">
			<label for="gtitle">
				Titre du podcast <sup style="color:red">*</sup>
				<input type="text" name="gtitle" id="gtitle" value="'.$global_title.'" class="form-control">
			</label>
			<label for="gdesc">
				Description du podcast <sup style="color:red">*</sup>
				<textarea name="gdesc" id="gdesc" class="form-control">'.$global_description.'</textarea>
			</label>
			<label for="gcat">
				Catégorie du podcast <sup style="color:red">*</sup>
				 <select class="form-control" id="gcat" name="gcat">';
				 foreach($itunescat as $cat) {
				 	$selected = ($cat == $global_cat) ? 'selected' : '';
				 	echo '<option value="'.$cat.'" '.$selected.'>'.$cat.'</option>';
				 }
			echo'	</select>
			</label>
			<label for="glang">
				Langue du podcast <sup style="color:red">*</sup>
				<input class="form-control" type="text" name="glang" id="glang" value="'.$global_language.'">
				<small class="form-text text-muted">format <a href="https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-2">ISO 639-2</a></small>
			</label>
			<label for="glink">
				Lien du podcast<sup style="color:red">*</sup>
				<input type="text" name="glink" id="glink"value="'.$global_link.'" class="form-control">
			</label>
			<label for="gimg">
				Couverture du Podcast <sup style="color:red">*</sup>
				<input type="text" name="gimg" id="gimg"value="'.$global_image.'"class="form-control">
				<small class="form-text text-muted">min: 1400 x 1400px max 3000 x 3000 px</small>
			</label>
			<label for="gauthor">
				Auteur<sup style="color:red">*</sup>
				<input type="text" name="gauthor" id="gauthor"value="'.$global_author.'" class="form-control">
			</label>
			<label for="gemail">
				Courriel<sup style="color:red">*</sup>
				<input type="email" name="gemail" id="gemail"value="'.$global_email.'" class="form-control">
			</label>
			<label for="gexplicit">
				<input type="checkbox" id="gexplicit" name="gexplicit" '.$global_explicit.'>
				Le Podcast contient un contenu/langage explicite ou « pour adulte »
			</label>
			<label for="glocked">
				<input type="checkbox" id="glocked" name="glocked" '.$global_locked.'>
				Verrouiller le Podcast<sup style="color:red">*</sup>
			</label>
			<fieldset class="form-group">
				<legend>Financement</legend>
				<label for="gfundurl">
					Adresse pour le financement
					<input type="url" name="gfundurl" id="gfundurl" value="'.$global_funding['url'].'" class="form-control">
				</label>
				<label for="gfundtext">
					Texte pour le financement
					<input type="text" name="gfundtext" id="gfundtext" value="'.$global_funding['text'].'" class="form-control">
				</label>
			</fieldset>
			<input type="submit" class="btn btn-primary mb-2"/>
			</form>';
			if(isset($_POST['gtitle'])) {
				$xml->channel->title = $_POST['gtitle'];
				$xml->channel->description = $_POST['gdesc'];
				$xml->channel->language = $_POST['glang'];
				$xml->channel->link = $_POST['glink'];
				$xml->xpath("//itunes:image")[0]['href'] = $_POST['gimg'];
				$xml->xpath("//googleplay:image")[0]['href'] = $_POST['gimg'];
				$xml->xpath("//itunes:author")[0][0] = $_POST['gauthor'];
				$xml->xpath("//itunes:category")[0]['text'] = $_POST['gcat'];
				$xml->xpath("//googleplay:category")[0]['text'] = $_POST['gcat'];
				$xml->xpath("//itunes:owner/itunes:name")[0][0] = $_POST['gauthor'];
				$xml->xpath("//itunes:owner/itunes:email")[0][0] = $_POST['gemail'];
				$xml->xpath("//itunes:explicit")[0][0] = (isset($_POST['gexplicit'])) ? 'yes': 'no';
				$xml->xpath("//googleplay:explicit")[0][0] = (isset($_POST['gexplicit'])) ? 'yes': 'no';
				$xml->xpath("//itunes:block")[0][0] = (isset($_POST['glocked'])) ? 'yes': 'no';
				$xml->xpath("//googleplay:block")[0][0] = (isset($_POST['glocked'])) ? 'yes': 'no';
				$xml->xpath("//podcast:locked")[0][0] = (isset($_POST['glocked'])) ? 'yes': 'no';
				$xml->xpath("//podcast:locked")[0]['owner'] = $_POST['gemail'];
				$xml->xpath("//podcast:funding")[0]['url'] = $_POST['gfundurl'];
				$xml->xpath("//podcast:funding")[0][0] = $_POST['gfundtext'];
				$xml->asXML(XMLFILE);
				redirect('index.php?do=global');
			}
		break;
		case 'add':
		if(isset($_GET['edit'])) {
			$id = (int)$_GET['edit'];
			$entry = $xml->channel->item[$id];
			$title = $entry->title;
			$desc =  $entry->description;
			$date = date('Y-m-d', strtotime($entry->pubDate));
			$link = $entry->link;
			$image = $entry->xpath("//itunes:image")[$id+1]['href'];
			$durationraw = (int)$entry->children('itunes', true)->duration;
			$h = floor($durationraw/ 3600);
			$m = floor($durationraw / 60 - $h * 60);
			$s = floor($durationraw - $h - ($m*60));
			$duration = ['h' => $h, 'm' => $m, 's' => $s];
			$enclosure = [
				'mimetype' 	=>	$entry->enclosure->attributes()->type,
				'length'	=>	$entry->enclosure->attributes()->length,
				'url'		=>	$entry->enclosure->attributes()->url
			];
			$transcript = [
				'url' 	=> $entry->xpath("//podcast:transcript")[$id]['url'],
				'type' 	=> $entry->xpath("//podcast:transcript")[$id]['type'],
			];
			$chapters = $entry->xpath("//podcast:chapters")[$id]['url'];
		}
		else {
			$id = -1;
			$title = '';
			$desc = '';
			$date = date('Y-m-d', time());
			$link = '';
			$image =  $xml->xpath("//itunes:image")[0]['href'];
			$duration = ['raw' =>0, 'h'=>0, 'm'=>0, 's'=>0];
			$enclosure = [
				'mimetype' 	=> 'audio/mpeg',
				'length'	=> 0,
				'url'		=> ''
			];
			$transcript = ['url' => '', 'type' => 'application/srt'];
			$chapters = '';
		}
		echo '<form method="post" action="?do=add" class="form-group">
			<label for="ptitle">
				Titre de l’épisode<sup style="color:red">*</sup>
				<input type="text" name="ptitle" id="ptitle" value="'.$title.'" class="form-control">
			</label>
			<label for="pdesc">
				Description de l’épisode<sup style="color:red">*</sup>
				<textarea name="pdesc" id="pdesc" class="form-control">'.$desc.'</textarea>
			</label>
			<label for="pdate">
				Date de publication<sup style="color:red">*</sup>
				<input type="date" name="pdate" id="pdate" value="'.$date.'" class="form-control">
			</label>
			<label for="plink">
				Lien de l’épisode<sup style="color:red">*</sup>
				<input type="url" name="plink" id="plink" value="'.$link.'" class="form-control">
			</label>
			<label for="pchap">
				Chapitrage
				<input type="url" name="pchap" id="pchap" value="'.$chapters.'" class="form-control">
				<small class="form-text text-muted">Fichier <a href="https://github.com/Podcastindex-org/podcast-namespace/blob/main/chapters/jsonChapters.md">JSON</a></small>
			</label>
			<label for="pimg">
				Couverture du Podcast<sup style="color:red">*</sup>
				<input type="text" name="pimg" id="pimg" value="'.$image.'" class="form-control">
				<small class="form-text text-muted">min: 1400 x 1400px max 3000 x 3000 px</small>
			</label>
			<fieldset class="form-group">
				<legend>Fichier audio</legend>
				<label for="paudiohref">
					Lien vers le fichier audio<sup style="color:red">*</sup>
					<input type="url" name="paudiohref" id="paudiohref" value="'.$enclosure['url'].'" class="form-control">
				</label>
				<label for="paudiosize">
					Poids du fichier audio<sup style="color:red">*</sup>
					<input type="number" name="paudiosize" id="paudiosize" value="'.$enclosure['length'].'" class="form-control">
					<small class="form-text text-muted">En octets</small>
				</label>
				<label for="paudiotype">
					Mime type du fichier audio<sup style="color:red">*</sup>
					<input type="text" name="paudiotype" id="paudiotype" value="'.$enclosure['mimetype'].'" class="form-control">
					</label>
				<label for="paudioduration">
					Durée du fichier audio<sup style="color:red">*</sup>
					<div class="row">
						<div class="col">HH<input type="number" name="paudioduration_h" id="paudioduration_h" value="'.$duration['h'].'" class="form-control"></div>
						<div class="col">MM<input type="number" name="paudioduration_m" id="paudioduration_m" value="'.$duration['m'].'" class="form-control" min="0" max="59"></div>
						<div class="col">SS<input type="number" name="paudioduration_s" id="paudioduration_s" value="'.$duration['s'].'" class="form-control" min="0" max="59"></div>
					</div>
					<small class="form-text text-muted">En secondes</small>
				</label>
			</fieldset>
			<fieldset class="form-group">
				<legend>Transcription</legend>
				<label for="paudiotype">
					Format de la retranscription
					<select class="form-control" id="transcript_type" name="transcript_type">';
					 foreach($transcript_format as $format) {
					 	$selected = ($format == $transcript['type']) ? 'selected' : '';
					 	echo '<option value="'.$format.'" '.$selected.'>'.$format.'</option>';
					 }
			echo	'</select>
					</label>
					<label for="paudiotype">
					URL de la retranscription
					<input type="url" name="transcript_url" id="transcript_url" value="'.$transcript['url'].'" class="form-control">
					</label>
			</fieldset>
			<input type="hidden" name="id" id="id" value="'.$id.'"/>
			<input type="submit" class="btn btn-primary mb-2"/>
			</form>';
			if(isset($_POST['ptitle'])) {
				$id = (int)$_POST['id'];
				$_POST['pchap'] = isset($_POST['pchap']) ? $_POST['pchap'] : '';
				if($id >= 0) {
					$item = $xml->channel->item[$id];
					$item->title = $_POST['ptitle'];
					$item->enclosure->attributes()->type = $_POST['paudiotype'];
					$item->enclosure->attributes()->length = $_POST['paudiosize'];
					$item->enclosure->attributes()->url = $_POST['paudiohref'];
					$item->guid = $_POST['plink'];
					$item->xpath("//itunes:image")[$id+1]['href'] = $_POST['pimg'];
					$item->pubDate = date('D, d M Y H:i:s', strtotime($_POST['pdate']));
					$item->description = $_POST['pdesc'];
					$item->children('itunes', true)->duration = (int)$_POST['paudioduration_h'] * 3600 + (int)$_POST['paudioduration_m'] * 60 + (int)$_POST['paudioduration_s'];
					$item->link = $_POST['plink'];
					$item->children('podcast', true)->chapters[$id+1]['url'] = $_POST['pchap'];
					$item->children('podcast', true)->chapters[$id+1]['type'] = 'application/json+chapters';
					$item->children('podcast', true)->transcript[$id+1]['url'] = $_POST['transcript_url'];
					$item->children('podcast', true)->transcript[$id+1]['type'] = $_POST['transcript_type'];
					$xml->asXML(XMLFILE);
					redirect('index.php?do=list');
				}
				else {
					$item = $xml->channel->addChild('item','');
					$item->addChild('title',$_POST['ptitle']);
					$enclosure = $item->addChild('enclosure','');
					$enclosure->addAttribute('length',$_POST['paudiosize']);
					$enclosure->addAttribute('type',$_POST['paudiotype']);
					$enclosure->addAttribute('url',$_POST['paudiohref']);
					$image = $item->addChild('itunes:image', '', $itunes);
					$image->addAttribute('href',$_POST['pimg']);
					$item->addChild('guid',$_POST['plink']);
					$item->addChild('pubDate',date('D, d M Y H:i:s', strtotime($_POST['pdate'])));
					$item->addChild('description',$_POST['pdesc']);
					$item->addChild('itunes:duration', ((int)$_POST['paudioduration_h'] * 3600 + (int)$_POST['paudioduration_m'] * 60 + (int)$_POST['paudioduration_s']), $itunes);
					$item->addChild('link',$_POST['plink']);
					$item->addChild('itunes:explicit', 'false', $itunes);
					$item->addChild('googleplay:explicit', 'false', $itunes);
					$chapters = $item->addChild('podcast:chapters', '');
					$chapters->addAttribute('url', $_POST['pchap']);
					$chapters->addAttribute('type', 'application/json+chapters');
					$transcript = $item->addChild('podcast:transcript', '');
					$transcript->addAttribute('url', $_POST['transcript_url']);
					$transcript->addAttribute('type', $_POST['transcript_type']);
					$item->removeAttributeNS('1', 'xmlns:itunes');
					$item->removeAttributeNS('1', 'xmlns:googleplay');
					$xml->asXML(XMLFILE);
					redirect('index.php?do=list');
				}
			}
			if(isset($_GET['del'])) {
				$id = (int)$_GET['del'];
				unset($xml->channel->item[$id]);
				$xml->asXML(XMLFILE);
				redirect('index.php?do=list');
			}
		break;
		case 'list':
			$id = 0;
			echo '<ul class="list-group container">';
			echo '<li class="list-group-item"><a href="?do=add"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter un épisode</a></li>';
			foreach($xml->channel->item as $key => $item) {
				echo '<li class="list-group-item">
					'.$item->title.'
					<small>'.date('Y-m-d', strtotime($item->pubDate)).'</small>
					<span class="float-right">
						<a href="?do=add&edit='.$id.'"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						<a href="?do=add&del='.$id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>
					</span>
				</li>';
				$id +=1;
			}
			echo '</ul>';
		break;
		case 'doc':
			echo '<p>Derrière chaque podcast, il y a un flux RSS (un petit fichier texte au format XML). Les agrégateurs, que cela soit une application (comme <a href="http://antennapod.org/">AntennaPod</a>) ou une plateforme (comme Itunes), ne font que compiler les liens des différents flux RSS.<p>
			<ul>
				<li><a href="https://framagit.org/qwertygc/bricolages/-/tree/master/Alexandra">Code source du logiciel</a></li>
				<li>Spécifications techniques (<a href="https://help.apple.com/itc/podcasts_connect/">Itunes</a>, <a href="https://github.com/Podcastindex-org/podcast-namespace/blob/main/docs/1.0.md">PodcastIndex.org</a>,  <a href="https://support.google.com/podcast-publishers/answer/9889544">Google</a>)</li>
				<li>Faire référencer son podcast : <a href="http://podcastsconnect.apple.com/">Itunes</a>, <a href="https://podcastsmanager.google.com/">Google Podcasts</a>, <a href="https://podcasters.spotify.com/">Spotify</a>, <a href="https://podcasters.deezer.com/submission">Deezer</a>.</li>
				</ul>';
		break;
		case 'login':
		echo '<form method="post" action="?do=login" class="form-group">
			<input type="password" id="password" name="password" class="form-control"/>
			<input type="submit"/>
			</form>';
			if (isset($_POST['password'])) {
				if ($_POST['password'] == PWD) {
				    $_SESSION['login'] = true;
				    echo 'True';
				    redirect('index.php?do=list');
				} else {
				    $_SESSION['login'] = false;
				    echo '<div class="alert alert-warning" role="alert">Erreur : mauvais mot de passe</div>';
				}
			}
		if (isset($_GET['logout'])) {
		    unset($_SESSION['login']);
		    session_destroy();
		     redirect('index.php');
		    exit();
    	}

		break;
		case 'files':
			echo '<form method="post" enctype="multipart/form-data" action="?do=files">
				<input type="file" name="file[]" multiple="multiple" class="form-controle-file">
				<input type="submit" name="upload" class="btn btn-primary btn-sm">
				</form>';
			if (isset($_POST['upload'])) {
				$files = array();
				$fdata = $_FILES['file'];
				$count = count($fdata['name']);
				if (is_array($fdata['name'])) {
					for ($i = 0;$i < $count;++$i) {
						$files[] = array(    'name' => date('Y-m-d_his', time()) . '_' . $fdata['name'][$i], 'tmp_name' => $fdata['tmp_name'][$i], 'type' => mime_content_type($fdata['tmp_name'][$i]));
					}
				} else {
					$files[] = $fdata;
				}
				foreach ($files as $file) {
					if (!is_uploaded_file($file['tmp_name'])) {
						exit('<div class="block">' . 'Impossible de téléverser le fichier ' . '</div>');
					}
					if (!move_uploaded_file($file['tmp_name'], DIR . $file['name'])) {
						exit('<div class="block">' . 'Impossible de téléverser le fichier' . '</div>');
					}
					redirect("?do=files");
				}
			}
			if(isset($_GET['del'])) {
				if(file_exists(DIR.$_GET['del'])) {
					unlink(DIR.$_GET['del']);
					redirect('?do=files');
				}
			}
			foreach (scandir(DIR) as $content) {
				if (!in_array($content, array('.', '..'))) {
					$file = DIR.$content;
					echo '<div class="card">
					 		<div class="card-body">
					 		<h5 class="card-title">'.$content.'</h5>
					 			<a href="'. $file . '" class="btn btn-primary" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
								<a href="?do=files&del=' .$content . '" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
					 			<input onclick="this.select()" class="form-control" value="' . ROOT.$content. '"/>
					 			 <p class="card-text"><b>Poids</b> : '.filesize($file).'</p>
					 			 <p class="card-text"><b>Mime Type</b> : '.mime_content_type($file).'</p>
							</div>
				   		</div>';
				}
			}
		break;
	}
}
else {
	echo '<ul class="list-group">';
	echo '<li class="list-group-item">
		<img src="'.$xml->xpath("//itunes:image")[0]['href'].'" class="float-left" style="width:250px; height:250px; margin-right: 1em;"/>
		<h2>'.$xml->channel->title.'</h2>
		<adress>
			'.$xml->xpath("//itunes:author")[0].'
			<a href="mailto:'.$xml->xpath("//itunes:owner/itunes:email")[0][0].'"><i class="fa fa-envelope" aria-hidden="true"></i></a>
		</adress>
		<div class="inline-flex">
			<a href="'.$xml->channel->link.'" class="btn btn-outline-secondary"><i class="fa fa-link" aria-hidden="true"></i></a>
			<a href="'.XMLFILE.'" class="btn btn-outline-secondary"><i class="fa fa-rss" aria-hidden="true"></i></a>
			<a href="'.$xml->xpath("//podcast:funding")[0]['url'].'" class="btn btn-outline-secondary">'.$xml->xpath("//podcast:funding")[0][0].'</a>
		</div>
		<p>'.$xml->channel->description.'</p>
		<p class="float-right"></p>
		</li>';

	$id = 0;
	foreach($xml->channel->item as $key => $item) {
		echo '<li class="list-group-item">
		<img src="'.$item->xpath("//itunes:image")[$id+1]['href'].'" class="float-left" style="width:100px; height:100px; margin-right: 1em;"/>
		<h6><a href="'.$item->link.'">'.$item->title.'</a></h6>
		<small>'.date('Y-m-d', strtotime($item->pubDate)).' ⋅ '.round($item->children('itunes', true)->duration/60).'mn</small><br>
		<p>'.$item->description.'</p>
		 <audio controls>
		 	<source src="'.$item->enclosure->attributes()->url.'" type="'.$item->enclosure->attributes()->type.'">
		 	<transcript>
		 		'.$item->children('podcast', true)->transcript->attributes()->url.'
		 	</transcript>
		 </audio> 
		</li>';
		$id =+ 1;
	}
	echo '</ul>';
}
?>
</body>
</html>

