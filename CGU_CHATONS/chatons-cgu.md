Nos CGU et mentions légales, parce qu’il en faut bien !
		Mentions légales
		CHATONS est un site édité à titre non-professionnel.

		Hébergement et responsable d’édition
		30 rue des roses
		38000 Grenoble

		Rectification des informations
		En cas de réclamation sur le contenu de ce site, vous pouvez 
contacter l’éditeur par courrier électronique, par le formulaire de 
contact dédié. Dans l’éventualité ou nous ne donnerions pas suite à vos 
sollicitations, la loi vous permet de vous adresser directement à notre 
hébergeur. Néanmoins, nous vous rappelons l’Article 6, I, 4° de la loi 
2004-575 du 21 juin 2004, afin d’éviter tout abus : « Le fait, pour 
toute personne, de présenter aux [hébergeurs du site] un contenu ou une 
activité comme étant illicite dans le but d’en obtenir le retrait ou 
d’en faire cesser la diffusion, alors qu’elle sait cette information 
inexacte, est puni d’une peine d’un an d’emprisonnement et de 15 000 EUR
 d’amende. »

		
		Contrat social
		Dans le cadre de l’utilisation nous serveur, nous voulons instituer
 un « contrat social » entre les différents membres du service. Ces 
principes, simples, permettent de garantir un bon fonctionnement. Pour 
faire simple : on fait du mieux que l’on peut, mais la qualité du 
service dépends chacun de vous ! Le serveur étant autogéré, chacun est 
responsable et peut mettre la main à la pâte pour le rendre meilleur !

		Une obligation de moyen : Les services sont soumis à une 
obligation de moyens : on déploiera tout nos efforts pour atteindre une 
bonne qualité de service, sans garantie d’une interruption de service ou
 de pertes de données : l’utilisation étant fourni telle quelle, il se 
fait à vos propres risques. En cas d’interruption de service définitif, 
chaque utilisateur sera prévenu par le biais d’un courriel et d’un 
message sur cette présente page. Tout utilisateur est responsable de ces
 données : vous devez gérer la sécurité et la sauvegarde de ses 
dernières. Vous êtes responsable de la sécurité de vos identifiants et 
mots de passe.
		
Vie privée : La législation française impose une 
conservation de l’adresse IP et de la date de connexion ou d’utilisation
 du serveur pendant un an.

		Respect des lois en vigueur et des autres membres : chaque 
adhérent devra se conformer à la législation française. Tout contenu à 
caractère injurieux, diffamatoire, xénophobe, raciste, antisémite, 
pornographique, révisionniste, homophobe, sexiste, d’incitation à la 
haine, à la violence, violant le droit d’auteur, ou en général tout 
sujet contraire à la loi et aux valeurs humanistes n’est pas accepté. 
Les restrictions peuvent aussi être d’ordre technique ou relatives à un 
abus de services. Il est demandé une utilisation raisonnable des 
services pour ne pas nuire aux autres utilisateurs. Le spam est 
strictement interdit. Le gestionnaire peut, à sa discrétion, résilier à 
tout moment un compte s’il juge que cela peut poser problème.

		Participation universelle : chaque membre devra participer, à
 sa manière et à la hauteur de ses possibilités, au bon fonctionnement 
du collectif. La réussite générale dépend de l’implication du plus petit
 dénominateur commun. Il devra faire preuve d’esprit de camaraderie lors
 des discussions avec le reste du groupe, et ne pas avoir une attitude 
nocive et discriminante envers les autres. Le service est à prix libre. 
En cas d’utilisation jugée importante, il pourra être demandé une 
contribution financière, matérielle ou logistique.