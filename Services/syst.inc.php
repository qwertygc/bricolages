<?php
try {
	$bdd = new PDO('mysql:host='.URL_DB .';dbname='.TABLE, LOGIN, PASSWORD);
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
		echo "<p>Erreur : " . $e->getMessage() . "</p>";
		exit();
}
// include("rain.tpl.class.php");
// $tpl = New RainTPL();
// raintpl::configure("tpl_dir", "tpl/");
