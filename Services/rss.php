<?php
header("Content-type: application/xml");
include("config.inc.php");
include("syst.inc.php");
// $url= dirname("http://". $_SERVER['HTTP_HOST']. $_SERVER["PHP_SELF"]. "/");
$sql = $bdd->query("SELECT * FROM `services` WHERE `statut` = '1'");
if(DEBUG == true) { $querycount+=1;}
$rss ='<?xml version="1.0" encoding="utf-8"?>'. "\n";
$rss.='<rss version="2.0">'. "\n";
$rss.='<channel>'."\n";
$rss.='<title>Les services</title>'. "\n";
//$rss.='<description><![CDATA['.DESCRIPTION. ']]></description>'. "\n"; Facultatif
$rss.='<description>Tout les services du quartier</description>'. "\n";
$rss.='<link>'. ROOT. '</link>'. "\n";
$sql->setFetchMode(PDO::FETCH_BOTH);
while($donnees = $sql->fetch()) {
if($donnees['typeofservice'] == "0") { $typeofservice = "proposition"; } else { $typeofservice = "demande";}
		$rss.='<item>'. "\n";
		$rss.='<title>'.$typeofservice. '</title>'. "\n"; 
		$rss.='<link>http://nantbruyantsolidaire.free.fr/services/</link>'. "\n";
		$rss.='<description>'.htmlspecialchars(nl2br($donnees['service'])). '</description>'. "\n";
		$rss.='</item>'. "\n";
}
$rss.='</channel>'. "\n";
$rss.='</rss>';
echo  stripslashes($rss);

?>