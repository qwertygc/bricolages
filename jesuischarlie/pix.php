<?php
function calculateTextBox($text,$fontFile,$fontSize,$fontAngle) { // pour centrer ($imagewidth/2)-($textwidth/2)-2;
    $rect = imagettfbbox($fontSize,$fontAngle,$fontFile,$text);
    $minX = min(array($rect[0],$rect[2],$rect[4],$rect[6]));
    $maxX = max(array($rect[0],$rect[2],$rect[4],$rect[6]));
    $minY = min(array($rect[1],$rect[3],$rect[5],$rect[7]));
    $maxY = max(array($rect[1],$rect[3],$rect[5],$rect[7]));
    return array(
     "left"   => abs($minX) - 1,
     "top"    => abs($minY) - 1,
     "width"  => $maxX - $minX,
     "height" => $maxY - $minY,
     "box"    => $rect
    );
}
header('Content-Disposition: Attachment;filename=picture.png'); 
header('Content-Type: image/png');
$im = imagecreatetruecolor(520, 482);
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 211, 215, 218);
$black = imagecolorallocate($im, 0, 0, 0);
imagefilledrectangle($im, 0, 0, 399, 29, $black);
$text_jesuis = 'JE SUIS';
$font_jesuis = './arial-black.ttf';
$font_charlie = './ufonts.com_block-berthold-condensed.ttf';
$text_charlie = $_GET['t'];
$jesuis = calculateTextBox(strtoupper($text_jesuis), $font_jesuis, 75, 0);
imagettftext($im, 75, 0, (260-($jesuis['width']/2)-2), 200, $white, $font_jesuis, $text_jesuis);

$charlie = calculateTextBox(strtoupper($text_charlie), $font_charlie, 120, 0);
imagettftext($im,  120, 0, (260-($charlie['width']/2)-2), 340, $grey, $font_charlie, strtoupper($text_charlie));
imagepng($im);
imagedestroy($im);
?>
