<?php
if(isset($_POST['submit_html']) OR isset($_POST['submit_csv']) OR isset($_POST['submit_stats'])) {
        $period = 'date >= date("'.$_POST['date_begin'].'") AND date <= date("'.$_POST['date_end'].'")';
        $journaux = '';
        if(isset($_POST['journal'])) {
            $journaux .= 'AND (';
            foreach($_POST['journal'] as $journal) {
                $journaux .= 'journal LIKE "'.$journal.'" OR ';
            }
            $journaux = substr($journaux, 0, -4);
            $journaux .=')';
        }

        $categories = '';
        if(isset($_POST['category'])) {
            $categories .= 'AND (';
            foreach($_POST['category'] as $category) {
                $categories .= ' category LIKE "'.$category.'" OR ';
            }
            $categories = substr($categories, 0, -4);
            $categories .=')';
        }
        $search = '';
        if(isset($_POST['search']) AND $_POST['search'] != '') {
            $search .= "AND (title LIKE '%".$_POST['search']."%' OR content LIKE '%".$_POST['search']."%')"; 
        }
        $sql = 'SELECT * FROM presse WHERE '.$period.' '.$journaux.' '.$categories.' '.$search.' ORDER BY date';
        echo '<h1>Corpus de la revue de presse (généré le '.date('c', time()).')</h1>';
        echo '<a href="javascript:window.print()">Imprimer</a>';
        echo '<p><code>'.$sql.'</code></p>';
        $query = $db->prepare($sql);
        $query->execute();
        $count = 0;
        $html_output = '';
        $csv_output = array();
        $iramuteq = '';
        $csv_output[] = array('date', 'journal', 'category', 'title', 'content', 'source');
        while($data = $query->fetch()) {
        $html_output .= '
            <article>
                    <header>
                    <h1><a href="#'.$count.'" id="'.$count.'">#'.$count.'</a> '.$data['title'].'</h1>
                    <aside>
                    <p class="d-print-none"><a href="?action=add&id='.$data['id'].'">✏Éditer</a></p>
                    <p><time>'.$data['date'].'</time> ⋅ Journal : '.$data['journal'].' ⋅ - '.$data[' category'].' ⋅ source : '.$data['source'].' ⋅ '.str_word_count((string)$data['content'],0).' mots</p>
                    </aside>
                    </header>
                    <main>
                    '.$data['content'].'
                    </main>
                </article>';
                $iramuteq .= '**** *date_'.$data['date'].' *journal_'.$data['journal'].' *category_'.$data['category'].PHP_EOL.$data['title'].PHP_EOL.$data['content'].PHP_EOL.PHP_EOL;
                $csv_output[] = array($data['date'], $data['journal'], $data['category'], $data['title'], $data['content'], $data['source']);
                $count++;

            }
        if(isset($_POST['submit_html'])) {
            echo $count.' résultat(s)';
            echo $html_output;
        }
        if(isset($_POST['submit_iramuteq'])) {
           
        }
        if(isset($_POST['submit_csv'])) {
            echo 'Fichier Iramuteq exporté !';
            file_put_contents('data/'.$_SESSION['project'].'/'.date('Y-m-d-His', time()).'_presse.txt', $iramuteq);
            echo 'Fichier CSV exporté !';
            exportcsv($csv_output, 'data/'.$_SESSION['project'].'/'.date('Y-m-d-His', time()).'_presse.csv');
        }

        if(isset($_POST['submit_stats'])) {
           $sql_day = 'SELECT strftime(\'%Y-%m-%d\', date) as dates, count(*) as nb FROM presse WHERE '.$period.' '.$journaux.' '.$categories.' '.$search.' GROUP BY strftime(\'%Y-%m-%d\', date);';
           $sql_month = 'SELECT strftime(\'%Y-%m\', date) as dates, count(*) as nb FROM presse WHERE '.$period.' '.$journaux.' '.$categories.' '.$search.'  GROUP BY strftime(\'%Y-%m\', date);';
           $sql_year = 'SELECT strftime(\'%Y\', date) as dates, count(*) as nb FROM presse WHERE '.$period.' '.$journaux.' '.$categories.' '.$search.' GROUP BY strftime(\'%Y\', date);';
            $sql_categories = 'SELECT category, count(*) as nb FROM presse WHERE '.$period.' '.$journaux.' '.$categories.' '.$search.'  GROUP BY category ORDER BY nb DESC';
            $sql_journaux = 'SELECT journal, count(*) as nb FROM presse WHERE '.$period.' '.$journaux.' '.$categories.' '.$search.' GROUP BY journal ORDER BY nb DESC';
                echo '<h1>Occurences par jour</h1><table class="table"><tr><th>Dates</th><th>nb</th></tr>';
                echo '<p><code>'.$sql_day.'</code></p>';
                $query = $db->query($sql_day);
                while($data = $query->fetch()) {
                    echo '<tr><td>'.$data['dates'].'</td><td>'.$data['nb'].'</td></tr>';
                }
                echo '</table>';
                echo '<h1>Occurences par mois</h1><table class="table"><tr><th>Dates</th><th>nb</th></tr>';
                echo '<p><code>'.$sql_month.'</code></p>';
                $query = $db->query($sql_month);
                while($data = $query->fetch()) {
                    echo '<tr><td>'.$data['dates'].'</td><td>'.$data['nb'].'</td></tr>';
                }
                echo '</table>';
                echo '<h1>Occurences par année</h1> <table class="table"><tr><th>Dates</th><th>nb</th></tr>';
                echo '<p><code>'.$sql_year.'</code></p>';
                $query = $db->query($sql_year);
                while($data = $query->fetch()) {
                    echo '<tr><td>'.$data['dates'].'</td><td>'.$data['nb'].'</td></tr>';
                }
                echo '</table>';
            echo '<h1>Catégories</h1><table class="table"><tr><th>Catégorie</th><th>nb</th></tr>';
            echo '<p><code>'.$sql_categories.'</code></p>';
            $query = $db->query($sql_categories);
            while($data = $query->fetch()) {
                echo '<tr><td>'.$data['category'].'</td><td>'.$data['nb'].'</td></tr>';
            }
            echo '</table>';
          echo '<h1>Journaux</h1><table class="table"><tr><th>Journal</th><th>nb</th></tr>';
             echo '<p><code>'.$sql_journaux.'</code></p>';
            $query = $db->prepare($sql_journaux);
            $query->execute();
            while($data = $query->fetch()) {
                echo '<tr><td>'.$data['journal'].'</td><td>'.$data['nb'].'</td></tr>';
            }
            echo '</table>';
        }
    }
