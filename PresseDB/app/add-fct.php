<?php
if (isset($_GET['id'])) {
    $something = sql('SELECT * FROM presse WHERE id=?', array(intval($_GET['id'])));
    $something->setFetchMode(PDO::FETCH_BOTH);
    $data = $something->fetch();
    $date = $data['date'];
    $journal = $data['journal'];
    $category = $data['category'];
    $title = $data['title'];
    $content = $data['content'];
    $source = $data['source'];
    $id = $_GET['id'];
}
elseif(isset($_POST['quick'])) {
    $txtify = file_get_contents('https://txtify.it/'.$_POST['quick']);
    $txtify_explode = explode("\n",$txtify);  

    $date = date('Y-m-d', time());
    $journal = '';
    $category = '';
    $title = ucfirst(strtolower($txtify_explode[0]));
    $content = $txtify;
    $source = $_POST['quick'];
    $id = 0;
}
else {
    $date = date('Y-m-d', time());
    $journal = '';
    $category = '';
    $title = '';
    $content = '';
    $source = '';
    $id = 0;
}
if (isset($_POST['content'])) {
    echo "Ajout dans la base de données";
    //si id_member = 0 on insert sinon on update
    if ($_POST['id'] == 0) {
        echo "nouvelle donnée";
        sql('INSERT INTO presse (date, journal, category, title, content, source) VALUES (?, ?, ?, ?, ?, ?)', array($_POST['date'], $_POST['journal'],$_POST['category'],$_POST['title'],$_POST['content'], $_POST['source']));
    } else {
        sql('UPDATE presse SET date = ?, journal = ?, category = ?, title=?, content = ?, source = ? WHERE id=?', array($_POST['date'], $_POST['journal'], $_POST['category'], $_POST['title'], $_POST['content'], $_POST['source'], $_POST['id']));
    }
    header('Location: index.php?action=view&id='.$_POST['id'].'');
}
