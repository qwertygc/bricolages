<?php
error_reporting(0);
session_start();
if(!file_exists('data.db')) {
	$db = New PDO('sqlite:data.db');
/*
ITEM : le nom de l'objet
LINK : lien de suivi
Statut : 0 en cours de livraison 1 livré
*/
	$db->query('CREATE TABLE "tracker" ("id" INTEGER PRIMARY KEY AUTOINCREMENT,"item" TEXT, "link" TEXT, "statut" INTEGER);');
	$db->query('CREATE TABLE "config" ("id" INTEGER PRIMARY KEY AUTOINCREMENT,"key" TEXT, "value" TEXT);');
	$query = $db->prepare('INSERT INTO config(key, value) VALUES("password", ?);');
	$query->execute(array(password_hash('demo', PASSWORD_DEFAULT)));
	header('Location: index.php');
}
else {
	$db = New PDO('sqlite:data.db');
	 $CONFIG = array();
    foreach ($db->query('SELECT * FROM config') as $data) {
        $CONFIG[$data['key']] = $data['value'];
    }
}

if(isset($_POST['password_login'])) {
	if(password_verify($_POST['password_login'], $CONFIG['password'])) {
		$_SESSION['login'] = true;
        $_SESSION['token'] = sha1(microtime());
        header('Location: index.php');
	}
}
if(isset($_GET['disconnect'])  AND $_GET['t'] == $_SESSION['token']) {
   unset($_SESSION['login']);
   unset($_SESSION['token']);
   session_destroy();
   header('Location: index.php');
    exit();
}

if(isset($_POST['password_change']) AND $_POST['t'] == $_SESSION['token']) {
	$query = $db->prepare('UPDATE config SET value=? WHERE key=?');
	$query->execute(array(password_hash($_POST['password_change'], PASSWORD_DEFAULT), 'password'));
	header('Location: index.php');
}
if(isset($_POST['link']) AND $_POST['t'] == $_SESSION['token']) {
	$query = $db->prepare('INSERT INTO tracker (item, link, statut) VALUES(?,?, ?);');
	$query->execute(array($_POST['item'], $_POST['link'], 0));
}
if(isset($_GET['delete']) AND $_GET['t'] == $_SESSION['token']) {
	$query = $db->prepare('DELETE FROM tracker WHERE id=?');
	$query->execute(array($_GET['delete']));
	header('Location: index.php');
}
if(isset($_GET['received']) AND $_GET['t'] == $_SESSION['token']) {
	$query = $db->prepare('UPDATE tracker SET statut=1 WHERE id=?');
	$query->execute(array($_GET['received']));
	header('Location: index.php');
}
?>
<!doctype html>
<html>
	<head>
		<title>Packages Tracker</title>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
	</head>
	<body>
		<header>
			<h1>📦 Suivi des colis</h1>
		</header>
		<main>
			<?php if($_SESSION['login'] == true): ?>
				<form method="post" action="index.php">
				<label for="item">Nom (facultatif)</label> <input type="text" name="item" id="item"/>
				<label for="link">Lien de suivi<sup style="color:red">*</sup></label> <input type="url" name="link" id="link" required/>
				<input type="hidden" name="t" id="t" value="<?php echo $_SESSION['token']; ?>"/>
				<input type="submit">
				</form>
				<table>
					<tr>
						<th></th>
						<th>Colis</th>
						<th>Lien</th>
						<th>Statut</th>
					</tr>
			<?php
			$query = $db->prepare('SELECT * FROM tracker');
			$query->execute();
			$result = $query->fetchAll();
			foreach($result as $data):
			$statut = ((int)$data['statut'] == 0) ? '🚚 En cours de livraison' : '✅ Livré';
			?>
					<tr>
						<td><a href="?received=<?php echo $data['id']; ?>&t=<?php echo $_SESSION['token']; ?>">✅</a> <a href="?delete=<?php echo $data['id']; ?>&t=<?php echo $_SESSION['token']; ?>">🗑</a></td>
						<td><?php echo $data['item']; ?></td>
						<td><a href="<?php echo $data['link']; ?>" target="_blank">🔗 lien de suivi</a></td>
						<td><?php echo $statut; ?></td>
					</tr>
			<?php
			endforeach;
			?>
				</table>
				<form method="post" action="index.php">
				<label for="password_change">Nouveau mot de passe</label> <input type="password" name="password_change" id="password_change"/>
				<input type="hidden" name="t" id="t" value="<?php echo $_SESSION['token']; ?>"/>
				<input type="submit">
				</form>
			<a href="?disconnect=true&t=<?php echo $_SESSION['token']; ?>">Se déconnecter</a>
			<?php else: ?>
			<form method="post" action="index.php">
				<label for="password_login">Mot de passe</label> <input type="password" name="password_login" id="password_login"/>
				<input type="submit">
				</form>
			<?php endif;?>
		</main>
	</body>
</html>
