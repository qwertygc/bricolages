<?php
session_start();
if(isset($_GET['cookie'])) {
	if($_GET['cookie'] == 'true') {
		$_SESSION['cookie'] = true;
		setcookie('cookie', true, time() + 365*24*3600, null, null, false, true);
	}
}
function is_cookie() {
	if(isset($_SESSION['cookie']) || isset($_COOKIE['cookie'])) {
		if($_SESSION['cookie']||$_COOKIE['cookie']) {
			return true;
		}
		else {
			return false;
		}
	}
}
function cookie_msg($txt, $url) {
	if(!is_cookie()) {
	echo $txt.' <a href="?cookie=true">OK</a> <a href="'.$url.'">En savoir plus</a>';
	}
}
cookie_msg('Ce site utilise des cookies afin de vous offrir une meilleure expérience de navigation. Continuer signifie que vous en acceptez les conditions d\'utilisation.', 'cookie.php');