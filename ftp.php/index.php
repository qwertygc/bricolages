<?php
#######CONFIG##########
define('ROOT', '/ftp/'); #Root of directory
$index_files = array( #open files in browser
	'index.html',
	'index.htm',
	'index.php'
);
$blacklist = array ( #blacklist in listing.
	'index.php',
	'url'
);
// Tip : to protect a folder, please create a .pwd in folder with password. To send the URL, add GET pwd=youpassword. Example: http://example.org/?dir=folder&pwd=helloworld
#######################
error_reporting(0);
function Size($path) { #http://stackoverflow.com/questions/5501427/php-filesize-mb-kb-conversion
    $bytes = sprintf('%u', filesize($path));
    if ($bytes > 0) {
        $unit = intval(log($bytes, 1024));
        $units = array('B', 'KB', 'MB', 'GB');
        if (array_key_exists($unit, $units) === true) {
            return sprintf('%d %s', $bytes / pow(1024, $unit), $units[$unit]);
        }
    }

    return $bytes;
}

function forcedl($file) {
	header("Content-disposition: attachment; filename=".$file);
	header("Content-type: ".mime_content_type($file));
	readfile($file);
}
$dir = (!isset($_GET['dir']) || $_GET['dir'] == "" || $_GET['dir'] == "." || $_GET['dir'] == ".." || $_GET['dir'] == "../") ? '.' : $_GET['dir'].'/';
$_GET['file'] = (!isset($_GET['file'])) ? null : forcedl($_GET['file']);
$files= scandir($dir, SCANDIR_SORT_ASCENDING);
$subdir = explode('/', $dir);
$count = count($subdir) -1;
$listdir = null;
$lastdir = null;
$listdirraw = null;
for($i=0;$i<$count;++$i){
	$slash = (substr($listdiraw, -1) == '/') ? '' : '/';
	$listdirraw .= $subdir[$i].$slash;
	$lastdir .= ($subdir[$i - 1] != null) ? $subdir[$i-1].$slash : '';
	$listdir .=  '<a href="?dir='.$listdirraw.'">'.$subdir[$i].'</a>/';
}
if((substr($_GET['dir'], -1) == '/')) { header('Location: index.php?dir='.substr($_GET['dir'], 0, -1)); }
$ariane = ($dir == '.') ? 'Home' : '<a href="'.ROOT.'">Home</a>/'.$listdir;
?>
<!doctype html>
<html>
<head>
<style>
* {
	margin:0;
	padding:0;
	box-sizing: border-box; 
}

html {
	min-height:100%;
	color:#61666c;
	font-weight:400;
	font-size:1em;
	font-family:'Open Sans', sans-serif;
	line-height:2em;
	width:90%;
	margin:auto;
}
body {
	padding:20px;
}
a {
	color:#61666c;
	text-decoration:none;
}
a:hover {
	color:#2a2a2a;
}
table {
	width:100%;
	border-collapse:collapse;
	font-size:.875em;
}
tr {
	outline:0;
	border:0;
}
tr:hover td {
	background:#f6f6f6;
}
th {
	text-align:center;
	font-size:.75em;
	padding-right:20px;
}
/* 2nd Column: Filename */

td {
	padding:5px 0;
	outline:0;
	border:0;
	border-bottom:1px solid #edf1f5;
	vertical-align:middle;
	text-align:center;
	transition:background 300ms ease;
}
td:first-of-type  {
	text-align:left;
}
td a{
	display: block;
}
</style>
<title>Directory</title>
</head>
<body>
<?php
if(in_array($_GET['dir'], $blacklist) || in_array($_GET['file'], $blacklist)) {
	header("HTTP/1.0 404 Not Found");
	header("Location: index.php");
	die();
}
echo '<header>'.$ariane.'</header>';
echo "<table>".PHP_EOL;
echo '<tr><th>File</th><th>Size</th><th>Last Modified</th></tr>';
echo ($dir != '.') ? '<tr><td><a href="?dir='.$lastdir.'">&#8624;..</a></td><td></td><td></td><td></td><td></td></tr>' : '';
foreach($files as $file) {		
	if($file !='.' AND $file !='..') {
		$dir = ($dir == '.') ? '' : $dir;
		$filename = $file;
		$file = $dir.$file;
		$lock = (is_dir($file) AND file_exists($file.'/.pwd')) ? '&#128274;' : '&#160;';
		$icon = (is_dir($file)) ? $lock.'&#128193;' : '&#160;';
		$filedl = (in_array($filename, $index_files)) ? $file : ((!is_dir($file)) ? '?file='.$file : '?dir='.$file);
		$md5 = (!is_dir($file)) ? md5_file($file) : '-';
		$sha1 = (!is_dir($file)) ? sha1_file($file) : '-';
		$filesize = (!is_dir($file)) ? Size($file) : '-';
		$filemtime =  date('Y-m-d H:i:s', filemtime($file));
		if(!in_array($file, $blacklist) AND (!file_exists($dir.'/.pwd') || $_GET['pwd'] == file_get_contents($dir.'/.pwd'))) {
			if($filename != '.pwd') {
				echo '<tr>';
				echo '<td><a href="'.$filedl.'" title="sha1: '.$sha1.' md5: '.$md5.'">'.$icon.' '.$filename.'</a></td>';
				echo '<td>'.$filesize.'</td>';
				echo '<td>'.$filemtime.'</td>';	
				echo '</tr>';
			}
		}
	}
}
echo '</table>';
?>
</body>
</html>