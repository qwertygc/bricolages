# RSS2BlueSky

Envoyer un flux RSS vers BlueSky et Mastodon. Éditer config.sample.php en le renommant config.php et en modifiant les informations et faire un cron sur feed2rs.php.


Crédits :

- [Class.Bsky.php par Nesges](https://codeberg.org/nesges/rss2bsky/src/branch/main/class.Bsky.php)
- [MastodonBotPHP par Eleirbag89](https://github.com/Eleirbag89/MastodonBotPHP)
- [SimplePie](https://github.com/simplepie/simplepie)


