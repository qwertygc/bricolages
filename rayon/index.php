<?php
function geoloc($loc) {
$options = array(
	'http'=>array(
		'method'=>"GET",
		'header'=>"Accept-language: en\r\n".
				  "Cookie: foo=bar\r\n".
				"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0\r\n")
		);
		$geocodage = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search/?q='.$loc.'&format=json&addressdetails=1&limit=1', false,  stream_context_create($options)), true);
		$lat = $geocodage[0]['lon'];
		$lon =  $geocodage[0]['lat'];
		/*$geocodage = json_decode(file_get_contents('https://photon.komoot.de/api/?q='.urlencode($loc).'&limit=1&lang=fr', false,  stream_context_create($options)), true);
		$lat = $geocodage['features'][0]['geometry']['coordinates'][0];
		$lon =  $geocodage['features'][0]['geometry']['coordinates'][1];
		/*$geocodage = json_decode(file_get_contents('https://api-adresse.data.gouv.fr/search/?q='.$loc), true);
		$lat = isset($geocodage['features'][0]['geometry']['coordinates']['0']) ? $geocodage['features'][0]['geometry']['coordinates']['0'] : '44.366';
		$lon = isset($geocodage['features'][0]['geometry']['coordinates']['1']) ? $geocodage['features'][0]['geometry']['coordinates']['1'] : '4.5332';*/


		return array('lat'=>$lat, 'lon'=>$lon);
		//print_r($geocodage);
}
function reversegeoloc($lat, $lon) {
$options = array(
	'http'=>array(
		'method'=>"GET",
		'header'=>"Accept-language: en\r\n".
				  "Cookie: foo=bar\r\n".
				"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0\r\n")
		);
	$geocodage = json_decode(file_get_contents('https://nominatim.openstreetmap.org/reverse.php?lat='.$lat.'&lon='.$lon.'&zoom=18&format=jsonv2', false,  stream_context_create($options)), true);
	return $geocodage['display_name'];
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
	<link rel="stylesheet" href="https://unpkg.com/leaflet@latest/dist/leaflet.css" />
	<script src="https://unpkg.com/leaflet@latest/dist/leaflet-src.js"></script>
    <title>Rayon de XX km</title>
    <script>
	if(navigator.geolocation)
	  navigator.geolocation.getCurrentPosition(maPosition); 
	function maPosition(position) {
		  document.getElementById("lat").setAttribute("value",position.coords.latitude.toFixed(4));
		  document.getElementById("lon").setAttribute("value",position.coords.longitude.toFixed(4));
		document.myform.submit();
	}
	</script>
    <style>
    	div.one {
			position: absolute;
			right: 10px;
			top: 10px;
			z-index: 10000;
			background: rgba(255, 255, 255, 0.75);
		}
		#macarte {
			position: absolute;
			width: 100%;
			height: 100%;
		}
	</style>
  </head>
  <body>

<?php
	$adress = (isset($_POST['loc'])) ? $_POST['loc'] : (isset($_POST['lat']) ? reversegeoloc($_POST['lat'], $_POST['lon']) : '');
	$rayon = (isset($_POST['rayon'])) ? $_POST['rayon'] : 1;
	if(isset($_POST['loc'])) {
	$loc = geoloc($_POST['loc']);
}
?>
	<div class="one">
		<form method="post" action="index.php">
			<input type="text" id="lat" name="lat"/>
			<input type="text" id="lon" name="lon"/>
			<input type="submit" value="Trouver moi"/>
		</form>
		<form method="post" action="index.php">
			<input type="text" name="loc" id="loc" value="<?php echo $adress; ?>" placeholder="adresse">
			<input type="number" name="rayon" id="rayon" value="<?php echo $rayon; ?>"/>
			<input type="submit"/>
		</form>
	</div>
	<div id="macarte"></div>

    <script>
      var carte = L.map('macarte').setView([46.3630104, 2.9846608], 6);
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(carte);
	<?php
if(isset($_POST['loc'])) {
	$rayon = $_POST['rayon'] * 1000;
	echo 'var marker = L.marker(['.$loc['lon'].', '.$loc['lat'].']).addTo(carte);
	var influence = L.circle(['.$loc['lon'].', '.$loc['lat'].'], '.$rayon.').addTo(carte);';
}
	?>
    </script>
  </body>

</html>
