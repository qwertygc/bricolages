<!doctype html>
<html lang="fr">
<head>
	<title>Rechercher un ouvrage</title>
<script src="https://unpkg.com/html5-qrcode"></script>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" >
		<style>
		label, input, .results a {
			display:block;
			width:100%;
			margin:auto;
			font-size:1.5em;
		}
		.results img {
			margin:auto;
		}
		.results a {
			padding: .5em;
			margin-top:.5em;
			text-align:center;
			border : 1px solid grey;
			border-radius: 20px;
		}
		</style>
</head>
<body>


<div id="reader" style="width: 100%"></div>

	<form method="get" name="search">
		<label for="isbn">Rentrer l'ISBN</label>
		<input type="text" name="isbn" id="isbn">
		<input type="submit">
	</form>
<div class="results">
<?php
if(isset($_GET['isbn'])) {
	$isbn = $_GET['isbn'];
	//$openlibrary = json_decode(file_get_contents('https://openlibrary.org/api/books?bibkeys=ISBN:'.$isbn.'&jscmd=details&format=json'), true);
	//print_r($openlibrary);

	$openlibrary = json_decode(file_get_contents('https://openlibrary.org/api/books?bibkeys=ISBN:'.$isbn.'&jscmd=details&format=json'), true);
	echo '<img src="https://covers.openlibrary.org/b/isbn/'.$isbn.'-L.jpg" />';
	echo '<p>'.$openlibrary['ISBN:'.$isbn]['details']['title'].', '.$openlibrary['ISBN:'.$isbn]['details']['publish_date'].', '.$openlibrary['ISBN:'.$isbn]['details']['publishers'][0].'</p>';
	echo '<a href="https://catalogue.bnf.fr/search.do?mots0=NRC;-1;0;'.$isbn.'&mots1=ALL;0;0;&&pageRech=rav">Notice BnF</a>';
	echo '<a href="https://worldcat.org/fr/search?q=bn%3A'.$isbn.'">Notice WorldCat</a>';
	echo '<a href="http://www.sudoc.abes.fr/cbs//DB=2.1/CMD?ACT=SRCHA&IKT=7&SRT=RLV&TRM='.$isbn.'">Notice Sudoc</a>';
	echo '<a href="https://www.zotero.org/save?q='.$isbn.'">Rajouter à Zotero</a>';


}
?>
</div>
	<script>
var input = document.getElementById('isbn');

function onScanSuccess(decodedText, decodedResult) {
  // handle the scanned code as you like, for example:
  console.log(`Code matched = ${decodedText}`, decodedResult);
  input.value = input.value + decodedText;
  html5QrcodeScanner.clear();
  //document.search.submit();
}

function onScanFailure(error) {
  // handle scan failure, usually better to ignore and keep scanning.
  // for example:
  console.warn(`Code scan error = ${error}`);
}

let html5QrcodeScanner = new Html5QrcodeScanner(
  "reader",
  { fps: 10, qrbox: {width: 250, height: 250} },
  /* verbose= */ false);
html5QrcodeScanner.render(onScanSuccess, onScanFailure);
</script>
</body>
</html>
