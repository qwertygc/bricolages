<?php
define('SERVER_ZMD', 'localhost:27272');
//header('Content-Type: text/plain');
function to_md($text) {
	return json_decode(curl(SERVER_ZMD.'/html', 'POST', json_encode(array('md'=>$text)), array('Content-Type: application/json')), true)[0];
}
function read_content($array, $slug='') {
	$content = file_get_contents('content/'.$array);
	$content = to_md($content);
	$content = str_replace('/static/', 'https://zestedesavoir.com/static/', $content);
	$content = str_replace('/media/', 'https://zestedesavoir.com/media/', $content);
	$content = str_replace('href="/', 'href="https://zestedesavoir.com/', $content);
	return $content;
}
function curl($url, $type='POST', $post=array(), $headers=array()) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec ($ch);
	curl_close($ch);
	return $response;
}
$manifest = json_decode(file_get_contents('content/manifest.json'), true);
$file = '<!doctype html>
<html>
	<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="https://framagit.org/champlywood/Reset.css/raw/master/reset.css"/>
	<style>
	body {
		max-width:800px;
		margin:auto;
		font-family: "Open Sans",sans-serif;
	}
	.custom-block {
		padding:1em;
		margin:1em;
	}
	.custom-block-question {
		background:#e2daee;
	}
	.custom-block-warning {
		background:#eee7da;
	}
	.custom-block-information {
		background:#eee7da;
	}
	.custom-block-error {
		background:#eedada;
	}
	.custom-block-spoiler {
		background:#eeeeee;
	}
	img {
		max-width:100%;
	}
	.footnotes {
	color:#999;
	}
	</style>
	</head>
	<body>';
switch($manifest['type']) {
	case 'TUTORIAL':
	$file .= '<h1 itemprop="name">'.$manifest['title'].'</h1>'.PHP_EOL.PHP_EOL;
	$file .= '<i style="color:#999">'.$manifest['description'].'</i>';
	$file .= '<p style="float:right">'.$manifest['licence'].'</p>';
	$file .= read_content($manifest['introduction'], 'intro_all').PHP_EOL.PHP_EOL;
	$file .= '<hr>';
	foreach($manifest['children'] as $part) {
		$file .= '<h2 id="'.$part['slug'].'"><a href="#'.$part['slug'].'">'.$part['title'].'</a></h2>'.PHP_EOL.PHP_EOL;
		$file .= read_content($part['introduction'], 'intro_'.$part['slug']).PHP_EOL.PHP_EOL;
		foreach($part['children'] as $subpart) {
			$file .= '<h3 id="'.$subpart['slug'].'"><a href="#'.$subpart['slug'].'">'.$subpart['title'].'</a></h3>'.PHP_EOL.PHP_EOL;
			$file .= read_content($subpart['introduction'], 'intro_'.$subpart['slug']).PHP_EOL.PHP_EOL;
			foreach($subpart['children'] as $subsubpart) {
				$file .= '<h4 id="'.$subsubpart['slug'].'"><a href="#'.$subsubpart['slug'].'">'.$subsubpart['title'].'</a></h4>'.PHP_EOL.PHP_EOL;
				$file .= read_content($subsubpart['text'], $subsubpart['slug']).PHP_EOL.PHP_EOL;
			}
			$file .= read_content($subpart['conclusion'], 'conclu_'.$subpart['slug']).PHP_EOL.PHP_EOL;
		}
		$file .= read_content($part['conclusion'], 'conclu_'.$part['slug']).PHP_EOL.PHP_EOL;
	}
	$file .=  '<hr>';
	$file .= read_content($manifest['conclusion'], 'conclu_all');
	break;








	case 'ARTICLE':
	$file .= '<h1 itemprop="name">'.$manifest['title'].'</h1>'.PHP_EOL.PHP_EOL;
	$file .= '<i style="color:#999">'.$manifest['description'].'</i>';
	$file .= '<p style="float:right">'.$manifest['licence'].'</p>';
	$file .= read_content($manifest['introduction']).PHP_EOL.PHP_EOL;
	$file .= '<hr>';
	foreach($manifest['children'] as $part) {
		$file .= '<h1 id="'.$part['slug'].'"><a href="#'.$part['slug'].'">'.$part['title'].'</a></h1>'.PHP_EOL.PHP_EOL;
		$file .= read_content($part['text']).PHP_EOL.PHP_EOL;
	}
	echo '<hr>'.PHP_EOL.PHP_EOL;
	$file .= read_content($manifest['conclusion']);
	break;
}
$file .= '</body></html>';

echo $file;
file_put_contents($manifest['slug'].'.html', $file);
?>