# Installation

Ce logiciel nécessite d'installer un serveur ZMarkdown et PHP.

## Installation du serveur PHP

```Sudo aptitude install php php-curl```

## Installation du serveur ZMarkdown

Si vous comprenez pas comment faire, regardez [ce billet](https://zestedesavoir.com/billets/2502/comment-et-pourquoi-jai-integre-zmarkdown-a-pelican/).

D'abord, il faut cloner le [serveur ZMarkdown](https://github.com/zestedesavoir/zmarkdown.git)

Ensuite, il faut installer [NodeJS](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions) et [Yarn](https://yarnpkg.com/en/docs/install#debian-stable).

Aller dans le dépôt cloné (le dossier) et installer les dépendances en faisant ```yarn```.

On lance le serveur en allant dans packages/zmarkdown/ et en faisant ```npm run server```. Le serveur se trouve dans localhost:27272

## Installation du serveur PHP
Cloner le logiciel ici présent. Faire ```php -s localhost:8080``` dans le dossier.

Il faut ensuite mettre le contenu de l'archive dans le dossier « content/ » (à la du serveur). Ca s'affiche.

Aller sur localhost:8080. Le contenu du tuto/article s'affiche et un fichier .html est crée.