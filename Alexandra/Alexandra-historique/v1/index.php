<?php
include ("header.php");
try {
    $bdd = new PDO('mysql:host=localhost;dbname=test', 'root', '');
   $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "<h1>Le serveur SQL ne répond plus</h1>";
    echo "<p>Désolé de notre indisponibilité, le serveur SQL ne répond plus. Revenez dans quelques minutes ;) !";
	    echo "<p>Erreur : " . $e->getMessage() . "</p>";

    exit();
}



if(isset($_GET['action'])){
switch ($_GET['action'])
{
case 'export':

 header("Expires: Mon, 26 Jul 1997 05:00:00 GMT\n");
 header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
 header("Content-type: text/ods;\n"); 
 header("Content-Transfer-Encoding: binary");
 header("Content-Disposition: attachment; filename=\"testfile.ods\";\n\n");
 ?>
<table>
<tr>
<th>Titre</th>
<th>Auteur</th>
<th>emprunteur</th>
<th>Date du prêt</th>
</tr>
<?php
$retour = $bdd->query('SELECT * FROM preteur, livre WHERE  preteur.id = livre.id_preteur ORDER BY livre.id DESC');
while($donnees = $retour->fetch())
{
?>

<tr>
<td><?php echo stripslashes($donnees['titre']); ?></td>
<td><?php echo stripslashes($donnees['auteur']); ?></td>
<td><?php echo stripslashes($donnees['preteur']); ?></td>
<td <?php $interval = 31536000;  if((time() - $donnees['timestamp']) > $interval) {echo 'style="background-color: #f2dede;border-color: #eed3d7;color: #b94a48;"';} ?>><?php echo date('d/m/Y', $donnees['timestamp']); ?></td>
</tr>
<?php
} 
?>
</table>

<?php
break;
		case 'addpreteur':
		if (isset($_GET['modifier_preteur'])) {
    $_GET['modifier_preteur'] = htmlspecialchars($_GET['modifier_preteur']);
    $retour = 	$bdd->query('SELECT * FROM preteur WHERE id=\'' . $_GET['modifier_preteur'] . '\'');
$retour->setFetchMode(PDO::FETCH_BOTH);
$donnees = $retour->fetch();
    $preteur = stripslashes($donnees['preteur']);
    $id_preteur = $donnees['id']; // Cette variable va servir pour se souvenir que c'est une modification
}
else // C'est qu'on rédige une nouvelle news
{
	$preteur ='';
    $id_preteur = 0; // La variable vaut 0, donc on se souviendra que ce n'est pas une modification
}
		?>
		<form action="index.php" method="post">
	<label>Emprunteur</label><input type="text" size="30" name="preteur" value="<?php echo  $preteur; ?>" spellcheck="true" required/>
	<input type="hidden" name="id_preteur" value="<?php echo $id_preteur; ?>" /><br/>
	    <input type="submit" value="Envoyer" />

	</form>
<?php
		break;
		
		case 'addpret':
	if (isset($_GET['modifier_pret'])) {
    $_GET['modifier_pret'] = htmlspecialchars($_GET['modifier_pret']);
    $retour = 	$bdd->query('SELECT * FROM livre WHERE id=\'' . $_GET['modifier_pret'] . '\'');
$retour->setFetchMode(PDO::FETCH_BOTH);
$donnees = $retour->fetch();
    $titre = stripslashes($donnees['titre']);
    $auteur = stripslashes($donnees['auteur']);
    $id_preteur = $donnees['id-preteur']; 
    $id_livre = $donnees['id']; 
}
else // C'est qu'on rédige une nouvelle news
{
    $titre = '';
    $auteur = '';
    $id_preteur = ''; 
	$id_livre = 0; // La variable vaut 0, donc on se souviendra que ce n'est pas une modification
}
?>
		<form action="index.php" method="post">
	<label>Titre</label><input type="text" size="30" name="titre" value="" spellcheck="true" required/><br/>
	<label>Auteur</label><input type="text" size="30" name="auteur" value="" spellcheck="true" required/><br/>
	<label>emprunteur</label> <select name="id_preteur">
	<?php
	$retour = $bdd->query('SELECT * FROM preteur ORDER BY id DESC');
while($donnees = $retour->fetch())
{
?>
	    <option value="<?php echo $donnees['id']; ?>"><?php echo $donnees['preteur']; ?></option>
		<?php } ?>
     </select>
	<input type="hidden" name="id_livre" value="<?php echo $id_livre; ?>" /><br/>
	    <input type="submit" value="Envoyer" />
<?php
		break;

	
}}
else {
if (isset($_POST['preteur']))
{
    $preteur = htmlspecialchars(addslashes($_POST['preteur']));
    if ($_POST['id_preteur'] == 0)
    {
    $bdd->query("INSERT INTO preteur(preteur) VALUES('" . $preteur . "')");
    }
    else
    {
        $_POST['id_preteur'] = addslashes($_POST['id_preteur']);
        	
 $bdd->query("UPDATE preteur SET preteur='" . $preteur . "' WHERE id='" . $_POST['id_news'] . "'");
    }
}

if (isset($_POST['titre']) AND isset($_POST['auteur']) AND isset($_POST['id_preteur']))
{
    $titre = htmlspecialchars(addslashes($_POST['titre']));
    $auteur = htmlspecialchars(addslashes($_POST['auteur']));
    if ($_POST['id_livre'] == 0)
    {
    $bdd->query("INSERT INTO livre(titre, auteur, id_preteur, timestamp) VALUES('" . $titre . "', '" . $auteur . "','" . $_POST['id_preteur'] . "', '". time()."')");
    }
    else
    {
        $_POST['id_livre'] = addslashes($_POST['id_livre']);
        	
 $bdd->query("UPDATE livre SET titre='" . $titre . "', auteur='" . $auteur . "', id_preteur='" . $_POST['id_preteur'] . "' WHERE id='" . $_POST['id_news'] . "'");
    }
}
if (isset($_GET['supprimer_livre'])) 
{
    $_GET['supprimer_livre'] = addslashes($_GET['supprimer_livre']);
    	
$bdd->query('DELETE FROM livre WHERE id=\'' . $_GET['supprimer_livre'] . '\'');
}
?>

<table>
<tr>
<th>Delete</th>
<th>Titre</th>
<th>Auteur</th>
<th>Emprunteur</th>
<th>Date du prêt</th>
</tr>
<?php
$retour = $bdd->query('SELECT * FROM preteur, livre WHERE  preteur.id = livre.id_preteur ORDER BY livre.id DESC');
while($donnees = $retour->fetch())
{
?>

<tr>
<td><a href="?supprimer_livre=<?php echo $donnees['id']; ?>"><img src="supprimer.png" /></a>
<td><?php echo stripslashes($donnees['titre']); ?></td>
<td><?php echo stripslashes($donnees['auteur']); ?></td>
<td><?php echo stripslashes($donnees['preteur']); ?></td>
<td <?php $interval = 31536000;  if((time() - $donnees['timestamp']) > $interval) {echo 'style="background-color: #f2dede;border-color: #eed3d7;color: #b94a48;"';} ?>><?php echo date('d/m/Y', $donnees['timestamp']); ?></td>
</tr>
<?php
} 
?>
</table>
<?php
}
include("footer.php");
?>