<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<link rel="stylesheet"  type="text/css" href="design.css">
<script src="scripts.js" type="text/javascript"></script>
<link rel="alternate" type="application/rss+xml" title="" href="rss.xml" />
<link rel="apple-touch-icon" href= "apple-touche-icon.png " />
<link rel="icon" type="image/png" href="favicon.png" />
<link rel="search" type="application/opensearchdescription+xml" title="Recherche" href="opensearch.xml" /> 
<link rel="canonical" href="" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
</head>
<body>
<nav id="container">
<ul>
<li><a href="/">Accueil</a></li>
<li><a href="?action=addpreteur">Ajouter un emprunteur</a></li>
<li><a href="?action=addpret">Ajouter un prêt</a></li>
<li><a href="?action=export">Export</a></li>
</ul>
<div id="corps">