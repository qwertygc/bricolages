<?php
session_start();
include 'app/inc/inc.php';
include 'app/tpl/header.php';
error_reporting(-1);
#############################################
#		MODIFIER ICI LES IDENTIFIANTS		#
#############################################
define('LOGIN', 'demo');
define('PASSWORD', 'demo');
try {
    $bdd = new PDO('sqlite:data.sqlite');
   $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "<h1>Le serveur SQL ne répond plus</h1>";
    echo "<p>Désolé de notre indisponibilité, le serveur SQL ne répond plus. Revenez dans quelques minutes ;) !";
	 echo "<p>Erreur : " . $e->getMessage() . "</p>";
    exit();
}
if(isset($_GET['verif'])){
	switch ($_GET['verif']){
		case 'verif':
			if(isset($_POST['login']) && isset($_POST['password'])) {
				if (($_POST['login'] == LOGIN && $_POST['password'] ==PASSWORD)) {
					$_SESSION['login'] = LOGIN;
					header('Location: index.php');
				}
				else {
					echo 'Erreur';
				}
			}
		break;
	}
}
if(isset($_SESSION['login'])) {
	if(isset($_GET['action'])){
	switch ($_GET['action']) {
		case 'addemprunteur':
			$form = New form();
			$form->startform(array('action'=>'index.php', 'method'=>'post'));
			$form->label('', 'Emprunteur');
			$form->input(array('type'=>'text', 'name'=>'emprunteur', 'required'=>'required'));
			$form->input(array('type'=>'hidden', 'name'=>'id_emprunteur'));
			$form->input(array('type'=>'submit', 'value'=>'Envoyer'));
			$form->endform();
		break;
		case 'install':
			$bdd->query("
			CREATE TABLE `pret` (
	  `id` INTEGER PRIMARY KEY,
	  `id_livre` INTEGER  NOT NULL,
	  `id_emprunteur` INTEGER  NOT NULL,
	  `timestamp` bigint(20) NOT NULL
	);
	CREATE TABLE  `emprunteur` (
	  `id` INTEGER PRIMARY KEY,
	  `emprunteur` varchar(255) NOT NULL
	);

	CREATE TABLE  `livre` (
	  `id`INTEGER PRIMARY KEY,
	  `titre` varchar(255) NOT NULL,
	  `auteur` varchar(255) NOT NULL
	);


			");
		break;
		case 'addlivre':
				$form = New form();
				$form->startform(array('action'=>'index.php', 'method'=>'post'));
				$form->label('', 'Titre');
				$form->input(array('type'=>'text', 'name'=>'titre', 'required'=>'required'));
				$form->label('', 'Auteur');
				$form->input(array('type'=>'text', 'name'=>'auteur', 'required'=>'required'));
				$form->input(array('type'=>'hidden', 'name'=>'id_emprunteur'));
				$form->input(array('type'=>'submit', 'value'=>'Envoyer'));
				$form->endform();
		break;
			
			case 'addpret':
				$emprunteur_array = array();
				$form = New form();
				$form->startform(array('action'=>'index.php', 'method'=>'post'));
				$form->label('', 'Emprunteur');
				list_emprunteur();
				$form->label('', 'Livre');
				$book = isset($_GET['book']) ? $_GET['book'] : '1';
				list_book($book);
				$form->input(array('type'=>'hidden', 'name'=>'id_pret'));
				$form->input(array('type'=>'submit', 'value'=>'Envoyer'));
				$form->endform();
		break;
		case 'listelivre':
			echo '<table><tr><th>delete</th><th>Titre</th><th>Auteur</th><th>Add</th></tr>';
			$retour = $bdd->query('SELECT * FROM livre ORDER BY id DESC');
			while($donnees = $retour->fetch()):
				echo '<tr>';
				echo '<td><a href="?action=listelivre&supprimer_livre='.$donnees['id'].'"><img src="app/tpl/supprimer.png" /></a></td>';
				echo '<td>'.stripslashes($donnees['titre']).'</td>';
				echo '<td>'.stripslashes($donnees['auteur']).'</td>';
				echo '<td>'.str_replace('/index.php','','http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']).'/?book='.$donnees['id'].'&action=addpret</td>';
				echo '</tr>';
			endwhile; 
			echo '</table>';
			if (isset($_GET['supprimer_livre']))  {sql('DELETE FROM livre WHERE id=?', array($_GET['supprimer_livre'])); header('Location: ?action=listelivre'); }
		break;
		case 'logout':
			$_SESSION = array();
			session_destroy();
		break;
	}}
	else {
	if (isset($_POST['pret_emprunteur'])&&isset($_POST['pret_livre'])) {
		sql('INSERT INTO pret(id_livre, id_emprunteur, timestamp) VALUES(?, ?, ?)', array($_POST['pret_livre'],$_POST['pret_emprunteur'],time()));
	}
	if (isset($_POST['emprunteur'])) {
		sql('INSERT INTO emprunteur(emprunteur) VALUES(?)', array($_POST['emprunteur']));
	}
	if (isset($_POST['titre']) AND isset($_POST['auteur'])) {
	   sql('INSERT INTO livre(titre, auteur) VALUES(?, ?)', array($_POST['titre'], $_POST['auteur']));
	}
	if (isset($_GET['supprimer_pret']))  {sql('DELETE FROM pret WHERE id=?', array($_GET['supprimer_pret']));}
	?>
	<table>
	<tr>
	<th>Delete</th>
	<th>Titre</th>
	<th>Auteur</th>
	<th>Emprunteur</th>
	<th>Date du prêt</th>
	</tr>
	<?php
	$retour = $bdd->query('SELECT pret.id as pret_id, livre.titre, livre.auteur, pret.timestamp, emprunteur.emprunteur, pret.id_livre, livre.id, pret.id_emprunteur, emprunteur.id FROM pret INNER JOIN livre ON pret.id_livre = livre.id INNER JOIN emprunteur ON pret.id_emprunteur = emprunteur.id');
	while($donnees = $retour->fetch()) {
	?>

	<tr>
	<td><a href="?supprimer_pret=<?php echo $donnees['pret_id']; ?>"><img src="app/tpl/supprimer.png" /></a></td>
	<td><?php echo stripslashes($donnees['titre']); ?></td>
	<td><?php echo stripslashes($donnees['auteur']); ?></td>
	<td><?php echo stripslashes($donnees['emprunteur']); ?></td>
	<td <?php $interval = 31536000;  if((time() - $donnees['timestamp']) > $interval) {echo 'style="background-color: #f2dede;border-color: #eed3d7;color: #b94a48;"';} ?>><?php echo date('d/m/Y', $donnees['timestamp']); ?></td>
	</tr>
	<?php
	} 
	?>
	</table>
	<?php
	}
}
else {
	$form = New form();
	$form->startform(array('method'=>'post', 'action'=>'?verif=verif'));
	$form->label('login','Utilisateur');
	$form->input(array('type'=>'text', 'name'=>'login'));
	$form->label('password', 'Mot de passe');
	$form->input(array('type'=>'password', 'name'=>'password'));
	$form->input(array('type'=>'submit', 'name'=>'submit'));
	$form->endform();
}
include 'app/tpl/footer.php';
?>