<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<link rel="stylesheet"  type="text/css" href="app/tpl/design.css">
<link rel="canonical" href="" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
</head>
<body>
<nav id="container">
<ul>
<li><a href="?">Accueil</a></li>
<li><a href="?action=addemprunteur">Ajouter un emprunteur</a></li>
<li><a href="?action=addlivre">Ajouter un livre</a></li>
<li><a href="?action=addpret">Ajouter un prêt</a></li>
<li><a href="?action=listelivre">Liste des livres</a></li>
<li><a href="?action=logout">Déconnexion</a></li>
</ul>
<div id="corps">