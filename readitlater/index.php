<?php
// extraire les articles https://github.com/j0k3r/graby

function sql($query, $array=array()) {
	global $db;
	$retour = $db->prepare($query);
	$retour->execute($array);
}
function redirect($url) {
	echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	header('Location: '.$url);
	die;
}
function get_title($url){
  $str = file_get_contents($url);
  if(strlen($str)>0){
    $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
    preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title); // ignore case
    return $title[1];
  }
}
function readingtime($text) {
		$readingtime =null;
	    $word = str_word_count(strip_tags($text));
        $seconds_estimate = floor($word / (200 / 60));
		$hours = intval($seconds_estimate / 3600);
		$minutes=intval(($seconds_estimate % 3600) / 60);
		$secondes=intval((($seconds_estimate % 3600) % 60));
		if($secondes == 60) {$minutes = 1; $secondes = 0;}
		if($minutes == 60) {$hours = 1; $minutes = 0;}
		if($hours>=1) { $readingtime .= $hours.'h ';}
		if($minutes>=1) { $readingtime .= (ceil($minutes/5)*5).'min ';}
		if($minutes<1 && $hours == 0) { $readingtime .= (ceil($secondes/5)*5).'s ';}
		return $readingtime;
}


if(!file_exists('data')) {
	mkdir('data');
	$db = new PDO('sqlite:data/data.db');
	$db->query('CREATE TABLE IF NOT EXISTS content ( 
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	title TEXT,
	url TEXT,
	content TEXT,
	time INTEGER,
	read INTEGER);');
}
else {
	$db = new PDO('sqlite:data/data.db');
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<style>
.block {
	box-shadow: 0 1px 4px rgba(0, 0, 0, .10);
	border-radius:0.25em;
    background: #fff;
	border: 1px solid #e5e5e5;
	box-sizing:border-box;
	padding: 10px;
	margin: 5px;
}
a {
color:#4496cc;
text-decoration:none;
}

a:hover {
color:#337199;
text-decoration:none;
}
.action {
	float:right;
	text-decoration:none;
	color:grey;
}
nav {
	margin:auto;
	display:flex;
	flex-direction:row;
	justify-content:space-around;
	margin-bottom:1em;
}
nav a {
	text-decoration:none;
	font-size:1.4em;
}
input[type="url"] {
	width:100%;
}
input[type="submit"] {
	width:100%;
}
</style>
</head>
<body>
<?php
echo '<nav><a href="?">Non lus</a> <a href="?do=archive">Archives</a> <a href="?do=add">Ajouter</a></nav>';
if(isset($_GET['do'])) {
	switch($_GET['do']) {
		case 'archive':
			$something = $db->query('SELECT * FROM content WHERE read=1 ORDER BY time DESC');
			while($data = $something->fetch()) {
				echo '<div class="block">
						<p><a href="'.$data['url'].'" target="_blank">'.$data['title'].'</a> 
						<i><a href="'.$data['url'].'" target="_blank">'.parse_url($data['url'], PHP_URL_HOST).'</a></i>
						<a href="?unread='.$data['id'].'" class="action">↶</a></p>
					</div>';
			}
		break;
		case 'add':
			echo '<form method="GET" action="?do=add">
					<input type="url" name="url" id="url">
					<input type="hidden" name="do" value="add"/>
					<input type="submit"/>
				</form>';
			if(isset($_GET['url'])) {
				sql('INSERT INTO content(title, url, content, time, read) VALUES (?, ?, ?, ?, ?)', array(get_title($_GET['url']), $_GET['url'], '', time(), 0));
				redirect('index.php');
			}
		break;
	}
}
elseif(isset($_GET['id'])) {
	$something = $db->prepare('SELECT * FROM content WHERE id=?');
	$something->execute(array($_GET['id']));
	$data = $something->fetch();
	echo '<h1>'.$data['title'].'</h1>';
	echo '<p><i><a href="'.$data['url'].'" target="_blank">'.parse_url($data['url'], PHP_URL_HOST).'</a></i> <a href="?read='.$data['id'].'">✔</a></p>';
	echo $data['content'];
}
else {
	if(isset($_GET['read'])) {
		sql('UPDATE content SET read=1 WHERE id=?', array($_GET['read']));
		redirect('index.php');
	}

	if(isset($_GET['unread'])) {
		sql('UPDATE content SET read=0 WHERE id=?', array($_GET['unread']));
		redirect('index.php');
	}
	$something = $db->query('SELECT * FROM content WHERE read=0 ORDER BY time DESC');
	while($data = $something->fetch()) {
		echo '<div class="block">
				<p><a href="'.$data['url'].'" target="_blank">'.$data['title'].'</a> 
				<i><a href="'.$data['url'].'" target="_blank">'.parse_url($data['url'], PHP_URL_HOST).'</a></i>
				<a href="?read='.$data['id'].'" class="action">✔</a></p>
			</div>';
	}
}
?>
</body>
</html>
