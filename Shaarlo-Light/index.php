<?php
session_start();
header('content-type: text/html; charset=utf-8');
?>
<!doctype html>
<html>
	<head>
	<style>
	article,aside,details,figcaption,figure,footer,header,hgroup,main,nav,section,summary{display:block;}
audio,canvas,video{display:inline-block;}
summary{margin:0 0 .75em;}
html{font-size:100%;}
body{background:#fff;color:#333;font-family:Arial, helvetica, sans-serif;font-size:1.4em;line-height:1.5;margin:0;}
h1{display:block;font-size:1.8571em;line-height:1.6154;margin:1.6154em 0 .8077em;}
h2{display:block;font-size:1.7143em;line-height:1.75;margin:1.75em 0 .875em;}
h3{display:block;font-size:1.5714em;line-height:1.909;margin:1.909em 0 .9545em;}
h4{display:block;font-size:1.4286em;line-height:1.05;margin:2.1em 0 1.05em;}
h5{display:block;font-size:1.2857em;line-height:1.1667;margin:2.3334em 0 1.1667em;}
h6{display:block;font-size:1.1429em;line-height:1.3125;margin:2.625em 0 1.3125em;}
blockquote,q{quotes:none;}
blockquote:before,blockquote:after,q:before,q:after{content:"";}
blockquote em, blockquote i {font-style:normal;}
pre{white-space:pre-wrap;word-wrap:break-word;}
code,kbd,pre,samp,tt,var{font-family:menlo,monaco,consolas,"courier new",monospace;font-style:normal;line-height:normal;}
abbr,acronym,dfn{border-bottom:1px dotted;cursor:help;font-style:normal;font-variant: small-caps;}
a:focus{outline:1px dotted;}
img{border:0;}
ul,ol{padding:0 0 0 2em;}
li{margin:0 0 0 2em;}
li ul,li ol{margin:0;}
hr{border:0;border-bottom:1px solid;}
big{font-size:1.25em;}
small,sub,sup{font-size:.875em;}
sub{vertical-align:bottom;top:.5ex;}
sup{vertical-align:top;bottom:1ex;}
del,s,strike{text-decoration:line-through;}
mark{background:#ff0;color:#000;}
fieldset{border:1px solid;padding:1em;}
legend{font-weight:700;padding:0 .25em;}
input,textarea,select,button{font-family:inherit;font-size:1em;line-height:normal;}
input[type=button],input[type=image],input[type=reset],input[type=submit],button[type=button],button[type=reset],button[type=submit]{cursor:pointer;}
table{border:0;border-collapse:collapse;border-spacing:0;table-layout:fixed;margin-bottom:1.5em;}
th,td{border:1px solid;padding:.25em .5em;}
caption{padding:0 0 1em;}
audio,canvas,details,figure,video,address,blockquote,dl,fieldset,form,hr,menu,ol,p,pre,q,table,ul{margin:0 0 1.25em;}
dd{margin:1px 0 1.25em;}
b,strong,dt,th{font-weight:700;}
textarea,caption,th,td{text-align:left;vertical-align:top;}
q:lang(fr){quotes:"« " " »" &#8220; &#8221;;}
q:lang(it){quotes:« » &#8220; &#8221; &#8216; &#8217;;}
q:lang(en){quotes:&#8220; &#8221; &#8216; &#8217;;}
q::before{content:open-quote;}
q::after{content:close-quote;}
sup,sub{vertical-align:0;position:relative;}
h1:first-child,h2:first-child,h3:first-child,h4:first-child,h5:first-child,h6:first-child{margin-top:0;}
p:last-child,ul:last-child,ol:last-child,dl:last-child,blockquote:last-child,pre:last-child,table:last-child{margin-bottom:0;}
li p,li ul{margin-bottom:0;margin-top:0;}
textarea,table,td,th,code,pre,samp,div,p{word-wrap:break-word;hyphens:auto;}
code,pre,samp{white-space:pre-wrap;}
kbd{border:solid 1px;border-top-left-radius:.5em;border-top-right-radius:.5em;padding:0 .25em;}
abbr[title]{border-bottom:dotted 1px;cursor:help;}
a:link img,a:visited img,img{border-style:none;}
blockquote,q,cite,i{font-style:italic;}
sub,sup,code{line-height:1;}
ins,u{text-decoration:underline;}
img,table,td,blockquote,code,pre,textarea,input{height:auto;max-width:100%;}
/*STRUCTURE */

body {
	/*font-family:Garamond, Baskerville, "Baskerville Old Face", "Hoefler Text", "Times New Roman", serif;*/
	font-family: "Book Antiqua", Palatino, "Palatino Linotype", "Palatino LT STD", Georgia, serif;
}
a {
	color:#000;
}

.header {
position:fixed;
}
.header h1 {
font-size:1.1em;
margin:auto;
}
.header a {
text-decoration:none;
}
body main {
	max-width:600px;
	margin:auto;
}
blockquote {
	border-left:5px solid #f1f0ea;
	padding-left:5px;
}

code {
	background:#f1f0ea;
	border:1px solid #dddbcc;
	border-radius:3px;
	display:block;
	margin:0 0 40px;
	overflow:auto;
	padding:15px 20px;
}
sup a {
	font-size:.75em;
	text-decoration:none;
}
:target, mark {
	background:#fff8ab;
	color:#666
}

article {
	hyphens:auto;
	word-break:normal;
}
article main {
margin-top:-1.5em;
}
article h1 a {
text-decoration:none;
margin:0;
}
.permalink {
margin-top:-50em;
text-decoration:none;
font-size:0.7em;
}
time {
	color:#ccc;
}
footer {
	margin:auto;
	text-align:center;
}

footer a {
	color:#727272;
	font-size:.75em;
	margin-top:1em;
	text-align:center;
	text-decoration:none;
}
</style>
	<title>Flux des Shaarlis</title>
	</head>
</body>
<header class="header">
<h1><a href="index.php">./shaarli.sh</a></h1> <a href="?action=search" title="search">&#128270;</a> <a href="?action=add" title="add">✚</a> <a href="?action=refresh" title="refresh">&#8635;</a>
<?php
$min = (isset($_GET['min'])) ? $_GET['min']: mktime(0, 0, 0, date('m', time()), date('d', time()), date('y', time()));
$max = (isset($_GET['max'])) ? $_GET['max']:  mktime(23, 59, 59, date('m', time()), date('d', time()), date('y', time()));
?>
<a href="?min=<?php echo $min-(24*60*60);?>&max=<?php echo $max-(24*60*60); ?>" title="go yesterday">&#8592;</a>
<a href="?min=<?php echo $min+(24*60*60);?>&max=<?php echo $max+(24*60*60); ?>" title="go tomorrow">&#8594;</a>
</header>
<main>
<?php
function sql($query, $array=array()) {
	global $bdd;
	$retour = $bdd->prepare($query);
	$retour->execute($array);
}
function redirect($url) {
	// echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	header('Location: '.$url);
	die;
}
if(!file_exists('config.php')) {
	echo '<a href="?action=install">Go install!</a>';
}
else {
	include 'config.php';
	$bdd = new PDO('sqlite:database.db');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
define('HEAD_ELEMENTS', serialize(array(
'title', 'dateCreated', 'dateModified', 'ownerName', 'ownerEmail',
'ownerId', 'docs', 'expansionState', 'vertScrollState', 'windowTop',
'windowLeft', 'windowBottom', 'windowRight'
)));
function libopml_parse_string($xml, $strict = true) { #source https://github.com/marienfressinaud/lib_opml/blob/master/src/marienfressinaud/lib_opml.php
	$dom = new DOMDocument();
	$dom->recover = true;
	$dom->strictErrorChecking = false;
	$dom->loadXML($xml);
	$dom->encoding = 'UTF-8';
	$opml = simplexml_import_dom($dom);
	$array = array(
		'version' => (string)$opml['version'],
		'head' => array(),
		'body' => array()
	);
	// First, we get all "head" elements. Head is required but its sub-elements
	// are optional.
	foreach ($opml->head->children() as $key => $value) {
		if (in_array($key, unserialize(HEAD_ELEMENTS), true)) {
			$array['head'][$key] = (string)$value;
		}
	}
	// Then, we get body oulines. Body must contain at least one outline
	// element.
	$at_least_one_outline = false;
	foreach ($opml->body->children() as $key => $value) {
		if ($key === 'outline') {
			$at_least_one_outline = true;
			$array['body'][] = libopml_parse_outline($value, $strict);
		}
	}
	return $array;
}
function libopml_parse_outline($outline_xml, $strict = true) {
	$outline = array();
	// An outline may contain any kind of attributes but "text" attribute is
	// required !
	$text_is_present = false;
	foreach ($outline_xml->attributes() as $key => $value) {
		$outline[$key] = (string)$value;
		if ($key === 'text') {
		$text_is_present = true;
		}
	}
	foreach ($outline_xml->children() as $key => $value) {
		// An outline may contain any number of outline children
		if ($key === 'outline') {
			$outline['@outlines'][] = libopml_parse_outline($value, $strict);
		}
	}
	return $outline;
}
function refresh() {
	global $bdd;
	$retour = $bdd->query('SELECT * FROM user ORDER BY id DESC');
	while($donnees = $retour->fetch()) {
		$fluxrss=simplexml_load_file($donnees['rss']);
		foreach($fluxrss->channel->item as $item){
			// echo '<li><a href="'.(string)$item->link.'">'.(string)$item->title.'</a> <i>publié le'.(string)date('d/m/Y à G\hi',strtotime($item->pubDate)).'</i></li>';
			$query = $bdd->query('SELECT count(url) as nb  FROM flux where url="'.(string)$item->link.'" AND id_user='.$donnees['id'].''); //si pas dans la BDD, on enregistre
			$count= $query->fetch();
			if($count['nb'] == 0) {
			$id=$donnees['id'];
			$title=(string)($item->title);
			$date=(string)strtotime($item->pubDate);
			$description = (string)$item->description;
			$link = (string)$item->link;
			sql('INSERT INTO `flux`(`id_user`, `title`, `timestamp`, `content`, `url`) VALUES (?, ?, ?, ?, ?)', array($id, $title, $date, $description, $link));
			}
		}
	}
	echo "synchronization is complete";
	redirect('index.php');
}
if(isset($_GET['action'])){
	switch ($_GET['action']){
		case 'refresh':
			refresh();
		break;
		case 'opml':
			if(((fileatime('shaarlis.opml')+60*60*24) < time()) OR !file_exists('shaarlis.opml')) {
				$file = '<opml version="1.1">'.PHP_EOL;
				$file .= "\t".'<head>'.PHP_EOL;
				$file .= "\t\t".'<title>Flux RSS shaarlis</title>'.PHP_EOL;
				$file .= "\t\t".'<dateCreated>2014-07-09T20:34:03+00:00</dateCreated>'.PHP_EOL;
				$file .= "\t\t".'<dateModified>'.date('c').'</dateModified>'.PHP_EOL; // $file .= "\t\t".'<dateCreated>2014-07-09T22:40:47+01:00</dateCreated>'.PHP_EOL;
				$file .= "\t".'</head>'.PHP_EOL;
				$file .= "\t".'<body>'.PHP_EOL;
				$query = $bdd->query('SELECT * FROM `user`');
				$query->execute();
				while($donnees = $query->fetch()) {
					$file .= '<outline text="'.$donnees['name'].'" htmlUrl="'.$donnees['rss'].'/" xmlUrl="'.$donnees['rss'].'"/>'.PHP_EOL;
				}
				$file .= "\t".'</body>'.PHP_EOL;
				$file .= '</opml>'.PHP_EOL;
				file_put_contents('shaarlis.opml', $file);
			}

		break;
		case 'install':
			echo '<form method="post" action="?action=install"><input type="password" name="pwd" id="pwd" placeholder="Your administration password"/><input type="submit"/></form>';
			if(isset($_POST['pwd'])) {
				file_put_contents('config.php', '<?php define("PWD", "'.$_POST["pwd"].'");');
				$bdd = new PDO('sqlite:database.db');
				$bdd->query('CREATE TABLE flux (id INTEGER PRIMARY KEY,id_user int(20) NOT NULL,title text NOT NULL,timestamp int(20) NOT NULL, content text NOT NULL, url text NOT NULL)');
				$bdd->query('CREATE TABLE user (id INTEGER PRIMARY KEY,name text NOT NULL,rss text NOT NULL)');
			echo 'It’s Okay! Enjoy!';
		}
		break;
		case 'search':
			echo '<form action="index.php"><input type="search" placeholder="search" id="q" name="q"><input type="submit"/></form>';
		break;
		case 'add':
			if(isset($_GET['logout'])) {$_SESSION = array();session_destroy();redirect('index.php?action=add');}
			if(isset($_SESSION['login'])) {
				echo '<a href="?action=add&logout">Logout</a>';
				echo '<form action="?action=add" method="post"><input type="text" placeholder="Name" id="name" name="name"><input type="text" placeholder="RSS link" id="url" name="url"/><input type="submit"/></form>';
				echo '<form action="?action=add" method="post"><input type="url" placeholder="OPML file" id="OPML" name="OPML"><input type="submit"/></form>';
				if(isset($_POST['name']) && isset($_POST['url'])) {
					sql('INSERT INTO `user` (`name`, `rss`) VALUES (?, ?);', array($_POST['name'], $_POST['url']));
					refresh();
				}
				if(isset($_POST['OPML'])) {
					$array = libopml_parse_string(file_get_contents($_POST['OPML']))['body'];
					for ($i = 0; $i < sizeof($array); $i ++) {
						sql('INSERT INTO `user` (`name`, `rss`) VALUES (?, ?);', array($array[$i]['text'], $array[$i]['xmlUrl']));
					}
					refresh();
				}
				if(isset($_GET['delete']) && $_GET['token'] == $_SESSION['token']) {
					sql('DELETE FROM user WHERE id=?', array($_GET['delete']));
					sql('DELETE FROM flux WHERE id_user=?', array($_GET['delete']));
					redirect('index.php?action=add');
				}
				$query = $bdd->query('SELECT * FROM `user`');
				$query->execute();
				echo '<ul>';
				while($donnees = $query->fetch()) {
					echo '<li>'.$donnees['name'].'<a href="?action=add&delete='.$donnees['id'].'&token='.$_SESSION['token'].'">&#10060;</a></li>';
				}
				echo '</ul>';
			}
			else {
				if(isset($_POST['password']) AND $_POST['password'] == PWD) {
					$_SESSION['login'] = true;
					$_SESSION['token'] = md5(uniqid(rand(), true));
					redirect('index.php?action=add');
				}
				echo '<form action="?action=add" method="post"><input type="password" id="password" name="password" placeholder="password"><input type="submit"/></form>';
			}
		break;
	}
}	
else {
	if(isset($_GET['permalink'])) {
		$query = $bdd->prepare('SELECT * FROM `flux`,`user` WHERE `user`.`id` = `flux`.`id_user` AND url=? ORDER BY timestamp ASC');
		$query->execute(array(base64_decode($_GET['permalink'])));
		$donnees = $query->fetch();
			echo '<article><header>';
			echo '<h1><a href="?permalink='.base64_encode($donnees['url']).'">#</a> - <a href="'.$donnees['url'].'" title="'.$donnees['url'].'">'.$donnees['title'].'</a></h1>';
			echo '</header><main>';
		$query = $bdd->prepare('SELECT * FROM `flux`,`user` WHERE `user`.`id` = `flux`.`id_user` AND url=? ORDER BY timestamp ASC');
		$query->execute(array(base64_decode($_GET['permalink'])));
		while($donnees = $query->fetch()) {
			echo '<span class="author"><a href="?user='.$donnees['id_user'].'">@</a>'.$donnees['name'].'</span> <time>'.date('d/m/y H:m P', $donnees['timestamp']).'</time>';
			echo '<p>'.html_entity_decode($donnees['content']).'</p>';
		}
		echo '</main></article>';
	}
	elseif(isset($_GET['user'])) {
		$retour = $bdd->prepare('SELECT * FROM `flux`,`user` WHERE `user`.`id` = `flux`.`id_user` AND id_user=? ORDER BY timestamp DESC');
		$retour->execute(array($_GET['user']));
		while($donnees = $retour->fetch()) {
			echo '<article><header>';
			echo '<h1><a href="?permalink='.base64_encode($donnees['url']).'">#</a> - <a href="'.$donnees['url'].'" title="'.$donnees['url'].'">'.$donnees['title'].'</a></h1>';
			echo '<span class="author"><a href="?user='.$donnees['id_user'].'">@</a>'.$donnees['name'].'</span> <time>'.date('d/m/y H:m P', $donnees['timestamp']).'</time><p>'.html_entity_decode($donnees['content']).'</p>';
			echo '</header></article>';
		}
	}
	elseif(isset($_GET['q']) && $_GET['q'] != NULL) {
		$query = htmlspecialchars($_GET['q']); 
		$motrecherche=explode(' ',$query); 
	//on créée une variable
		foreach($motrecherche as $query) {
			@$where.='title LIKE :query OR ';
			@$where.='content LIKE :query OR ';
		}
		$where=substr($where, 0,-4); 
		$retour = $bdd->prepare("SELECT * FROM flux, user WHERE `user`.`id` = `flux`.`id_user` AND (".$where.")  ORDER BY timestamp DESC");
		$retour->execute(array('query'=>'%'.$query.'%'));
		$nb_resultats = $retour->rowCount(); 
		if($nb_resultats != 0) {
			while($donnees = $retour->fetch()) {
				echo '<article>';
				echo '<header>';
				echo '<h1><a href="?permalink='.base64_encode($donnees['url']).'">#</a> - <a href="'.$donnees['url'].'" title="'.$donnees['url'].'">'.$donnees['title'].'</a></h1>';
				echo '</header>';
				echo '<main>';
				echo '<span class="author"><a href="?user='.$donnees['id_user'].'">@</a>'.$donnees['name'].'</span> <time>'.date('d/m/y H:m P', $donnees['timestamp']).'</time>';
				echo '<p>'.html_entity_decode($donnees['content']).'</p>';
				echo '</main';
				echo '</article>';
			} 
		}
	}
	else {
		$min = (isset($_GET['min'])) ? $_GET['min'] : mktime(0, 0, 0, date('m', time()), date('d', time()), date('y', time()));
		$max = (isset($_GET['max'])) ? $_GET['max'] :  mktime(23, 59, 59, date('m', time()), date('d', time()), date('y', time()));
		$retour = $bdd->prepare('SELECT url, title FROM `flux`,`user` WHERE `user`.`id` = `flux`.`id_user` AND (timestamp > ? AND timestamp < ?) GROUP BY url ORDER BY timestamp DESC');
		$retour->execute(array($min, $max));
		while($donnees = $retour->fetch()) {
			echo '<article>';
			echo '<header>';
			echo '<h1><a href="?permalink='.base64_encode($donnees['url']).'">#</a> - <a href="'.$donnees['url'].'" title="'.$donnees['url'].'">'.$donnees['title'].'</a></h1>';
			echo '</header>';
			echo '<main>';
			$query = $bdd->prepare('SELECT * FROM `flux`,`user` WHERE `user`.`id` = `flux`.`id_user` AND url=? ORDER BY timestamp ASC');
			$query->execute(array($donnees['url']));
			while($donnees = $query->fetch()) {
				echo '<span class="author"><a href="?user='.$donnees['id_user'].'">@</a>'.$donnees['name'].'</span> <time>'.date('d/m/y H:m P', $donnees['timestamp']).'</time>';
				echo '<p>'.html_entity_decode($donnees['content']).'</p>';
			}
			echo '</main';
			echo '</article>';
		}
	}
}
?>
</main>
</body>
</html>