<?php
if(isset($_SESSION['admin'])) {
	echo "<ul>";
	$listdir = scandir('./');
	foreach($listdir as $dir) {
		if(is_dir($dir) == true and $dir != '.' and $dir != '..' and $dir != 'app') {
			$config = unstore($dir.'/.config');
			$color = (foldersize($dir)>MAX_SIZE_DIR) ? 'red' : 'green';
			echo '<li><a href="'.$dir.'">'.$dir.'</a> <meter value="'.foldersize($dir).'" min="0" max="'.MAX_SIZE_DIR.'"></meter>
			<span style="color:'.$color.'">'.octectsconvert(foldersize($dir)).'/'.octectsconvert(MAX_SIZE_DIR)."</span> ".
			sendmail($config['mail'])."
			</li>";
		}
	}
	echo "</ul>";
	echo '<a href="?admin&logout='.$_SESSION['admin_token'].'">Déconnexion</a>';
	if(isset($_GET['logout']) and $_GET['logout']==$_SESSION['admin_token']) {
		unset($_SESSION['admin']);
		header("Location: index.php");
	}
}
else {
	if(!file_exists("app/inc/config.php")) {
		include("app/tpl/admin_install.php");
		if(isset($_POST['password'])) {
			file_put_contents('app/inc/config.php', '<?php define("MAX_SIZE_DIR", 1000000000); #IN OCTETS
			define("PASSWORD_ADMIN", "'.sha1($_POST['password']).'"); # hash in sha1()?>');
			header('Location: ?admin');
		}
	}
	else {
		include("app/tpl/admin_login.php");
		if(isset($_POST['password']) and sha1($_POST['password']) == PASSWORD_ADMIN) {
			$_SESSION['admin'] = true;
			$_SESSION['admin_token'] = md5(uniqid(rand(), true));
			header("Location: ?admin");
		}
	}
}
?>