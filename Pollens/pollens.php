<?php
$pollens = json_decode(json_decode(file_get_contents('https://www.pollens.fr/load_vigilance_map'), true)['vigilanceMapCounties'], true)['73']['riskSummary'];  
	
	$pollens = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $pollens);
	$pollens = preg_replace('/^[ \t]*[\r\n]+/m', '', $pollens);
	$pollens = preg_replace('/^.+\n/', '', $pollens);
	$pollens = preg_replace('/^.+\n/', '', $pollens); 
	$pollens = preg_replace('/^.+\n/', '', $pollens);                                                                                                   
	$pollens = str_replace('<svg width=" 297px" height="513px" style="overflow: visible; width: 100%; height: 100%;"></svg>', '', $pollens);
	$pollens = strip_tags($pollens, '<svg><g><rect><line><text><tspan>');
//echo $pollens;
	$array = json_decode(json_encode(simplexml_load_string($pollens, "SimpleXMLElement", LIBXML_NOCDATA)), true);

	$i = 0;
	$content = array();
	while($i <= 18) {
		$name = trim($array['g']['2']['g'][$i]['text']['tspan']);
		$width = trim($array['g'][0]['rect'][$i]['@attributes']['width']);
		if($width == 0) {
			$content[$i]['risk'] = 0;
			$content[$i]['color'] = 'brown';
			$content[$i]['text'] = 'nul';
			$content[$i]['name'] = $name;
		}
		elseif($width == 28.8) {
			$content[$i]['risk'] = 1;
			$content[$i]['color'] = 'rgb(0,255,0)';
			$content[$i]['text'] = 'très faible';
			$content[$i]['name'] = $name;
		}
		elseif($width == 57.6) {
			$content[$i]['risk'] = 2;
			$content[$i]['color'] = 'rgb(0,176,80)';
			$content[$i]['text'] = 'faible';
			$content[$i]['name'] = $name;
		}
		elseif($width == 86.4) {
			$content[$i]['risk'] = 3;
			$content[$i]['color'] = 'rgb(255,255,0)';
			$content[$i]['text'] = 'moyen';
			$content[$i]['name'] = $name;
		}
		elseif($width == 115.2) {
			$content[$i]['risk'] = 4;
			$content[$i]['color'] = 'rgb(247,150,70)';
			$content[$i]['text'] = 'fort';
			$content[$i]['name'] = $name;
		}
		elseif($width == 144) {
			$content[$i]['risk'] = 5;
			$content[$i]['color'] = 'red';
			$content[$i]['text'] = 'très fort';
			$content[$i]['name'] = $name;
		}
		else {
			$content[$i]['risk'] = 'no data';
			$content[$i]['color'] = '#000';
			$content[$i]['text'] = 'no data';
			$content[$i]['name'] = $name;
		}
		$i++;
	}
print_r($content);
echo 'Source : <a href="https://www.pollens.fr/">RNSA</a>';
