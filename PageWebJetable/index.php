<?php
include 'app/Parsedown.php';
include 'app/functions.php';
if(isset($_POST['content'])) {
	$Parsedown = new Parsedown();
	$Parsedown->setSafeMode(true);
	$txt = $Parsedown->text($_POST['content']);
	$namefile = int_to_alph(time()).'.html';
	file_put_contents('data/content/'.$namefile, create_html($_POST['title'], $txt));
	header('Location: data/content/'.$namefile);
}
?>
<!doctype html>
<html>
<head>
	<title>Vite dit</title>
<link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
<script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
	<style>
	textarea, input {display:block;}
	input {width:100%;}
	</style>
</head>
<body>
	<form method="post" action="index.php">
	<input type="text" name="title" id="title">
	<textarea id="content" name="content"/></textarea>
	<input type="submit"/>
	</form>
<script>
var easyMDE = new EasyMDE ({
	element: document.getElementById('content'),
	promptURLs: true,
	uploadImage: true,
	imageUploadEndpoint: 'upload.php',
	imageMaxSize: 1024*1024*20,
});
</script>
</body>
</html>
