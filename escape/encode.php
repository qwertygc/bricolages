<?php
$from = 'ABCDEFGHIJKLMNOPKRSTUVWXYZ';
$to = 	'AZERTYUIOPQSDFGHJKLMWXCVBN';
?>
<?php
function normaliza ($string){
    $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ
ßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $b = 'aaaaaaaceeeeiiiidnoooooouuuuy
bsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $string = utf8_decode($string);    
    $string = strtr($string, utf8_decode($a), $b);
    return utf8_encode($string);
} 
if(!function_exists("stritr")){
    function stritr($string, $one = NULL, $two = NULL){
/*
stritr - case insensitive version of strtr
Author: Alexander Peev
Posted in PHP.NET
*/
        if(  is_string( $one )  ){
            $two = strval( $two );
            $one = substr(  $one, 0, min( strlen($one), strlen($two) )  );
            $two = substr(  $two, 0, min( strlen($one), strlen($two) )  );
            $product = strtr(  $string, ( strtoupper($one) . strtolower($one) ), ( $two . $two )  );
            return $product;
        }
        else if(  is_array( $one )  ){
            $pos1 = 0;
            $product = $string;
            while(  count( $one ) > 0  ){
                $positions = array();
                foreach(  $one as $from => $to  ){
                    if(   (  $pos2 = stripos( $product, $from, $pos1 )  ) === FALSE   ){
                        unset(  $one[ $from ]  );
                    }
                    else{
                        $positions[ $from ] = $pos2;
                    }
                }
                if(  count( $one ) <= 0  )break;
                $winner = min( $positions );
                $key = array_search(  $winner, $positions  );
                $product = (   substr(  $product, 0, $winner  ) . $one[$key] . substr(  $product, ( $winner + strlen($key) )  )   );
                $pos1 = (  $winner + strlen( $one[$key] )  );
            }
            return $product;
        }
        else{
            return $string;
        }
    }/* endfunction stritr */
}/* endfunction exists stritr */ 
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<style>
body {
max-width:80%;
margin:auto;
}
* {
	color:green;
	font-family: Lucida Console, Lucida Sans Typewriter, monaco, Bitstream Vera Sans Mono, monospace;
	background:#000;
}
textarea,input {display:block;border:1px solid green; width:100%; margin: 1em; font-size:1.4em;}
textarea {height:5em; padding:0.4em}
input {height:2em;}
</style>
</head>
<body>
<p>hacker@computer ~$ <span style="color:#fff; font-size:1.5em;">&#9646;</span></p>
<form action="encode.php" method="post">
	<textarea id="clair" name ="clair"></textarea>
	<input type="submit" value="Encoder" name="encoder" id="encoder"/>
</form>
<?php
if(isset($_POST['encoder'])) {
	echo stritr(strtoupper($_POST['clair']), $from, $to);
}
?>
<form action="encode.php" method="post">
	<textarea id="chiffre" name ="chiffre"></textarea>
	<input type="submit" value="Décoder" name="decoder" id="decoder"/>
</form>
<?php
if(isset($_POST['decoder'])) {
		echo strtoupper(normaliza(stritr($_POST['chiffre'], $to, $from)));
}
?>
</body>
</html>
