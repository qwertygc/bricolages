Quelques bricolages, qui ne méritent pas de créer un dépôt spécifique. La plupart des logiciels sont abandonnées.

# Alexandra

Alexandra est un gestionnaire de prêt de livres. Alexandra vient du nom de la bibiothèque d'Alexandrie et de la chanson "Alexandrie, Alexandra" de Claude François. Le projet à commencé le 25/03/12. Il est codé en PHP et MySQL.


# Internet National
Bricolage de 2012-14. WIP artistique d'un internet nationalisé.

## Loups Garou
Bricolage de 2015 pour jouer au loup-garou

# Print
Bricolage de 2015, impression recto/verso

# saint2csv
Bricolage de juillet 2017, convertir la liste des saints (calendrier romain) en CSV

# Batch-download
Fork de https://gist.github.com/yosko/7989061 du 4 janvier 2014, télécharger des fichiers en série

# Calendar
bricolage du 29 août 2015 : calendrier républicain. Trouvé je ne sais où. Il y a aussi le calendrier romain.

# Editor Word
Bricolage du 16 avril 2014. Editeur de texte en JS.

# Epub
Bricolage du 3 septembre 2015, pour générer des epubs en PHP

# Nuit debout
Bricolage du 20 avril 2016, pour générer le calendrier nuit debout

# Portail
Bricolage du 10 décembre 2016, pour générer un portail à a partir de flux RSS

# Chatons CGU

bricolage du 17 mai 2017 de CGU de chatons

# Split

Bricolage du 3 février 2019 Partager sur un même écran plusieurs pages web.

# .class.php
Lessdb (base de données en fichier), atom (générer un fichier atom) et form (génération de formulaire) sont des class PHP qui m'ont facilité le développement

# CAPTCHA

Un captcha sous forme de questions/réponses

# Cookie

Afficher un message en rapport avec la loi européenne sur les cookies

# LDFIF2VCF

Convert LDIF File to VCF File

# Tweetomatic

Diviser un long texte pour publier sur Twitter.


# ZDS-Content

Petit bricolage pour avoir tout un tutoriel ou article de ZDS sur une seule page. 

# Chat
Un logiciel pour chatter

# Corpus

Petit script permettant d’étudier la fréquence de mots dans un texte.

# deontologie-charte-blog
Un habeas corpus du bloggeur (très très vieux projet)

# CV
Une base de CV sémantique.

# NotepadPHP

Bloc-note minimaliste

# NanoWriMo


Proof of Concept d'un NaNoWriMo collaboratif

# Bonjour

Logiciel pour faire un site comme bonjouraxolotl.fr

# Devis

Calculer un devis, selon la méthode de https://zestedesavoir.com/tutoriels/341/se-faire-payer-daccord-mais-combien/

# languedebois

Générateur de discours politique

# Roulette

Roulette Russe

# Services

Proposer des services entre voisins

#HTML5 Bingo

A bingo board for mobile and desktop

# Validation_ZDS

Un petit logiciel pour aider à la rédaction du feedback de contenus sur ZestedeSavoir.com


# pwd

Générateur de mot de passe facile à retenir.

# DodoTime

DodoTime est une application permettant de voir l'heure de réveil optimum.

# Hexa

Convertir un texte en couleur hexa

# Je suis Charlie

Pour générer des images dans le style « je suis charlie » (projet de 2015).

# MD Editor

Un petit éditeur de markdown sans prétention.


# Munch Color
Statistique à propos d'un code source. D'après un code de Silversthem

# Upload

Envoyez des fichiers sur votre serveur


# Sharlo Light
Une variante du logiciel Shaarlo en version plus KISS.

# OpMyWeb

Installation de logiciels.

# Portail
Un petit portail pour afficher les derniers flux de sites.

# Muscardin
inième moteur de blog statique.

# Peck
Blog en PHP

# Galerie.php
Une petite galerie basé sur Proton du Hollandais Volant

# Pixel
Manipulation d'images

# OpenFTP
Permet d’avoir un dossier ouvert à tous tant en lecture qu’en écriture

# FTP.php
Lister les fichiers d'un répertoire.

# AudioPlayer
 Un simple lecteur en HTML5

# Blog statique PHP

Un essai de blog statique

# DL
Un petit bricolage pour tracker les téléchargements d'un fichier

# Smoothie
Générateur de smoothies

# PandocWebsite
Générer un site statique avec pandoc

# DOI

Rechercher des documents par DOI

# Escape

Jouer avec le code césar

# Dico

Dico est un logiciel pour gérer son propre dictionnaire.


# Money

Création d'un système de banque.

# VerdaProxy
Petit fork d'un webproxy, nécessaire pour contourner la censure au lycée !

# Podcast

Petit générateur de fichier xml de podcast

# Bildumak
Linkt.ree like

# Package tracker
Bricolage de 2021 pour garder une trace des liens des colis

# BookSearch

Application permettant de chercher des livres en scannant le codebar

# PressDB

Bricolage pour jouer avec la presse

# WordleSolver

Aide pour résoudre le jeu Wordle

# Mastofeed

Extraire les données d'un flux Mastodon

# QRCode

Générer et extraire des données QRCode
