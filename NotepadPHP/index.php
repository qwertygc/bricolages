﻿<?php
if(isset($_POST['save'])) {
	file_put_contents('posts/'.$_POST['title'].'.md', $_POST['textarea']);
}
if(isset($_GET['delete'])) {
	unlink('posts/'.base64_decode($_GET['delete']).'.md');
}
if(isset($_GET['filename'])) {
	$title = base64_decode($_GET['filename']);
	$textarea = file_get_contents('posts/'.base64_decode($_GET['filename']).'.md');
}
else {
	$title = null;
	$textarea = null;
}
?>
<!DOCTYPE HTML>
<html lang="fr">
<head>
    <title>Editor</title>
<link rel="stylesheet" href="editor/style/style.css"/>
</head>
   <body>
   <ul class="list_files">
		<li><a href="?">Nouvelle note</a></li>
		<?php
		foreach (glob('posts/*.md') as $filename) {
			$file = explode('.md', basename($filename))[0];
			echo '<li><a href="?filename='.base64_encode($file).'">'.$file.'</a> <a class="icon-trash" href="?delete='.base64_encode($file).'"><span class="visually-hidden">Reset</a></li>';
		}
	?>
	</ul>
		<form id="editor" method="post" action="index.php">
			<input type="text" name="title" id="title" Placeholder="Title" value="<?php echo $title; ?>"/>
			<a onclick="insertTag('**','**','textarea');" class="icon-bold"><span class="visually-hidden">Bold</span></a>
			<a onclick="insertTag('*','*','textarea');" class="icon-italic"><span class="visually-hidden">Italic</span></a>
			<a onclick="insertTag('\n>','','textarea');" class="icon-quote"><span class="visually-hidden">Quote</span></a>
			<a onclick="insertTag('[','](http://)','textarea', 'link');" class="icon-link"><span class="visually-hidden">Link</span></a>
			<a onclick="insertTag('![','](http://)','textarea', 'picture');" class="icon-picture"><span class="visually-hidden">Picture</span></a>
			<a onclick="insertTag('\n* ','','textarea');" class="icon-list-bullet"><span class="visually-hidden">List-Bullet</span></a>
			<a onclick="insertTag('\n1. ','','textarea');" class="icon-list-numbered"><span class="visually-hidden">List-Numbered</span></a>
			<div style="float:right">
			<a onclick="toggleFullScreen();" class="icon-resize-full-alt"><span class="visually-hidden">Full Screen</span></a>
			<button class="icon-floppy" type="submit" name="save" value="save"><span class="visually-hidden">Save</span></button>
			<button class="icon-trash" type="reset"><span class="visually-hidden">Reset</span></button>
			<a onclick="javascript:toggle('preview')" class="icon-eye"><span class="visually-hidden">Preview</span></a>
			<a onclick="javascript:toggle('info')" class="icon-help-circled"><span class="visually-hidden">Information</span></a>
			</div>
			<textarea id="textarea" name="textarea" rows="20" cols="40" autofocus placeholder="typing your text" role="application"><?php echo $textarea; ?></textarea>
			<div class="notif" id="notif"></div>
			<output><span id="result"></span><span id="read"></span></output>
			<div class="upload"><div id="dropArea"><span class="icon-upload"><span class="visually-hidden">Upload</span></span></div><div><span id="count"></span></div><div id="resultupload"></div></div>
	</form>
	<div id="preview"></div>
	<div id="info">
		<p>Le constat est simple : il est difficile de rédiger de longs articles. Ainsi, ce plugin va vous fixer des objectifs à atteindre : 750 mots (comme sur 3pages.fr), 1000 mots ou 50000 mots (comme sur NaNoWriMo). Vous prenez ainsi l’habitude d’écrire de longues choses.</p>
		<ul>
			<li>-- → —</li>
			<li><< → «</li>
			<li>>> → »</li>
			<li>ééé → É</li>
			<li>èè → È</li>
			<li>çç → Ç</li>
			<li>àà → À</li>
			<li>' → ’</li>
			<li>... → …</li>
		</ul>
		Merci à <a href="https://github.com/tanakahisateru/js-markdown-extra">JS Markdown Extra</a>.
	</div>
	<script type="text/javascript" src="editor/js/js-markdown-extra.js"></script>
	<script type="text/javascript" src="editor/js/script.js"></script>
	<script type="text/javascript" src="editor/js/upload.js"></script>
   </body>
   </html>