<?php
if(!file_exists('database.db')) {
	try {
			$bdd = new PDO('sqlite:database.db');
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} 
			catch (PDOException $e) {
			echo "<p>Error : " . $e->getMessage() . "</p>";
			exit();
		}
		$bdd->query("CREATE TABLE track (
		id INTEGER PRIMARY KEY,
		ip TEXT,
		ua TEXT,
		timestamp int(20),
		file TEXT);");

}
else {
	try {
			$bdd = new PDO('sqlite:database.db');
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} 
			catch (PDOException $e) {
			echo "<p>Error : " . $e->getMessage() . "</p>";
			exit();
		}
}
if(isset($_GET['file'])) {
	$query = $bdd->prepare('INSERT INTO track(ip, ua, timestamp, file) VALUES(?,?,?,?)');
	$query->execute(array($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], time(),  $_GET['file']));
	header("Content-type: application/pdf");
	header("Content-Disposition: inline; filename=".$_GET['file']);
	@readfile($_GET['file']);
}
else {
	$something = $bdd->query('SELECT * FROM track');
	$something->setFetchMode(PDO::FETCH_BOTH);
	while($data = $something->fetch()) {
		echo $data['file'].', '.$data['ip'].', '.$data['ua'].', '.date('Y-m-d', $data['timestamp']).'<br>'.PHP_EOL;
	}

}
