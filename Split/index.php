<!doctype html>
<html lang="fr">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Split Web</title>
		<style>
		* {
			box-sizing: border-box;
		font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu, Arial, "Open Sans", "Noto Sans", "Fira Sans", "Droid Sans", Cantarell,"Helvetica Neue",sans-serif,"Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Emoji Regular";
		}
		body {
			height:100vh;
			margin:0;
			padding:0;
		}
		body.vertical {
			display: flex;
		}
		body .horizontal {
			width:100%;
		}
		.split {
			border: none;
			overflow:auto;
		}
		.vertical .split {
			display: flex;
			flex-direction: column;
			justify-content: center;
			flex:1;
		}
		.horizontal .split {
			<?php $number = ($_GET['c'] != '') ? 3 : 2; ?>
			height:calc(100% / <?php echo $number; ?>);
		}
		iframe {
			width:100%;
			border:0;
		}
		.vertical iframe {
			height:100%;
		}
		.horizontal iframe {
			height:99%;
		}
		form {
			max-width:500px;
			margin:auto;
		}
		label, input, select {
			display:block;
			width:100%;
			margin-bottom:1em;
		}
		</style>
	</head>
	<?php
	if(isset($_GET['orientation'])) {
		if($_GET['orientation'] == 'v') {
			$orientation = ' class="vertical"';
		}
		elseif($_GET['orientation'] == 'h') {
			$orientation = ' class="horizontal"';
		}
		else {
			$orientation = '';
		}
	}
	else {
		$orientation = '';
	}
	?>
	<body<?php echo $orientation;?>>
	<?php
	if(isset($_GET['a']) AND isset($_GET['b'])) {
		echo '<div class="split"><iframe src="'.urldecode($_GET['a']).'"></iframe></div>';
		echo '<div class="split"><iframe src="'.urldecode($_GET['b']).'"></iframe></div>';
		if($_GET['c'] != '') {
			echo '<div class="split"><iframe src="'.urldecode($_GET['c']).'"></iframe></div>';
		}
	}
	else {
?>
<form method="get">
<h1>Combiner des pages</h1>
<p>Combinez plusieurs pages web en une seule !</p>
	<label for="a">Première page<sup style="color:red">*</sup> <input type="url" id="a" name="a" required/></label>
	<label for="b">Deuxième page<sup style="color:red">*</sup> <input type="url" id="b" name="b" required/></label>
	<label for="c">Troisième page <input type="url" id="c" name="c"/></label>
	<label for="orientation">
		Orientation
		<select id="orientation" name="orientation">
			<option value="v">Vertical</option>
			<option value="h">Horizontal</option>
		</select>
	</label>
	<input type="submit" value="Créer"/>
</form>
<?php
	}
?>
	</body>
</html>
