<?php
session_start();
error_reporting(-1);
if(!is_dir('data')) {mkdir('data/');}
if(!is_dir('img')) {mkdir('img/');}
if(!file_exists('data/config.php')) {file_put_contents('data/config.php', '<?php define("PWD", "password"); define("TITLE", "photo gallery");');} else {include 'data/config.php';}
if(!file_exists('data/comment.html')) {file_put_contents('data/comment.html', '');}
function location($url) {
	header('Location: '.$url);
	echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	die;
}

echo '<!DOCTYPE html>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="initial-scale=1.0, user-scalable=yes" />
<title>'.TITLE.'</title>
<style>
html{background:#212121;color:#eee;font-size:62.5%;font-family:"Trebuchet MS",Arial,Helvetica,sans-serif}
body{font-size:14px;width:80%;margin:0 auto}
a{text-decoration:none;color:inherit}
a:hover{text-decoration:underline}
header h1{font-weight:400;font-size:1.7em;text-shadow:2px 2px 2px #000}
#axe{background:linear-gradient(to right,#212121,#212121) 0 2px no-repeat,linear-gradient(to right,#646464,#212121) 0 1px no-repeat,linear-gradient(to right,#010101,#212121) 0 0 no-repeat;padding-top:1px}
#list-folders{margin:0 auto;color:#eee}
#list-folders a{color:#fff}
#list-images{text-align:left;padding:20px 0}
#list-images .image_bloc{text-align:center;background:#fff;display:inline-block;margin:1px;line-height:160px;height:160px;width:160px;border:1px solid #666;border-radius:3px;border-color:#000 gray;padding:5px;position:relative}
#list-images .image_bloc .spantop{overflow:hidden;opacity:0;text-shadow:0 0 3px white,0 0 3px #fff;left:0;position:absolute;background:rgba(255,255,255,.8);width:100%;height:0;word-wrap:break-word;top:0;border-radius:3px}
#list-images .image_bloc .spantop .bouton{line-height:32px;height:32px;width:32px;vertical-align:middle;display:inline-block;cursor:pointer;text-decoration:none}
#list-images .image_bloc .spantop img{border:none;text-decoration:none}
#list-images .image_bloc:hover .spantop{opacity:1;transition:ALL .15s ease-out;line-height:160px;height:100%}
#list-images .image_bloc img{border:1px solid gray;vertical-align:middle;max-width:160px;max-height:160px}
#slider{display:none;background:#000;position:fixed;top:0;left:0;right:0;bottom:0;z-index:9}
#slider-box{border:none;position:absolute;top:0;right:0;left:0;bottom:0;text-align:left}
#slider-box-cnt{position:absolute;top:0;right:0;bottom:48px;left:0}
#slider-box-img-bg,#slider-box-img-bg-blur-fallback{background-color:#000;width:100%;height:100%;position:absolute}
#slider-box-img-bg-blur{background-size:100% 100%;width:100%;height:100%;filter:blur(25px) opacity(.2);-webkit-filter:blur(25px) opacity(.2);-ms-filter:blur(25px) opacity(.2)}
#slider-box-img-bg-blur-fallback{background:#000;filter:opacity(0);-webkit-filter:opacity(0);-ms-filter:opacity(0)}
#slider-box-img-wrap{width:100%;height:100%;display:inline-block;position:absolute;text-align:center}
#slider-img{max-width:100%;max-height:100%;width:auto;height:auto;vertical-align:middle}
#slider-img-a{display:block;position:absolute;top:0;bottom:0;left:0;right:0}
#slider-box-bottom{box-sizing:border-box;-moz-box-sizing:border-box;height:48px;bottom:0;left:10px;right:10px;position:absolute;display:flex;align-items:center}
#slider-img-info{flex:1 1 0;box-sizing:border-box;-moz-box-sizing:border-box;height:48px;line-height:48px}
#slider-buttons{flex:1 1 0;line-height:48px;text-align:right}
#slider-buttons a,#slider-box .slider-quit{display:inline-block;height:32px;width:32px;vertical-align:middle;position:relative;border-radius:4px}
#slider-buttons a:active{top:1px}
#slider-box .slider-quit{background-position:0 -64px;position:absolute;top:10px;right:10px;z-index:99}
footer{font-size:.8em;color:silver;text-align:center;margin:20px 0;padding-top:20px;background:linear-gradient(to right,#212121,#212121) 0 2px no-repeat,linear-gradient(to right,#212121,#646464,#212121) 0 1px no-repeat,linear-gradient(to right,#212121,#010101,#212121) 0 0 no-repeat}
label{display:block;width:300px;float:left}
</style>
</head>
<body id="body">
<header>
	<h1><a href="?">'.TITLE.'</a></h1>
</header>';
if(isset($_GET['a'])) {
if(isset($_SESSION['login'])) {
	echo '<a href="?a&logout">Déconnexion</a><h1>Envoyer des images</h1>
	<form method="post" action="?a" enctype="multipart/form-data">        
		<label for="">Fichier : </label><input type="file" name="fichier[]" size="60"><br>
		<input type="submit" value="Envoyer" name="upload">   
	</form>
	<h1>Modifier la configuration</h1>
	<form method="post" action="?a">        
	   <label for="description">Modifier la description : </label><textarea name="description">'.file_get_contents('data/comment.html').'</textarea><br>
	   <label for="title">Modifier le titre du site : </label><input type="text" name="title" id="title" value="'.TITLE.'"/><br>
	  <label for="pwd"> Modifier le mot de passe (laissez vide sinon) : </label><input type="text" id="title" name="pwd" /><br>
		<input type="submit" value="Envoyer" name="config">   
	</form>
	<h1>Mémo de la mise en forme pour la description</h1>
	La mise en page se fait en HTML. Voici un petit mémo pour vous aider :<br>
	&lt;p&gt;&lt;/p&gt; : Pour un paragraphe.<br>
	&lt;hr /&gt; : Tracer une ligne horizontal.<br>
	&lt;br /&gt;: Retour à la ligne.<br>
	&lt;i&gt;&lt;/i&gt; : Texte en italique.<br>
	&lt;b&gt;&lt;/b&gt; : texte en gras.<br>
	&lt;s&gt;&lt;/s&gt; : texte barré.<br>
	&lt;u&gt;&lt;/u&gt; : Souligne un texte.<br>
	&lt;sup&gt; &lt;/sup&gt; : Mise en exposant.<br>
	&lt;sub&gt; &lt;/sub&gt; : Mise en indice.<br>
	<br>
	&lt;ul&gt;<br>
	&lt;li&gt;1er élément de la liste.&lt;/li&gt;<br>
	&lt;li&gt;2ème élément de la liste.&lt;/li&gt;<br>
	&lt;li&gt;3ème élément de la liste.&lt;/li&gt;<br>
	&lt;/ul&gt;<br>
	&lt;h1&gt;&lt;/h1&gt; : titre de niveau 1<br>
	&lt;h2&gt;&lt;/h2&gt; : sous-titre de niveau 2<br>
	&lt;h3&gt;&lt;/h3&gt; : sous-titre de niveau 3<br>
	&lt;h4&gt;&lt;/h4&gt; : sous-titre de niveau 4<br>
	&lt;h5&gt;&lt;/h5&gt; : sous-titre de niveau 5<br>
	&lt;h6&gt;&lt;/h6&gt; : sous-titre de niveau 6<br>
	<br></p>';
		if(isset($_POST['config'])) {
			$pwd = (empty($_POST['pwd'])) ? PWD : $_POST['pwd'];
			file_put_contents('data/comment.html', $_POST['description']);
			file_put_contents('data/config.php', '<?php define("PWD", "'.$pwd.'"); define("TITLE", "'.$_POST['title'].'");');
			location('index.php');
		}
		elseif(isset($_POST['upload'])) {
			$files = array();
			$fdata = $_FILES['fichier'];
			$count = count($fdata['name']);
			if(is_array($fdata['name'])){
				for($i=0;$i<$count;++$i){
					$files[]=array(
						'name'     => $fdata['name'][$i],
						'tmp_name' => $fdata['tmp_name'][$i],
						'type' => $fdata['type'][$i],
					);
				}
			}
			else {$files[]=$fdata;}
			foreach ($files as $file) {
				if(!is_uploaded_file($file['tmp_name'])) {exit('error');}
				$file['name']=date('YmdHis', time()).'_'.$file['name'];
				if(!move_uploaded_file($file['tmp_name'], 'img/'. $file['name']) ) {exit('error');}
				echo $file['name'].'<br>';
			}
			location('index.php');
		}
	elseif(isset($_GET['logout'])){$_SESSION = array();session_destroy(); location('index.php');}
}
else {
	echo '<h1>Se connecter</h1>
<form method="post" action="?a">        
   <label for="password">Rentrer le mot de passe : </label><input type="password" name="password"><br>
<input type="submit" value="Envoyer" name="config">   
</form>';
	if(isset($_POST['password']) && PWD == $_POST['password']) {$_SESSION['login'] = true; location('index.php?a');}
}
}
else {
if(file_exists('data/comment.html')) {echo file_get_contents('data/comment.html');}

echo '<div id="axe">';
	$imgs = scandir('img/');
	array_shift($imgs);
	array_shift($imgs);
		echo '<div id="list-images">';
			foreach ($imgs as $i => $image) {				
				echo '<div id="bloc_'.$i.'" data-img-url="img/'.$image.'" data-img-name="'.$image.'" class="image_bloc">
					<span class="spantop black">
						<span title="Ouvrir Slideshow" class="bouton bouton-slide" onclick="slideshow(\'start\', '.$i.');"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAASdAAAEnQF8NGuhAAAAB3RJTUUH3wEYEBEY2+JKrAAABKRJREFUWMPFV01MG0cU/mbWO7Y3tV3aOMSJgULbgISIIAkBarUSUguKhXzLoZdcEHDJKZFyyiHKIcohp5ySSy6JxAHBIbLqpkhNKkA5RFhRqahsAyIYCLZly4uNsfHuTg/dTd3UODjl50kjjezned987/l7b4AjNlKyFwAwAKYDjqkA2AGglgIwA2ijlHoBuA8YQFTTtACAOQAFkw7iOKV0QNM0FwB+wADceqy3ANapTr1LX4dlRjyB6h9Yj6D+rPjIgqN62njJ+mgzaoBqmlbRsbW19bNbt2511tfXO1wul5ROp3ei0ejm+Ph45NGjRxEA2l6D6rEoAEIAiAA8AH7c7ca3b9++cOXKlVbGmJhKpdTl5eVtp9PJTp8+zRhjfGpqam1wcPDXdDqdr+LyowBmaAmtZXXi+vXrHcPDw+2xWIwPDQ390dbWFhgYGHjW1dX1c29v78yLFy8y3d3d7tHR0UtVppQa4iMA+AJA6/seTU1Nx+/du/ddLBZTfT7fb69fvw4DSAPYBpBNpVKpsbGx9ebm5lqPx3NcFEU6MzOzvkcA8wDe0EoqOTIyctZqtYp37tz5Mx6PrwIovOejAkjdvHnzVTweL/b19TXqF6qOhl1SINTV1X2ayWS0QCAQAlDc5QyeSCRiwWAw3dDQYD916pSjmti0TE949w85efLksZWVle1isVj4kL4vLS1lFEWBzWaTqulDlQBgbW2t4HQ62Z701e22FotF5HI59WMAlEuBMj8/LzPGRK/XW7FBiaLIWlpaaqLRaD4ajcr7lQJ1YmJiOZvNalevXr3AGDPvdtDdu3c9drvd8vLly4TeaqtigOxWXKFQaOXx48dvampqbOPj475z586dKPW3WCzswYMH33s8nkZFURAOh1NGn98rCKIPIT8A8JXzEATh8xs3bnzb399/wmw2q4lEIpNMJrM2m81cW1trJ4SYVVUFpRSLi4vJa9euPUulUtk9BH8KYNIQojP6+i8NnBemp6fXV1dXKWPM6nQ67Q6HwyGK4rFQKFTw+/2xJ0+eLGUyGd7T01N7+fLlM8lkcktno5ItAFgwGLgEwPuBH5gBWC0Wi8NisYg7OztKLpfbBJADUCSEOB4+fOhtbGyUOOfK/fv3p/1+f7jCeT8BCBgMfK2vSqYCyCuKIufz+XSxWDQkWdFbcmFubq5w/vx5lyRJ4sWLF92yLOfC4XByl/MiABYMAM0AvqqieMrNADydTsvBYHCro6PDJUkS6+zsrJNleSsSiZQDsQggYgBooZR+yfn/Hgc1WZY3g8HgVnt7u0uSJJExduz58+eLpfMCpRSc8yUA4XcMEEL2AwAAaJlMZnN2djbLGJMmJyejGxsbG6UACCHgnC8CCBtF6KOU9mmatp8TsQmARRemf4kTpZRomvYLgKfvRjJCyEE8QLLl1ecfEaRH/TSjekXnKaX5Qwv6d6w8AG7SqQqpqvqNJEmMEGIymUyEEAJBEAilFIIgkNJ9yXdEn3K5qqqccw5VVbmmaVBVlZfuOedQFIVzzpV8Pp8DEAKgGIn/BMBZAJ2H8DZcBfAKwO8AsqWVJwKQdMk9SCsY8g0AfwFilurOrF0d0QAAAABJRU5ErkJggg=="/></span>
						<a title="Voir" class="bouton bouton-lien" href="img/'.$image.'"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAASdAAAEnQF8NGuhAAAAB3RJTUUH3wEYEBcZ+r/dvAAAAshJREFUWMPFl8tqFEEUhr/pmQkxRAOKUbHbiFnoRkRw0R3ElQt130HQl0g9SNdTCELt40IUxJAa3QSjIiaQCF1RiNF4yX1ubmqkE2bSPT25HGgoZqrq/Oc/14JjlkJiXQT6gdIh66wBW0A9CaAf8IF7wOlDBvATmATeAltFC2IEeAwMHQHrJ4DLwGfgl2OpPwsMHqHrTwHDQNGxP5SPIf7KJAKu0EWwZpVmljvTADhRFN3xff98HhMrlco3IcRroNEJQMFScRt42GbTQBzHj3IyAND0PO8JsNHmv6fAlJNygdOF8ppS6nscx5t7rNxXR6kHH+8SIcQnpdSs67rDWuu7WePKSVTBXmUbWAfWMu4v9sSA1notCIL/tSOKohthGF50XXco4327GMgEII7jLav8z/j4+EshxHwi3cpBEFzwPG+gmz7kJIJtX4mi6OvY2NjzKIqMEEIDy0qpN0KIj3EcNyzAehckOi0XFFKsxhjzW0qpgVUp5Stg0+b2hlJqxhhT9X3/jFJqXghxy/f9rD2lkMkFWutlYNUqXd9TWDa11rNSyiljzKIQ4oVS6stBVcKsZXXbfgArUsppwAnD8NJBAchUVJKs2KEjMwP7TUHNMAxHwzAc6TJTSynMldIUA9SklAtBEOSakrTWP4BqGg19wAPgfoc9vcyJdeuOdvIMmMwSA0lflicmJm56nlfqkLI1KeVMmtV5g7DV4+tBEFxv0z9qSqmZbifyov2uAqMZDjWMMSvGmEYQBOcSWVGTUs5orT9ktB5gAZgrWhauAVcyHtwLopFDOcAiMJd0QSFDwWlJVWv9vuWGHMoLe2PAyRHhVa31u9Y6x3knWSx27JOp28FkJ2d6Vu3ZZskqXrLpNgCUXNctJDJjv3W7ftHstDbGNBNvwyWg1rpk0L4NxyyIw5QNYBqoAGtJK/qAkwc0H6ZVx78t9/0DHMAJ+7owCVkAAAAASUVORK5CYII="/></a>
					</span>
					<img id="img_'.$i.'" src="img/'.$image.'" alt="'.htmlspecialchars($image).'">
				</div>'."\n";
			}
			echo '</div>';

?>


<div id="slider">
	<div id="slider-box">
		<div id="slider-box-cnt">
			<div id="slider-box-img-bg">
				<div id="slider-box-img-bg-blur-fallback"></div>
				<div id="slider-box-img-bg-blur"></div>
			</div>
			<div id="slider-box-img-wrap"><a id="slider-img-a" href="#"></a>
<img id="slider-img" alt="#" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAbsAAAG7AEedTg1AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAk9QTFRF////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPDw8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgoK4+Pj3d3dAAAAAAAAjo6OhISE39/f3d3d9fX1+Pj4FBQUS0tLTExMUVFRX19fdHR0f39/hYWFh4eHiVdXiYmJioqKi4uLjIyMjY2Njo6Oj4+PkJCQkZGRkpKSk5OTlJSUlZWVl5eXmJiYmZmZmpqam5ubnZ2dnp6en5+foKCgoaGhoqKio6OjpKSkpaWlpqamp6enqKioqampqqqqq6urrKysra2trq6ur6+vsLCwsbGxsrKys7OztLS0tbW1t7e3uLi4urq6u7u7vLy8vb29vr6+v7+/wMDAwcHBwsLCw8PDxMTExcXFxsbGx8fHyMjIycnJysrKy8vLzMzMzc3Nzs7Oz8/P0NDQ0dHR0tLS09PT1NTU1dXV1tbW19fX2NjY2dnZ2tra29vb3Nzc3d3d3t7e39/f4ODg4eHh4uLi5OTk5eXl5ouL5ubm56Oj5+fn6I2N6Jyc6Ojo6Y6O6enp6pyc6urq6+vr7Ozs7Z2d7e3t7uXl7u7u7+/v8PDw8fHx8vLy8/Pz9PT09fX19vb29/f3+Pj4+fn5+vr6+/v7/Pz8D8IhUwAAAER0Uk5TAAECAwQHCAoLDA0RExQVFhcYGRobHB0eHistOkBBQ0tMTk9RU1VWV1laW1xdXl9hYmNlZmdpamtzg4qNk6KkstfZ3eBnGTaNAAADz0lEQVRIx7VW3W4bVRD+Zs7aa3ttx7GTOD9NoqZC4DaiTQU3FVIFD8ILIN4ACSHxBuSSK56Ad6gQlaBqqdSGpjTIzZ8bO3Hs2Bt7d88MF7aTzY/AveDcnNVo5sw3M9/MLPB/HwJgEobGUFUbWoCARGkl/e0YBt+dbh2GIJj87e9FxwHD37xsW0Yy7dpx9KHWTSdB8Io/2TEDNl8edR0wq45poMxwQKQA8BbA0r/cAKBE4PetgwPQGaSz26pEBHNFToBz9Q3br3Yo7Z9E6UzBXOsBALAIYBvAQqtOVHETCIO93c1iankoXxzywgFRPEvR3mFxInnYbGBqspQ73vHK7jksJboMqdfIlMzzl77jIIoyt28t1aoL3vWQBu/XE9n283fFtVwSwcmfT3Y+nmztLrtxqjoAVAHsApjbjyYaz/DZ3BRvoCIL+78/upfd6RUWR5AAMPjcQ2vXC1+bh3dahldX2WzeeWhehxONXox/YIBUVRWArZ3avdaDlcSHhpn5l4XEyoPWnlA90OEhgGMx9GvGr1YKATMb5vaTDzgoVKq+c9i+FAMAYB6o1ssBLbpMzET48QsWdhc3+0nd7gPzV6kRniCy6VyBiYjpZ/upUNoN0xJFnZIZ1mGYpWFp+k7k57MgIo7WTz5nJUI23/XYxjjlgEaElTCl3eUtXv3jkS+G7oKADZmseslwRAWmASQB8A5Rp5eZbn6Sxt01Nuu+qgKV0zfT6s+XUpCBfpwa5ITI1TtpVVX5CvVmXhWd9q12kIx1DYPO0ppQIGgfn6qqiBTDIz09bgcKe2Yw6DgSERHAeAZhcbsvIiLWSjE8kv52MQRlWQZnSG8AwDSQqHax8nR2SkWJoFq02qyu7fuztg5Mxz0MHWZnjvXg/uOt4JWItSKvoq3H9w+0Ozuiq1ysA8x0d2euMf+oM2d5AxXJv3h6o2EbKzNhTOlCA6Vmj7rA3G+ZSj6Dv9ob/qIGfmEh27w8NQDgEMB0hp71Slxu/eoY2ChfZjS7q9wsjTzEgwYAeOV7L/6+kUpN9QIkUwZRbebmTGxyDHo6vhtyrLU3qVzG8wDt+PLRgjEXt4kTyxIESBdS3sHpcWSiiDNzs+V0ZyCPGQwdTAJoAihh6matY8NU0vXg2s5IPjm0uAQJAFxXSlCP2G1es+Ac0DkkjNwbIDX8lLh8OL3HnuB8eZBN/Md9liX7HkuaAVkfV39dAEa/I2bMnSidPgiZyaUkfz2YaAQwGAQQFFAIBFAoIPhBgrdNn0CF3Lwz1q9DtHdyrARwJsdjGciJL4OcOmMaRAD+AQJEu+qkffMqAAAAAElFTkSuQmCC"/>

</div>
			<a href="#" onclick="slideshow('close'); return false;" class="slider-quit">
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAASdAAAEnQF8NGuhAAAAB3RJTUUH3wEYEBwBCiecIQAAA+dJREFUWMPFl91PHFUYxn9zZobP8hEsUj4FKoqKtSRlaTANxhjZNvWu/4WJ9d4a+x94b9pbG71oI02seEPhtl6JH22hFNKC3SwfxW53Z2fPe44XnQmL0t3FBHiTNzkze2af55zzvO88A4ccTtHYBaoAb58xNRACUkygGnhXKXUO6NpnAo+MMbeAOSDvRSSOKqXOG2PaAbvPBLoirL+AVRVtfXuUBxUxnquiG7WHoL9aAHXYVaAiDShjzIGBRlgKcNQu5ViubFUFC6q4BaiiB8s+cHk8MXrjQvKc73n/6RW+53k3LiTPXh5PjFZIQsXNxwV6gXdKgX/5/sjoxZ7WE71IQ+J4X9v1heVFE+2l73ne9+c/TI77tjPRfKSturHZn320ulKGwB/AckzgtRIEnEujJxMXO1ves0GA1UKPYxsS/b1t1xeXF13lqO+S48kzSjqs1tiwwGhTXVt1Q6M3u/JktQSB34Flr4IjqOnR4TGdye7Y1zFHdVz76EwSYEzCDtHbIrb5Aj06PAbUALlKjqAPePslE+XHVDrV5/uvDtRU1VsRrAhWa7qNNHSLbrC6QHzfiPDDajr96W/3Z6wlU2JhfwJLMYF+4K2XzbSW/E/pzVRfld86UF1Vb8VgxWC1vMjo2ohhMrWR/uzu0oxYnpZp63eBhzGB48BgKcVYCKbWt1K9vts64PvbO1GUk2sb6c8XHs8IbFbwTrkHLHp76ANWKfWsqblFSxjsOqGpuUUr9eSZGGP30gcqahy+Ut7V4aGJ09lMu+RDdsvT2Uz71eGhCV+pSj1FZZ3QV8q7MjSYPLWx3iG5PJIvIPkCOsr4WnJ5Tm2sd1wZGkxWQMIproI3otwt3G8GB86ObK532MJOtd/K5NLzQZjtd9hRHe25XMPJzs62ybX1ByW0sAAsVKIBf/rhUma4tgZ3W5BMFXT6i0z2NoCRug8mfK81/hMBpjeWMoAfW69yOzAQ5a4ebk7LWkakesTaV6wIU2GYvpTLzwg8tRBMh4VUtzWtfcbWaxG+DvL3vw0LvwDZEgubBxZiAm8Cr5eYHM4Zk85oqdkSI19pmS0uNQvBbTGpLjFHbxb0yjUxd4DnZTTwAJh3Iif8iVLq4wo8QX3kmv/e5WwdoDFyvSXBlVIYY34Gbu7Vgj8v3avY+j+OKGbvcHDh/LsRKcc5OPwIa4clO1RTaoFAKRUcGOgLrACwXqTaeyIyVldXV+U4jud5nuM4Dq7rOkopXNd1isdFvzmRy7UiYq21iIg1xiAitnhsrUVrba21OgiCbPQ21PHBHwFOACMH8G34GLgD/ApkipXnA3XRh+p+Rj7qkAWAfwA2RvZGZQJdhQAAAABJRU5ErkJggg=="/>


</a>
		</div>
		<div id="slider-box-bottom">
			<p id="slider-img-info"></p>
			<p id="slider-buttons">
				<a href="#" onclick="slideshow('first'); return false;">
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAASdAAAEnQF8NGuhAAAAB3RJTUUH3wEYECIWyDECmwAAAy5JREFUWMPNV89LG0EU/mZ2u2L80YKiDUTopWopDQki0oK1l0BJCz21x4A2N8+CR4/qWZD8E16ktCQnCfTSFEp7igo9RNsQk/ijmmx2ZnZ66CTd2s2aFBL7YGDZ2Z3vzfve++YNwW/TABgAdHTWOAALgAAAol72AHhAKY0CCHTYgZxt228BfAFQ05UTw5TS57Zt+wHIDjsQUFjfAXyjKvR+NbpldTyNqhe96L71AgDFNRtVOUBt2+4aqMKiAEg9AuQaNk+cFDSjQlPjSqr6+vqMYDA41Gb04RUBLZlMvjg5OXm9tLQ07bWTeDw+kclkXk1NTd1pNwKeqlcsFmWlUkGpVHJNkEAg0L+xsfFkZmbGL6UktVqtbR50Lwo457AsC0KIv7xfXl4Ox2KxoM/nM+rAkUhkJBwOzzg/3N/fP00kElkXgaNOB1yTkDEGxhik/P1vKBQaXl1dfTw+Pj4shABjrDE3Ozs7BmDMuUY6nT5KJBL76gxoSgHxikC9bNbW1h5Go9EJSqlerVZbCrFlWS3lgCsF9R1OTk7eSqVSL0dHR28KIdwoaWoe315JAWGMwbIslEoluru7SwYGBkBpe+LppKiZDhAv7xljyOVy5cXFxdT6+vpePp/n9dxoZVwRLW8lvJQDpa2trffxeHwnlUqVqtUqLMu6cnDOW8oBrZkDl3ZRKxQKX1dWVopzc3P3FhYWJvx+f099cnt7+3Bvb++Hc42jo6PzJj2G1m4VNM4SAKc7OzsfM5nMYSwWC0UikduGYZBsNptPJpOf3Nj8JyU0TZOcnZ2Bc+7mILu4uDjY3Nw8TqfTd+fn5+8rvRDtnkgGgGcAnrqUyYgKVQXAscc6mq7rQ4ODg0a5XD5oEfsdgDcNHaCUuoU632q5c84L5XK5pRpVWLQTHVHbXY2zDLvZlJDLQkQJ6R6+wvqjJbvWplQCMCmlZtdAf2GZAKSuzumsEOKRz+czCCG6ruuEEAJN0wilFJqmEeezY46oLlcKIaSUEkIIads2hBDS+SylBOdcSim5aZoVAFkADYHpBxAEMN2Fu+EBgA8APgM4d2beDQA+dVHtpNWUsDH8D/YThzCxw3RiKq8AAAAASUVORK5CYII="/>

</a>
				<a href="#" onclick="slideshow('prev'); return false;" id="slider-prev"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAASdAAAEnQF8NGuhAAAAB3RJTUUH3wEYECcERv+HlgAAAwBJREFUWMPNl0tLW1EUhb9z7jXGmIgDqQ+sorVU1Eo7EGkR6rA+QCFjh6J/QX+CzpyIf6HiRPAxERyIiJ21lkQrLUWbVgiWQio38Z57Oui5NlojScTYBRvuM2uftXb22VfwFxYQAGzuFi6QARSAMBfLgadSykGg8Y4TOPI8bw14D6Rtk0SNlHLY87x6QN9xAo2G6xuQkEb6ehOlgs9nSXOhgtKjAkByz5CmBqTneSUjNVwSEL4C4h4WL7ItuJUVvb29D4roHzKbuCgFbNu2FhYW+hcXF4eBUDEKFNv1RDQabZmenn7R0NBQmclkiu4ddqEWVFVVBefn5/v7+voeaq1FOp1GKcXc3NwzIcSlSl5aWvqyubl5dJMFdgEWyMnJyc6JiYnn4XA4mMlkLikyMDDQfvWF3d1dFzjKx4IbE2hpaamenZ191dXVVauU4gp5Tiil8q6BXBbIqampnmg02mHbdpnjOMX837mNBcFYLFaeSCSoq6sruMDyUcAySbQC7dc8dB6Px09WV1d/RCKRSHNzc6W/snxie3v7JBaLHedIYB/4bJkkHgFPcg0Q6XT659bWVmJvb0+1trZWRyIR2/M8lFIX4brupXOlFDs7OyfxeDxXAgfAJ2GmoNfAUB6qBgKBQO3Y2Fj3yMhIQzAYFEZqPTMzc/DPEvf3vyYSicMcv7UCrPsJDACDBXSwyra2trbx8fHOjo6OSqWUHh0dfQP8uvKs9keva7AKrPkWPDaRLzKnp6fJjY2N76lUqrypqSm8vLz8AXAALytu6pAfgUNfgSFjQzEI1dTUNCWTyWMgVcB768DKxW4oZdEb4lkymTy4Rv7cDUDKf/rArWeM20xEfmGVcigR2QOJAKQQpeM3XJdGsnsdSjXgSCmdkpH+4XIAbZtvtX2l1MtQKBQQQti2bQshBJZlCSkllmWJ7OOse8LsDVoppbXWKKW0adM6+1hrjeu6WmvtOo5zZvYC1zc+DHQDPSX4NjwG3gLvgFR25ZWZwbL8jhNIA2fAOf8DfgPqUkdZNYDu8gAAAABJRU5ErkJggg=="/></a>
				<a href="#" onclick="slideshow('next'); return false;" id="slider-next">
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAASdAAAEnQF8NGuhAAAAB3RJTUUH3wEYECcunUROQAAAAthJREFUWMPNl09LG1EUxX/vzTRqdFEwNKSVglZcBNpdDS0uCt2E1gpVqP0KrnQl+CVcBoNLl3ETRKq7upQKhRS1lkBttdbQZKGtMsnMm9dFJ20w0fwhiT3whuHNvDlvzrn3zh3BPxiADzBpLRygACgA4U12APellM+AvhZv4MB13TfAByBvepsISClHXdcNAbrFG+jzuL4DR9KTPuSNdqHIZ0hvoov2owtAcs2QXgxI13XbRupxSUAUFRDX8PKi1IJ6rTAjkcitJqhPowp0JRKJ0Xg8/sQ0TaMZCtQNrbUxNjY2tLm5+XpiYmKgURvNahaMjIzcnpycHLhALh3HQWtNb29v9/z8/NPx8fGDqampt6enp1Y9FpjVLAiFQnei0Wi4UiSXZI4YHh6+u7Gx8Soej79fWFjYBtx6LBBXpUyhUKhp+Hy+zpmZmUfLy8sv+vv7bzYlC5RS2LZd87Asi8HBwWAikXg5NzcXueLZtVlQVKBeHB8fs7u72wF0AueXKWBWi16tNbZt10xs2zbr6+s/YrFY6uTk5BtwVVCKqgoopcjn8+UrhSjb6P7+vhWLxfa2trY+Aj+rBKIoHnxAFHhe6a5gMHgvHA6XNSmzs7NDhmEIAMuydDKZPFpaWkoVCoWM1/FUwyqwVlWBTCbzOZPJfLkw3T09PT1kGAY7Oztni4uL2+l0Og2c1dHQCGrs/9wKUrq5XM5JJpOHKysrKa11zuv1GqrHPk/+aB3regKBQF82m/16SYTXgjVg9W8dkLKuz8JZNpv91Ci5xyVrtaBidjareS0txe1sSkRpKRaAvJjXLWUXoqwlu9amVAOWlNJqG+kfLgvQppe/e0qpx36/3yeEME3TFEIIDMMQUkoMwxCl5yXXhPfB0koprbVGKaVd10UppUvPtdY4jqO11o5lWefAHuAUje8BHgAP2/BveAi8A1LAr9LIuwH4vR/VViLv1Q+b/wG/AZzvW4/mi5YqAAAAAElFTkSuQmCC"/></a>
				<a href="#" onclick="slideshow('last'); return false;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAASdAAAEnQF8NGuhAAAAB3RJTUUH3wEYECQQdwgAKAAAAzhJREFUWMPFl01LJFcUhp97q8ZBM3SmM8lCI4mKEEQzi4GABHshboIZQRLI1kUWLgR/QW/8Be5ctroSwa0YED8wuNDgQicEmgghHxid6cTGTtruul9ZTLVTOmXbLbF94UBx7606p9733HPPFbyBBzQBPncLDQSAARDh4EPgUynlMNB+xwH8bq1dAV4AZT8M4n0p5XNrbSvg7jiA9tDXn8CRDKlvDa1RqPjzZDjQTOPRDCC5Z8gwB6S1tmFOQ18SEBUGxD38vIhKULMUvb297yUSiYc1suuFdt08dTPQ19f30e7u7jcTExM91d6bnJx8ls/nv93c3PzqmiDErZJQa+0SiURzOp1OLS8vP+/s7EzErcvlcq5YLJLL5Wy1QP0qEojx8fFPuru7340OJpPJx0EQVORoXVlZ+XphYeHF1NTUXrSIWWsJggCtdTWJLgKIi1D29/f3pFKpD65OKKXeHCCe92BsbOzZwMDAx+l0+vudnZ2XlQCUUpfWVpMglqIgCCiXy29ZEASX7Pz8nLa2tieZTGZkeno65fu+rIEBcZMEGGOq/UEcvKGhoZ7V1dUPt7a2XimlMMbcWgKUUlT0rqfIZLNZkcvlvApDNzFwbYbWy0A+nzdzc3O/LC0t7Y+MjHQopTqqMAAgqjKgta6JAWMM29vbf8/MzOyfnJz8BpSNMR315EBstVpcXPx1fX39r+hYV1fXo9HR0Yum5fj4OJidnf15Y2PjR6AA2Bp3gXdTDpi9vb39q4ODg4N9w8PD7Uop1tbWjufn5/cLhcIRoK6yUg8D17IbR/fh4eF5JpP56eDgIAv8G9dFWWvF2dkZpVJJ3HQiNQFfAl/UkmjJZLK9UChorfWruAAjeAy8E655WZEmgu+A5Ys6IKWklp7g9PT0KOZjsZsitLcLwGtf8rYd0f/auURLcSObEhE9CwQghWic/9DXpZbsXptSB5SklKWGOX3tqwQ4P7yrZY0xn7e0tDQJIXzf94UQAs/zhJQSz/NE9DkyJ8I974wxzjmHMcZZazHGuOizcw6ttXPO6VKpVASygK4I/wh4CnzWgLvhH8APwAHwTzTzHgAt4UX1LlEGildL973hP1j/nOsRFsXUAAAAAElFTkSuQmCC"/>

</a>
			</p>
		</div>
	</div>
</div>
<script type="text/javascript">
var curr_max = <?php echo count($imgs)-1; ?>;
var counter = 0;
document.onkeydown = checkKey;

function slideshow(action, imageIndex) {
	if (action == 'close') {
		document.getElementById('slider').style.display = 'none';
	}

	var ElemImg = document.getElementById('slider-img');

	var newImg = new Image();
	if (action == 'start') { document.getElementById('slider').style.display = 'block'; counter = imageIndex; }
	if (action == 'first') counter = 0;
	if (action == 'prev') counter = Math.max(--counter, 0);
	if (action == 'next') counter = Math.min(++counter, curr_max);
	if (action == 'last') counter = curr_max;

	var box_height = document.getElementById('slider-box-img-wrap').clientHeight;
	var box_width = document.getElementById('slider-box-img-wrap').clientWidth;
	var img_height = document.getElementById('img_'+counter).naturalHeight;
	var img_width = document.getElementById('img_'+counter).naturalWidth;
	var ratio_w = Math.max(1, img_width/box_width);

	newImg.onload = function() {
		ElemImg.src = newImg.src;
		document.getElementById('slider-img-a').href = newImg.src;
		ElemImg.style.marginTop = (Math.round((box_height - Math.min(img_height/ratio_w, box_height))/2))+'px';
	};

	newImg.onerror = function() {
		ElemImg.src = '';
		ElemImg.alt = 'Error Loading File';
		document.getElementById('slider-img-a').href = '#';
		ElemImg.style.marginTop = '0';
	};
	newImg.src = document.getElementById('bloc_'+counter).dataset.imgUrl;
	var imgName = document.getElementById('bloc_'+counter).dataset.imgName;
	document.getElementById('slider-img-info').innerHTML = '('+(counter+1)+'/'+(curr_max+1)+')'+' <b>'+imgName+'</b>';
	document.getElementById('slider-box-img-bg-blur').style.backgroundImage = 'url('+newImg.src+')';
}

function checkKey(e) {
	if (document.getElementById('slider').style.display != 'block') return true;

	e = e || window.event;

	var evt = document.createEvent("MouseEvents"); // créer un évennement souris
	evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

	if (e.keyCode == '37') {
		// left
		var button = document.getElementById('slider-prev');
		button.dispatchEvent(evt);
		e.preventDefault();
	}
	else if (e.keyCode == '39') {
		// right
		var button = document.getElementById('slider-next');
		button.dispatchEvent(evt);
		e.preventDefault();
	}


}

</script>
</div> <!-- end #axe -->
<?php } ?>
<footer><a href="?a">administration</a></footer>
<!-- fork of http://lehollandaisvolant.net/tout/folio -->
</body>
</html>