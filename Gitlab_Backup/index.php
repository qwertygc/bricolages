<?php
set_time_limit(0);
$list_repos = array();
function gitclone_groups($instance, $groupname, $token) {
	global $list_repos;
	$repos = json_decode(file_get_contents("https://".$instance."/api/v4/groups/".$groupname."/projects?private_token=".$token), true);
	foreach($repos as $repo) {
		$list_repos[] = $repo['ssh_url_to_repo'];
	}
	$subgroups =  json_decode(file_get_contents('https://'.$instance.'/api/v4/groups/'.$groupname.'/subgroups/'), true);
	foreach($subgroups as $subgroup) {
		$subrepos =  json_decode(file_get_contents('https://'.$instance.'/api/v4/groups/'.$subgroup['id']), true);
		foreach($subrepos['projects'] as $subrepo) {
			$list_repos[] = $subrepos['ssh_url_to_repo'];
		}
	}
}
function gitclone_users($instance, $userid, $username, $token) {
	global $list_repos;
	$repos = json_decode(file_get_contents("https://".$instance."/api/v4/users/".$userid."/projects?private_token=".$token), true);
	foreach($repos as $repo) {
		$list_repos[] = $repo['ssh_url_to_repo'];
	}
}

foreach($list_repos as $repo) {
	$parse = explode(':', $repo);
	$parse2 = explode('.git', $parse[1]);
	//mkdir($parse2[0]);	
	exec('git clone '.$repo.' '.$parse2[0]);

}
