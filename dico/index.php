<?php
session_start();
function redirect($url) {echo '<meta http-equiv="refresh" content="0; url=' . $url . '">';die;}
 function download($name, $url) {
	header('Content-Type: application/octet-stream');
	header('Content-Length: '. filesize($url));
	header('Content-disposition: attachment; filename='. $name);
	header('Pragma: no-cache');
	header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	header('Expires: 0');
	readfile($url);
	exit();
}
function utf8_to_rtf($utf8_text) { #http://spin.atomicobject.com/2010/08/25/rendering-utf8-characters-in-rich-text-format-with-php/
	$utf8_text = str_replace("\n", "\\par\n", str_replace("\r", "\n", str_replace("\r\n", "\n", $utf8_text)));
	return preg_replace_callback("/([\\xC2-\\xF4][\\x80-\\xBF]+)/", function ($matches) {return '\u'.hexdec(bin2hex(iconv('UTF-8', 'UTF-16BE', $matches[1]))).'?';}, $utf8_text);
}
function stripAccents($str) {
	$str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
	$str = ucfirst($str);
	return $str;
}
try {
    $db = new PDO('sqlite:data.sqlite');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$db->exec('PRAGMA encoding = "UTF-8";');
	$db->exec('  PRAGMA case_sensitive_like=OFF;');
}
catch(PDOException $e) {
    echo "<p>Erreur : " . $e->getMessage() . "</p>";
    exit();
}
if (!file_exists("config.php")) {
    $db->query("CREATE TABLE dictionnary (
        id INTEGER PRIMARY KEY,
        word text NOT NULL,
        word_without text NOT NULL,
        definition text NOT NULL,
		bookmark text NOT NULL default '0');");
		file_put_contents('config.php', '<?php'.PHP_EOL.'define("PASSWORD", "password");');
}
if(!is_dir('export')) { mkdir('export',0755);}
include 'inc/form.class.php';
include 'config.php';
?>
<!doctype html>
<html>
<title>Dico</title>
<meta charset="utf-8"/>
<link href="inc/style.css" rel="stylesheet"  type="text/css" />
<body>
<header class="header">
<h1><a href="?">Dictionnaire</a></h1>
<nav>
<?php if(isset($_SESSION['login'])) { ?>
	<a href="?a=entry">&#9998; Add entry</a>
	<a href="?a=bookmark">&#9733; Bookmark</a>
	<a href="?a=export">&#8614; Export</a>
	<a href="?a=import">&#8612; Import</a>
<?php }?>
</nav>
	<?php
	$form = new form(array('action'=>'?a=search', 'method'=>'post', 'class'=>'form'));
	$form->input(array('type'=>'search', 'name'=>'search', 'placeholder'=>'Search'));
	$form->input(array('type'=>'submit', 'value'=>'&#128270;'));
	$form->endform();
	if(!isset($_SESSION['login'])) {
		$form = new form(array('action'=>'?a=login', 'method'=>'post', 'class'=>'form'));
		$form->input(array('type'=>'password', 'name'=>'password', 'placeholder'=>'Password'));
		$form->input(array('type'=>'submit', 'value'=>'&#10004;'));
		$form->endform();
	}
	else {
		echo '<a href="?a=logout&t='.$_SESSION['token'].'">logout</a>';
	}
?>
<nav>
<?php
foreach(range('A','Z') as $i) {echo '<a href="?a=search&letter='.$i.'">'.$i.'</a>';}
?>
</nav>
</header>
<main>
<?php
if(isset($_GET['a'])) {
	switch($_GET['a']) {
		case 'login':
			if((isset($_POST['password']) AND $_POST['password'] == PASSWORD) OR (isset($_COOKIE['auth']) && !isset($_SESSION['login']) && $_COOKIE['auth'] == sha1(PASSWORD))) {
				$_SESSION['login'] = true;
				$_SESSION['token'] = md5(uniqid(rand(), true));
				setcookie('auth', sha1(PASSWORD), time() + 365*24*3600, null, null, false, true);
				redirect('?');
			}
		break;
		case 'logout':
			if(isset($_GET['t']) && $_GET['t'] == $_SESSION['token']) {
				$_SESSION = array();
				session_destroy();
				if(isset($_COOKIE['auth'])) {setcookie('auth', false, time() + 365*24*3600, null, null, false, true);}
			}
		break;
		case 'search':
			if(isset($_POST['search'])) {redirect('?a=search&search='.$_POST['search']);}
			if(isset($_GET['search'])) {
				$st = $db->prepare('SELECT * FROM dictionnary WHERE word LIKE :query OR definition LIKE :query ORDER BY word_without ASC');
				$st->execute(array('query'=>'%'.$_GET['search'].'%'));
			}
			if(isset($_GET['letter'])) {
				$st = $db->prepare('SELECT * FROM dictionnary WHERE word_without LIKE :query ORDER BY word_without ASC');
				$st->execute(array('query'=>''.$_GET['letter'].'%'));
			}
			echo '<dl>';
			while($data = $st->fetch()) {
				$bookmark = ($data['bookmark'] == 1) ? '&#9733;' : '&#9734;';
				echo '<dt><dfn>'.$data['word'].'</dfn> ';
				if(isset($_SESSION['login'])) {echo '<span class="tool"><a href="?a=bookmark&id='.$data['id'].'&t='.$_SESSION['token'].'">'.$bookmark.'</a>
				<a href="?a=entry&permalink='.$data['id'].'">&#128279;</a>
				<a href="?a=entry&modify='.$data['id'].'">&#9998;</a>
				<a href="?a=entry&delete='.$data['id'].'&t='.$_SESSION['token'].'">&#10060;</a></span>';}
				echo '</dt><dd>'.$data['definition'].'</dd>';
			}
			echo '</dl>';
		break;
		case 'entry':
			if(isset($_SESSION['login'])) {
				if(isset($_GET['modify'])) {
					$st = $db->prepare('SELECT * FROM dictionnary WHERE id=?');
					$st->execute(array(intval($_GET['modify'])));
					$st->setFetchMode(PDO::FETCH_BOTH);
					$data = $st->fetch();
					$id = $data['id'];
					$word = $data['word'];
					$definition = $data['definition'];
				}
				else {
					$id = 0;
					$word = '';
					$definition = '';
				}
				if(isset($_POST['word']) && isset($_POST['definition'])) {
					if($_POST['id'] == 0) {
						$st = $db->prepare('INSERT INTO dictionnary(word, word_without, definition) VALUES(?, ?, ?)');
						$st->execute(array(ucfirst($_POST['word']), stripAccents($_POST['word']), $_POST['definition']));
						redirect('?');
					}
					else {
						$st = $db->prepare('UPDATE dictionnary SET word=?, word_without=?, definition=? WHERE id=?');
						$st->execute(array(ucfirst($_POST['word']), stripAccents($_POST['word']), $_POST['definition'], $_POST['id']));
						redirect('?');
					}
				}
				if(isset($_GET['delete']) && isset($_GET['t']) && $_GET['t'] == $_SESSION['token']) {
					$st = $db->prepare('DELETE FROM dictionnary WHERE id=?');
					$st->execute(array($_GET['delete']));
					redirect('?');
				}
				if(isset($_GET['permalink'])) {
					$st = $db->prepare('SELECT * FROM dictionnary WHERE id=?');
					$st->execute(array(intval($_GET['permalink'])));
					$data = $st->fetch();
					$bookmark = ($data['bookmark'] == 1) ? '&#9733;' : '&#9734;';
					echo '<dl><dt><dfn>'.$data['word'].'</dfn> 
					<span class="tool"><a href="?a=bookmark&id='.$data['id'].'&t='.$_SESSION['token'].'">'.$bookmark.'</a>
					<a href="?a=entry&permalink='.$data['id'].'">&#128279;</a>
					<a href="?a=entry&modify='.$data['id'].'">&#9998;</a>
					<a href="?a=entry&delete='.$data['id'].'&t='.$_SESSION['token'].'">&#10060;</a></span></dt>
					<dd>'.$data['definition'].'</dd></dl>';
				}
				else {
					$form = New form(array('action'=>'?a=entry', 'method'=>'post'));
					$form->input(array('type'=>'text', 'name'=>'word', 'value'=>$word, 'placeholder'=>'Word'));
					$form->textarea(array('name'=>'definition', 'placeholder'=>'Definition'), $definition);
					$form->input(array('type'=>'hidden', 'name'=>'id', 'value'=>$id));
					$form->input(array('type'=>'submit'));
					$form->endform();
				}
			}
		break;
		case 'bookmark':
			if(isset($_SESSION['login'])) {
				if(isset($_GET['id']) && isset($_GET['t']) && $_GET['t'] == $_SESSION['token']) {
					$st = $db->prepare('SELECT bookmark FROM dictionnary WHERE id=?');
					$st->execute(array($_GET['id']));
					$data = $st->fetch();
					$bookmark = ($data['bookmark'] == 0) ? 1 : 0;
					$bookmark = ($data['bookmark'] == 1) ? 0 : 1;
					$st = $db->prepare('UPDATE dictionnary SET bookmark=? WHERE id=?');
					$st->execute(array($bookmark, $_GET['id']));
					if($bookmark == 1) {redirect('?a=bookmark');} else {redirect('?');}
				}
				else {
					$st = $db->prepare('SELECT * FROM dictionnary WHERE bookmark=1');
					$st->execute();
					echo '<dl>';
					while($data = $st->fetch()) {
						$bookmark = ($data['bookmark'] == 1) ? '&#9733;' : '&#9734;';
						echo '<dt><dfn>'.$data['word'].'</dfn> 
						<span class="tool"><a href="?a=bookmark&id='.$data['id'].'&t='.$_SESSION['token'].'">'.$bookmark.'</a>
					<a href="?a=entry&permalink='.$data['id'].'">&#128279;</a>
					<a href="?a=entry&modify='.$data['id'].'">&#9998;</a>
					<a href="?a=entry&delete='.$data['id'].'">&#10060;</a></span>
					</dt><dd>'.$data['definition'].'</dd>';
					}
					echo '</dl>';
				}
			}
		break;
		case 'import':
			if(isset($_SESSION['login'])) {
				$form = New form(array('method'=>'post', 'enctype'=>'multipart/form-data', 'action'=>'?a=import'));
				$form->input(array('type'=>'file', 'name'=>'file', 'id'=>'file'));
				$form->input(array('type'=>'submit', 'name'=>'upload'));
				$form->endform();
				if(isset($_POST['upload'])) {
					if(!is_uploaded_file($_FILES['file']['tmp_name'])) {exit('error');}
					if($_FILES['file']['type'] !='application/json') {exit('error');}
					$json = json_decode(file_get_contents($_FILES['file']['tmp_name']), true);
					foreach($json as $data) {
						$st = $db->prepare('INSERT INTO dictionnary(word, word_without, definition) VALUES(?, ?, ?)');
						$st->execute(array(ucfirst(($data['word'])), stripAccents($data['word']), $data['definition']));
					}
					redirect('?');
				}
			}
		break;
		case 'export':
			if(isset($_SESSION['login'])) {
				$st = $db->prepare('SELECT * FROM dictionnary ORDER BY word_without ASC');
				$st->execute();
				$md = null;
				$txt = null;
				$json = array();
				$rtf = '{\rtf1\ansi\ansicpg1252\deff0\deflang1036{}\viewkind4\uc1\pard\sa200\sl276\slmult1\lang12\b\f0\fs22';
				while($data = $st->fetch()) {
					if(isset($data)) {
						$md .= ''.$data['word'].'  : '.$data['definition'].PHP_EOL;
						$rtf .= '\b '.utf8_to_rtf($data['word']).'\b0  : '.utf8_to_rtf($data['definition']).'\par'.PHP_EOL;
						$txt .= ''.$data['word'].' : '.$data['definition'].''.PHP_EOL;
						$json[] = array('id'=>$data['id'], 'word'=>$data['word'], 'definition'=>$data['definition']);
					}
				}
				$rtf .= '}';
				file_put_contents('export/dico_'.date('Ymd', time()).'.md', $md);
				file_put_contents('export/dico_'.date('Ymd', time()).'.rtf', $rtf);
				file_put_contents('export/dico_'.date('Ymd', time()).'.txt', $txt);
				file_put_contents('export/dico_'.date('Ymd', time()).'.json', json_encode($json));
				echo '<ul>';
				echo '<li><a href="export/dico_'.date('Ymd', time()).'.txt" download>txt export</a></li>';
				echo '<li><a href="export/dico_'.date('Ymd', time()).'.rtf" download>rtf export</a></li>';
				echo '<li><a href="export/dico_'.date('Ymd', time()).'.md" download>markdown export</a></li>';
				echo '<li><a href="export/dico_'.date('Ymd', time()).'.json" download>json export</a></li>';
				echo '</ul>';
			}
		break;
	}
}
else {
	redirect('?a=search&search=');
}
?>
</main>
</body>
</html>