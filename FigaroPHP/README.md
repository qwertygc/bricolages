FigaroPHP
=========
This is a clone of [theotix/figarojs](https://github.com/theotix/figarojs) a script generating similar comments to those that can be found on the online newspaper leFigaro.fr. It's in PHP for a big project.
