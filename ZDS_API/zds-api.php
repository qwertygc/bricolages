<?php
class ZDS {
	private $ZDS_root = '';
	private $ZDS_username = '';
	private $ZDS_password = '';
	private $ZDS_client_id = '';
	private $ZDS_client_secret = '';
	public function __construct () {}
	public function __destruct () {}
	/**
	 * curl
	 *
	 * @param   string      $url
	 * @param   string      $type
	 * @param   array       $post
	 * @param   array      $headers
	 *
	 * @return  array       $reponse
	 */
	public function curl($url, $type='POST', $post=array(), $headers=array()) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec ($ch);
		curl_close($ch);
		return $response;
	}
	public function set_client($root, $username ,$password, $client_id, $client_secret) {
		$this->ZDS_root = $root;
		$this->ZDS_username = $username;
		$this->ZDS_password = $password;
		$this->ZDS_client_id = $client_id;
		$this->ZDS_client_secret = $client_secret;
	}
	public function login() {
		$parameters['username'] = $this->ZDS_username;
		$parameters['password'] = $this->ZDS_password;
		$parameters['grant_type'] = 'password';
		$parameters['client_id'] = $this->ZDS_client_id;
		$parameters['client_secret'] = $this->ZDS_client_secret;
		$headers = array('Content-Type: application/json');
		$curl = $this->curl($this->ZDS_root.'/oauth2/token/', 'POST', json_encode($parameters), $headers);
		$response = json_decode($curl, true);
		$this->access_token = $response['access_token'];
		$this->token_type = $response['token_type'];
		return $response;
	}
	public function get($url) {
		$curl = $this->curl($this->ZDS_root.$url, 'GET', array(), array('Content-Type: application/json', 'Authorization: Bearer '.$this->access_token));
		return json_decode($curl, true);
	}
	public function post($url, $data=array()) {
		$curl = $this->curl($this->ZDS_root.$url, 'POST', json_encode($data), array('Content-Type: application/json', 'Authorization: Bearer '.$this->access_token));
		return json_decode($curl, true);
	}
	public function patch($url, $data=array()) {
		$curl = $this->curl($this->ZDS_root.$url, 'PATCH', json_encode($data), array('Content-Type: application/json', 'Authorization: Bearer '.$this->access_token));
		return json_decode($curl, true);
	}
	public function put($url, $data=array()) {
		$curl = $this->curl($this->ZDS_root.$url, 'PUT', json_encode($data), array('Content-Type: application/json', 'Authorization: Bearer '.$this->access_token));
		return json_decode($curl, true);
	}
	public function delete($url, $data=array()) {
		$curl = $this->curl($this->ZDS_root.$url, 'DELETE', json_encode($data), array('Content-Type: application/json', 'Authorization: Bearer '.$this->access_token));
		return json_decode($curl, true);
	}
}



$ZDS = New ZDS();
$ZDS->set_client('https://zestedesavoir.com/', ZDS_username, ZDS_password, ZDS_client_id, ZDS_client_secret);
$ZDS->login();
print_r($ZDS->get('/api/membres/mon-profil/'));
