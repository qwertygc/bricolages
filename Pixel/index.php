<?php
define('w', 300); // Don't touch sinon ça bug
define('h', 700); // On peut modifier ça
define('s', 1*10); // peut être 1, 2, 3 selon la taille des pixels
function drawpix($color, $x, $y) {
	global $canvas;
	$rouge = hexdec(substr($color,0,2)); //conversion du canal rouge
	$vert = hexdec(substr($color,2,4)); //conversion du canal vert
	$bleu = hexdec(substr($color,4,6)); //conversion du canal bleu
	$color = imagecolorallocate($canvas,$rouge,$vert,$bleu);
	imagefilledrectangle($canvas, $x * s, $y * s, ($x * s)+s, ($y*s) + s, $color);
}
function random_color() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}
function id2coord($id) {
	$array = array();
	if($id<= w*h) {
		$array['x'] = $id%(w/s);
		$array['y'] = floor($id/(h/s));
		return $array;
	}
}
$canvas = imagecreatetruecolor(w, h);
for($n = 0; $n <=100; $n++) {
	$id = id2coord($n);
	drawpix(random_color(), $id['x'], $id['y']);
}
drawpix(random_color(), id2coord(1222)['x'], id2coord(1222)['y']);
header('Content-Type: image/png');
imagepng($canvas);