<?php
$allfeed = array();
function feed_reader($url) {
    global $allfeed;    
    if(preg_match('~<rss(.*)</rss>~si', file_get_contents($url))){$type='rss';}
    elseif(preg_match('~<feed(.*)</feed>~si', file_get_contents($url))){$type='atom';}
    else return false;
    $feed = simplexml_load_file($url);
    if($type == 'rss') {
        foreach ($feed->channel->item as $item) {
            $allfeed[strtotime($item->pubDate)] = array(
                "title" => $item->title,
                "link" => $item->link,
                "date" =>date("d/m/Y",strtotime($item->pubDate)),
                "desc" => substr(strip_tags($item->description),0,480)
            );
        }
    }
    elseif($type == 'atom') {
        foreach ($feed->entry as $item) {
            $allfeed[strtotime($item->updated)] = array(
                "title" => $item->title,
                "link" => $item->link['href'],
                "date" =>date("d/m/Y",strtotime($item->updated)),
                "desc" => substr(strip_tags($item->content),0,480)
            );
        }
    }
    else {}
    return $allfeed;
}
#############################################
feed_reader('<a href="http://example.org/feed.rss">http://example.org/feed.rss</a>');
feed_reader('<a href="http://example.com/feed.atom">http://example.com/feed.atom</a>');
#############################################
$maxfeed = 20;
krsort($allfeed);
$i = 0;
foreach ($allfeed as $entry) {
    echo '<p>'.$entry["date"].' – '.'<a href="'.$entry["link"].'">'.$entry["title"].'</a>'.$entry["desc"].'</p>';
    if (++$i == $maxfeed) break;
}