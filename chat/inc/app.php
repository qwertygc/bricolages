<?php
define('ROOT', 'http://tmp.grainedutopie.eu/');
if(!is_dir('data')) {
	mkdir('data');
	mkdir('data/upload');
	$bdd = new PDO('sqlite:data/database.db');
	$bdd->query('CREATE TABLE chat (id INTEGER PRIMARY KEY, id_chat text NOT NULL, pseudo text NOT NULL,content text NOT NULL, timestamp int(20) NOT NULL)');
}
else {
	$bdd = new PDO('sqlite:data/database.db');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
include 'Parsedown.php';
include 'ParsedownExtra.php';
function timeAgo($time) {
	$timestamp = time()-$time;
	$verbe = ($timestamp > 0) ? 'il y a ' : 'dans ';
	if ($timestamp < 1 AND $timestamp >= 0) {
		return 'maintenant';
	}
	else {
		foreach (array(31104000 => 'an', 2592000 => 'mois', 86400 => 'jour', 3600 => 'heure', 60 => 'minute', 1 => 'seconde') as $secs => $str) {
			if (abs($timestamp/$secs) >= 1) {
				return $verbe.round(abs($timestamp/$secs)).' '.$str.(((round(abs($timestamp/$secs))  >= 2 AND $str !='mois'))?'s':'').'';
			}
	   }
	}
}