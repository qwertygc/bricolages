<?php
include 'ldiftovcard.php';
if(!is_dir('a/')) { mkdir('a',0755);}
if(!is_dir('u/')) { mkdir('u',0755);}
function delTree($dir) { #http://php.net/manual/fr/function.rmdir.php
   $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
  } 
#Téléversement
if(isset($_POST['submit'])) {
	move_uploaded_file($_FILES["adressbook"]["tmp_name"],"u/" . $_FILES["adressbook"]["name"]);
	$file_ldif = $_FILES['adressbook']['name'];
	$array = explode('.ldif', $file_ldif);
	$file_name = $array[0];
	convert_ldif_to_vcard('u/'.$file_ldif, 'adressbook.vcf');
	#src http://stackoverflow.com/questions/1388526/convert-vcard-bulk-contact-file-to-single-vcf-files
	$filename ='adressbook.vcf';
	$file = file($filename);
	$i=1;
	$content = "";
	foreach ( $file as $line) {
		if($line == 'BEGIN:VCARD'."\r\n"){
			$content = "";
		}
		$content .= $line;
		if($line == 'END:VCARD'."\r\n"){
			$fileName = $i.".vcf";
			$ourFileHandle = fopen('a/'.$fileName, 'w') or die("can't open file");
			fwrite($ourFileHandle, $content);
			fclose($ourFileHandle);
			$i++;
		}
	}
	#Zippage
	# http://fr.openclassrooms.com/informatique/cours/les-fonctions-zip-en-php
	$zip = new ZipArchive();
      
      if(is_dir('a/'))
      {
        // On teste si le dossier existe, car sans ça le script risque de provoquer des erreurs.
	
        if($zip->open('Archive.zip', ZipArchive::CREATE) == TRUE)
	{
	  // Ouverture de l’archive réussie.

	  // Récupération des fichiers.
	  $fichiers = scandir('a/');
	  // On enlève . et .. qui représentent le dossier courant et le dossier parent.
	  unset($fichiers[0], $fichiers[1]);
	  
	  foreach($fichiers as $f)
	  {
	    // On ajoute chaque fichier à l’archive en spécifiant l’argument optionnel.
	    // Pour ne pas créer de dossier dans l’archive.
	    if(!$zip->addFile('a/'.$f, $f))
	    {
	      echo 'Impossible d&#039;ajouter &quot;'.$f.'&quot;.<br/>';
	    }
	  }
	
	  // On ferme l’archive.
	  $zip->close();
	
	  // On peut ensuite, comme dans le tuto de DHKold, proposer le téléchargement.
	  // header('Content-Transfer-Encoding: binary'); //Transfert en binaire (fichier).
	  // header('Content-Disposition: attachment; filename="Archive.zip"'); //Nom du fichier.
	  // header('Content-Length: '.filesize('Archive.zip')); //Taille du fichier.
	  echo '<a href="Archive.zip">Télécharger l’archive</a>';
	  // readfile('Archive.zip');
	}
	else
	{
	  // Erreur lors de l’ouverture.
	  // On peut ajouter du code ici pour gérer les différentes erreurs.
	  echo 'Erreur, impossible de créer l&#039;archive.';
	}
      }
      else
      {
        // Possibilité de créer le dossier avec mkdir().
        echo 'Le dossier &quot;a/&quot; n&#039;existe pas.';
      }
}
if(isset($_GET['delete'])) {
	delTree('u/');
	delTree('a/');
	unlink('Archive.zip');
	unlink('adressbook.vcf');
	header('Location: index.php');
}
?>
<form method="post" action="index.php" enctype="multipart/form-data">
     <label for="adressbook">Votre fichier .ldif :</label><br />
     <input type="file" name="adressbook" id="adressbook" /><br />
     <input type="submit" name="submit" value="Envoyer" />
</form>
<a href="?delete=yolo">effacer mes traces</a>
