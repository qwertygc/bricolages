<!--
PHP Batch Download Script
@author Yosko <contact@yosko.net>
@copyright none: free and opensource
@link http://www.yosko.net/article32/snippet-05-php-telechargement-de-fichiers-par-lots
-->
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Batch Downloader</title>
    <style>
textarea {
    width: 100%;
    min-height: 10em;
}
    </style>
</head>
<body>
    <h1>Batch Downloader</h1>
<?php
//config
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors','On');
set_time_limit ( 180 ); //facultative: to avoid timeout
ob_flush(); flush();
//recursive rmdir from php.net
function delTree($dir) {
    $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
        (is_dir($dir.'/'.$file)) ? delTree($dir.'/'.$file) : unlink($dir.'/'.$file);
    }
    return rmdir($dir);
}
$errors = array();
$urlString = '';
//a list of URLs to download was given
if(isset($_POST['urls'])
    && isset($_POST['directory'])
    && strpos($_POST['directory'], '..') === false
    && strpos($_POST['directory'], '/') === false
) {
?>
    <ul>
<?php
    
    $urls = explode(PHP_EOL, $_POST['urls']);
    $directory = trim($_POST['directory']);
    if(empty($directory)) { $directory = 'files'; }
    $zipFile = $directory.'.zip';
    
    //temporary directory to put the files into
    if(!file_exists($directory)) {
        mkdir($directory);
    }
    //temporary archive file for download
    if(file_exists($zipFile)) {
        unlink($zipFile);
    }
    
    //try and download each file
    foreach($urls as $url) {
        $url = trim($url);
        $filename = basename($url);
        $success = copy($url, $directory.DIRECTORY_SEPARATOR.$filename);
        if($success) {
            echo '<li>'.$filename.' : OK</li>';
        } else {
            $errors[] = $url;
            echo '<li>'.$filename.' : KO (source = <a href="'.$url.'">'.$url.'</a>)</li>';
        }
        ob_flush(); flush();
    }
    
    //zip files
    $zip = new ZipArchive();
    if ($zip->open($zipFile, ZIPARCHIVE::CREATE) === true) {
        foreach (glob($directory.'/*') as $file) {
            $zip->addFile($file);
        }
        $zip->close();
    }
    
    //delete directory
    delTree($directory);
?>
    </ul>
    <p>
        <?php echo count($urls); ?> file(s),
        <?php echo count($urls) - count($errors); ?> successe(s),
        <?php echo count($errors); ?> error(s)
    </p>
    <p>Download <a href="<?php echo $zipFile; ?>"><?php echo $zipFile; ?></a></p>
    <p>Errors :</p>
    <ul>
<?php foreach($errors as $error) { ?>
        <li><?php echo $errors; ?></li>
<?php } // end loop on errors ?>
    </ul>
<?php } // end form post ?>
    <form method="POST" target="">
        <input type="text" name="directory" placeholder="Destination file...">.zip
        <textarea name="urls" placeholder="Put each URL on a different line">
<?php
foreach($errors as $error) {
    echo $error.PHP_EOL;
}
?></textarea>
        <input type="submit" name="submitURLs" value="Download">
    </form>
</body>
</html>