<?php
if(!file_exists('Mastodon.php')) {file_put_contents('Mastodon.php', file_get_contents('https://raw.githubusercontent.com/Eleirbag89/MastodonBotPHP/master/Mastodon.php'));} else {include 'Mastodon.php';}
//include (__DIR__ . '/vendor/autoload.php');
date_default_timezone_set('Europe/Paris');
session_start();
$key = (isset($_SESSION['key']) AND $_SESSION['key'] != '') ? $_SESSION['key'] : null;
$server = (isset($_SESSION['server']) AND $_SESSION['server'] != '') ? $_SESSION['server'] : null;
$date = (isset($_SESSION['date']) AND $_SESSION['date'] != '') ? $_SESSION['date'] : date('Y-m-d\TH:i', time()+300);
$checked = (isset($_SESSION['date']) AND $_SESSION['date'] != '') ? 'checked' : '';
$rediff = (isset($_SESSION['date']) AND $_SESSION['date'] != '') ? '#Rediff ' : '';
?>
<!doctype html>
<html lang="fr">
<head>
<title>Send Toot on future</title>
<link rel="stylesheet" href="https://lab.grainedutopie.eu/style.css" type="text/css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">

</head>
<body>
	<form method="post">
		<p>Il est nécessaire de créer au préalable une application Mastodon, en allant sur <code>https://VOTRE_SERVEUR_MASTODON/settings/applications</code>. Il faut ensuite copier/coller le jeton.</p>
		<label for="key">Jeton d’accès</label>
		<input type="text" name="key" placeholder="Clé API" value="<?php echo $key; ?>"/>
		<label for="server">Serveur Mastodon</label>
		<input type="text" name="server" placeholder="https://mastodon.example" value="<?php echo $server; ?>"/>
	<input type="submit"/>
	</form>
<form method="post">
	<label for="toot">Toot</label><textarea id="toot" name="toot" maxlength="500" rows="5"><?php echo $rediff; ?></textarea><span id="counter"></span>
	<label for="schedule">Programmation</label><input type="datetime-local" id="schedule" name="schedule"  value="<?php echo $date; ?>"  min="<?php echo date('Y-m-d\TH:i', time()+300); ?>"/>
	<label for="oneperday"><input type="checkbox" name="oneperday" id="oneperday" <?php echo $checked; ?>/> Un toot par jour</label><br>
	<label for="visibility">Visibilité</label>
	<select id="visibility" name="visibility">
		<option value="public">Public</option>
		<option value="unlisted">Non listé</option>
		<option value="private">Abonnées uniquement</option>
		<option value="direct">Personnes mentionnées uniquement</option>
	</select>
	<input type="submit"/>
</form>
<?php
if(isset($_POST['key'])) {
	$_SESSION['key'] = $_POST['key'];
	$_SESSION['server'] = $_POST['server'];
	header('Location: index.php');
}
if(isset($_POST['toot'])) {
	$mastodon = new MastodonAPI($_SESSION['key'], $_SESSION['server']);
	$schedule = ($_POST['schedule'] != '') ?  date(DATE_ISO8601, strtotime($_POST['schedule'])) : date(DATE_ISO8601, time());
	if(filter_input(INPUT_POST, 'oneperday', FILTER_SANITIZE_STRING)) {
		$_SESSION['date'] = date('Y-m-d\TH:i', strtotime($_POST['schedule'])+86400);
	}
	else {
		$_SESSION['date'] = null;
	}
	$statusData = [
		'status'      =>  $_POST['toot'],
		'visibility'     => $_POST['visibility'],
		'language'    => 'fr',
		'scheduled_at' => $schedule,
	];
	$result = $mastodon->postStatus($statusData);
	header('Location: index.php');
}
if($key != '') {
	$mastodon = new MastodonAPI($key, $server);
	$result = $mastodon->callAPI('/api/v1/scheduled_statuses', 'GET', null);
	//print_r($result);
	foreach($result as $toot) {
		echo '<b>'.$toot['scheduled_at'].'</b> <a href="?delete='.$toot['id'].'">Supprimer</a><br>'.$toot['params']['text'].'<hr>';
	}
	if(isset($_GET['delete']) AND $_GET['delete'] !='') {
		$result = $mastodon->callAPI('/api/v1/scheduled_statuses/'.$_GET['delete'], 'DELETE', null);
	}
	//print_r($result);
}
?>
<footer><p>Utilise <a href="https://github.com/eleirbag89/mastodonbotphp">MastodonBotPHP</a></p></footer>
<script>
document.addEventListener('DOMContentLoaded', function () {
    const messageEle = document.getElementById('toot');
    const counterEle = document.getElementById('counter');
    const maxLength = messageEle.getAttribute('maxlength');

    counterEle.innerHTML = '0/' + maxLength;

    messageEle.addEventListener('input', function (e) {
        const target = e.target;
        const currentLength = target.value.length;
        counterEle.innerHTML = currentLength + '/' + maxLength;
    });
});
</script>

</body>
</html>
