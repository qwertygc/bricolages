<?php
define('OPENACCESSBUTTON_API', '');
define('SCIHUB', '');
define('UNIV_RESOLVER', '');
define('ROOT', '');
function curl($url, $type='POST', $post=array(), $headers=array()) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec ($ch);
	curl_close($ch);
	return $response;
}
function is_doi($query) {
	$response = file_get_contents('https://api.crossref.org/works/'.$query);
	$response = json_decode($response, true);
	if($response['status'] == 'ok') {
		return true;
	}
	else {
		return false;
	}
}
function redirect($url) {
	//@header('Location: '.$url);
	echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	die;
}
function newtab($url) {
	//@header('Location: '.$url);
	echo '<script>window.open("'.$url.'");</script>';
}
if(!file_exists('.htaccess')) {
	file_put_contents('.htaccess', '<IfModule mod_rewrite.c> 
	RewriteEngine On
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule ^(.*)$  index.php?article=$1 [L]
	# RewriteCond %{HTTPS} off
	# RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
</IfModule>');

}
if(isset($_GET['article']) AND $_GET['article'] != '') {
	$_GET['article'] = trim($_GET['article']);
	// 10.1177/0759106318762455 10.1080/02680939.2012.747109
	$oab = json_decode(curl('https://api.openaccessbutton.org/availability?url='.$_GET['article'], 'GET', '', array('Content-Type: application/json', 'x-apikey: '.OPENACCESSBUTTON_API)), true);
	$dissemin = json_decode('https://dissem.in/api/'.$_GET['article'], true);

	if(filter_var($_GET['article'], FILTER_VALIDATE_URL)) {
		redirect(UNIV_RESOLVER.$_GET['article']);
	}
	elseif(is_doi($_GET['article'])) {
		if(isset($oab['data']['meta']['article']['redirect'])) {
			echo $oab['data']['meta']['article']['redirect'];
			redirect($oab['data']['meta']['article']['redirect']);
		}
		elseif($dissemin['status'] == 'ok') {
			echo $dissemin['paper']['pdf_url'];
			redirect($dissemin['paper']['pdf_url']);
		}
		else {
			redirect(SCIHUB.$_GET['article']);
		}
	}
	else {
		newtab('https://rechercheisidore.fr/search/?q='.$_GET['article']);
		newtab('https://hal.archives-ouvertes.fr/search/?q='.$_GET['article']);
		newtab('https://books.openedition.org/catalogue?q='.$_GET['article']);
		newtab('https://doabooks.org/doab?func=search&uiLanguage=en&template=&query='.$_GET['article']);
		newtab('http://www.oapen.org/search?text='.$_GET['article']);
		newtab('http://www.opengrey.eu/search/request?q='.$_GET['article']);
		newtab('https://dumas.ccsd.cnrs.fr/search/index/?q='.$_GET['article']);
		newtab('http://www.theses.fr/?q='.$_GET['article']);
		newtab('https://tel.archives-ouvertes.fr/search/index/?q='.$_GET['article']);
		newtab('http://www.dart-europe.eu/basic-results.php?kw[]='.$_GET['article']);
		newtab('https://oatd.org/oatd/search?q='.$_GET['article']);
		newtab('http://www.bictel.be/Search/Home?lookfor='.$_GET['article']);
		newtab('http://www.ndltd.org/system/app/pages/search?scope=search-site&q='.$_GET['article']);
		newtab('https://gallica.bnf.fr/services/engine/search/sru?operation=searchRetrieve&version=1.2&query=%28gallica%20all%20%22'.$_GET['article'].'%22%29&lang=fr&suggest=0');
		newtab('https://scholar.google.fr/scholar?q='.$_GET['article']);
		newtab('https://www.istex.fr/rechercher/?q='.$_GET['article']);
		newtab('https://openedition.org/catalogue-journals?searchdomain=catalogue-journals&q='.$_GET['article']);
		newtab('https://www.cairn.info/resultats_recherche.php?searchTerm='.$_GET['article']);
		newtab('http://www.persee.fr/search?ta=article&q='.$_GET['article']);
		newtab('http://signal.sciencespo-lyon.fr/article/searchSimple?titre='.$_GET['article']);
		newtab('http://www.sudoc.abes.fr//DB=2.1/SET=3/TTL=1/CMD?ACT=SRCHA&IKT=1016&SRT=RLV&TRM='.$_GET['article']);
		newtab('https://doaj.org/search?source=%7B%22query%22%3A%7B%22query_string%22%3A%7B%22query%22%3A%22'.$_GET['article'].'%22%2C%22default_operator%22%3A%22AND%22%7D%7D%2C%22from%22%3A0%2C%22size%22%3A10%7D');
		newtab('https://www.erudit.org/fr/recherche/?basic_search_term='.$_GET['article']);
		redirect(ROOT);
	}
}
?>
<!doctype html>
<html lang="fr">
<head>
<style>
body {
	width:75%;
	margin:auto;
}
input[type="text"], input[type="url"] {
	width:70%;
	font-size:1.5em;
	margin:auto;
}
input[type="submit"] {
	font-size:1.5em;
}
</style>
</head>
<body>
	<form method="get" action="index.php">
			<label for="article"><input type="text" name="article" id="article" placeholder="article"/></label>
			<input type="submit" value="&#10140;"/>
		</form>
</body>
</html>
