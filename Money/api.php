<?php
include 'app/inc/system.php';
if(isset($_GET['action'])){
	switch ($_GET['action']){
		case 'output':
			if($_GET['subaction'] == 'transaction') {
				if(isset($_GET['key'])) {
					$retour_id = $bdd->prepare('SELECT id FROM user WHERE keyid=?');
					$retour_id->execute(array($_GET['key']));
					$data_id = $retour_id->fetch();
					$retour = $bdd->prepare('SELECT t.id, t.value, t.statut, t.text, t.time, t.id AS t_id, u_from.pseudo AS from_pseudo, u_to.pseudo AS to_pseudo
						FROM transactions t
						LEFT JOIN user u_from ON t.t_from = u_from.id
						LEFT JOIN user u_to ON t.t_to = u_to.id
						WHERE (t.t_from=? OR t.t_to=?)
						ORDER BY t.time DESC');
					$retour->execute(array($data_id['id'],$data_id['id']));
					$retour->execute();
					$array = array();
					while($data = $retour->fetchAll(PDO::FETCH_CLASS)) {
						echo json_encode($data);
					}
				}
			}
			if($_GET['subaction'] == 'user') {
				$retour = $bdd->prepare('SELECT id, pseudo, wallet, keyid FROM user WHERE keyid=?');
				$retour->execute(array($_GET['key']));
				$data = $retour->fetchAll(PDO::FETCH_CLASS);
				echo json_encode($data);
			}
			if($_GET['subaction'] == 'market') {
				$retour = $bdd->query('SELECT m.*, u.pseudo FROM market m, user u WHERE m.id_user = u.id ORDER BY time');
				$data = $retour->fetchAll(PDO::FETCH_CLASS);
				echo json_encode($data);
			}
		break;
		case 'input':
			if($_GET['subaction'] == 'transaction') {
				if(isset($_GET['key'])) {
					$retour = $bdd->prepare('SELECT id, wallet FROM user WHERE keyid=?');
					$retour->execute(array($_GET['key']));
					$data = $retour->fetch();
					if($data['wallet'] < 0) {
						echo 'overdraft';
					}
					else {
						$value = (isset($_GET['value'])) ? $_GET['value'] : '0';
						$text = (isset($_GET['text'])) ? strip_tags($_GET['text']) : '';
						$to = (isset($_GET['to'])) ? $_GET['to'] : '0';
						sql('INSERT INTO transactions(t_from, t_to, value, text, time, statut) VALUES(?, ?, ?, ?, ?, ?)', array($data['id'],$to,$value, $text, time(), 0));
						$array = array();
						$array['id'] = $data['id'];
						$array['to'] = $to;
						$array['value'] = $value;
						$array['text'] = $text;
						echo json_encode($array);
					}
					
				}
				else {echo "not key, not chocolat";}
			}
		if($_GET['subaction'] == 'user') {
			$retour = $bdd->prepare('SELECT email FROM user WHERE email = :email');
			$retour->execute(array('email'=> $_GET['email']));
			$member_exist=$retour->fetch();
			if($member_exist > 0) {
				$retour = $bdd->prepare('SELECT * FROM user WHERE keyid=?');
				$retour->execute(array($_GET['key']));
				$data = $retour->fetch();
				$pseudo = (isset($_GET['pseudo'])) ? $_GET['pseudo'] : $data['pseudo'];
				$password = (isset($_GET['password'])) ? hash('sha512', $_GET['password']) : $data['password'];
				sql('UPDATE user SET password=:password AND pseudo=:pseudo WHERE id=:id', array('password'=>hash('sha512', $_GET['password']), 'pseudo'=>$pseudo, 'id'=>$data['id']));
				$array = array();
				$array['id'] = $data['id'];
				$array['pseudo'] = $pseudo;
				$array['password'] = $password;
				$array['keyid'] = $data['keyid'];
				echo json_encode($array);
			}
			else {
				$mail = explode("@", $_GET['email']);
				$uniqid = uniqid();
				sql('INSERT INTO user(email, password, pseudo, wallet, keyid) VALUES(?, ?, ?, 5, ?)', array($_GET['email'],hash('sha512', $_GET['password']),  $mail[0], $uniqid));
				$array = array();
				$array['email'] = $_GET['email'];
				$array['password'] = hash('sha512', $_GET['password']);
				$array['keyid'] = $uniqid;
				echo json_encode($array);
			}
		}
		break;
		case 'admin':
			if($_GET['subaction'] == 'rdb' && $_GET['pwd'] == 'sinople') {
				// $bdd->query('UPDATE user SET wallet = wallet +1');
				$retour_r = $bdd->query('SELECT wallet from user WHERE id=0');
				$data_r = $retour_r->fetch();
				$retour = $bdd->prepare('SELECT id FROM user');
				$retour->execute();
				$data = $retour->fetchAll();
				foreach($data as $d) {
					if($d['id'] != 0) {
						if($data_r['wallet'] >0) {
							transaction(0, $d['id'], 1, false, "Versement du revenu de base");
						}
						else {
							transaction(0, $d['id'], 0, false, "Versement du revenu de base");
						}
					}
				}
				echo "Basic income";
			}
		break;
	}
}

else {
?>
<pre>
VIEW TRANSACTIONS : ?action=output&subaction=transaction&key=<b><u>YOUR KEY API</u></b>
WRITE TRANSACTION : ?action=input&subaction=transaction=key=<b><u>YOUR KEY API</u></b>&value=<b>VALUE OF TRANSACTION</b>&to=<b>ID OF USER TO SEND MONEY</b>&text=<b>THE TEXT OF TRANSACTION</b>
VIEW USER PROFILE : ?action=output&subaction=user&key=<b><u>YOUR KEY API</u></b>
WRITE NEW'S USER PROFILE : ?action=input&subaction=user&email=<b><u>YOUR E-MAIL</u></b>&password=<b><u>YOUR PASSWORD</u></b>
WRITE USER PROFILE : ?action=input&subaction=user&password=<b>YOUR PASSWORD</b>&pseudo=<b>YOUR PSEUDO</b>&key=<b><u>YOUR KEY API</u></b>
VIEW WHOLE MARKET : ?action=output&subaction=market
</pre>
<?php
}