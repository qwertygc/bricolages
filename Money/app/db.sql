CREATE TABLE IF NOT EXISTS transactions (
  id int(11) NOT NULL AUTO_INCREMENT,
  t_from int(11) NOT NULL,
  t_to int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `text` text NOT NULL,
  `time` varchar(255) NOT NULL,
  statut int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `user` (
  id int(11) NOT NULL AUTO_INCREMENT,
  email text NOT NULL,
  pseudo text NOT NULL,
  `password` text NOT NULL,
  wallet int(11) DEFAULT NULL,
  keyid varchar(255) NOT NULL,
  UNIQUE KEY id (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `market` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;
