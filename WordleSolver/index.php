<?php
error_reporting(-1);
function toslug($string) {
    $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '', ' ' => ''
    );
    $stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string);
    return strtolower(strtr($string, $table));
}

function check_format_word2($string, $pattern, $exclude_letters, $include_letters) {
	$pattern = str_replace('?', '.', $pattern);
	if($pattern != '') {
		if($pattern !='') {$search=$pattern;} else {$search='';}
		if($exclude_letters !='') {$exclude='(?!.*['.$exclude_letters.'])';} else {$exclude='';}
		if($include_letters !='') {
            $include = '';
			$letter_by_letter = str_split($include_letters);
			foreach($letter_by_letter as $uniq_letters) {
				$include .= '(?=.*'.$uniq_letters.')';
                //$include .= '('.$uniq_letters.')';
			} 
		} 
		else {
			$include='';
		}
		preg_match('/^'.$exclude.$include.$search.'$/', $string, $matches);
		if(!empty($matches)) {
			return $string;
		}
		else {
			return '';
		}
	}
	else {
		return $string;
	}
}
#####################
if(!file_exists('dico-fr.json')) { file_put_contents('dico-fr.json', file_get_contents('https://raw.githubusercontent.com/words/an-array-of-french-words/master/index.json'));}
if(!file_exists('dico-en.json')) { file_put_contents('dico-en.json', file_get_contents('https://raw.githubusercontent.com/words/an-array-of-english-words/master/index.json'));}

?>
<!doctype html>
<html>
	<head>
		<title>Liste de mots</title>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
	</head>
	<body>
		<header>
			<h1>🟩🟨🟦 Liste de mots</h1>
			<p><a href="index.php">Réinitialiser</a></p>
		</header>
		<main>
			<?php
				$_GET['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'fr';
				$_GET['ok_letter'] = isset($_GET['ok_letter']) ? $_GET['ok_letter'] : '';
				$_GET['nok_letter'] = isset($_GET['nok_letter']) ? $_GET['nok_letter'] : '';
				$_GET['format_word'] = isset($_GET['format_word']) ? $_GET['format_word'] : '';
				$resultats = '';
				$all_dico = [
					'fr' => 'dico-fr.json',
					'en' => 'dico-en.json'
				];
				$dico = json_decode(file_get_contents($all_dico[$_GET['lang']]), true);
				$count = 0;
				foreach($dico as $mot) {
					$mot = toslug($mot);
					$mot_possible = check_format_word2($mot, toslug($_GET['format_word']), $_GET['nok_letter'], $_GET['ok_letter']);
					if($mot_possible != '') {
						$resultats .= $mot_possible.' ';
						$count++;
					}
				 }
			?>
			<p>
				<ul>
					<li>En anglais : <a href="https://www.powerlanguage.co.uk/wordle/">Wordle</a></li>
					<li>En français : <a href="https://www.solitaire-play.com/lemot/">Le mot</a> <a href="https://sutom.nocle.fr/">SUTOM</a> <a href="https://wordle.louan.me/">Wordle-FR</a></li>
				</ul>
			</p>
			<form method="get" action="index.php">
			<label for="lang">Choix de la langue</label>
			<select name="lang">
				<option value="fr" <?=($_GET['lang']=='fr' ? 'selected' : '');?>>Français</option>
				<option value="en" <?=($_GET['lang']=='en' ? 'selected' : '');?>>Anglais</option>
			</select>
			<label for="ok_letter">Contenant les lettres</label> <input type="text" name="ok_letter" id="ok_letter" value="<?=$_GET['ok_letter'];?>"/>
			<label for="nok_letter">Ne contenant pas les lettres</label> <input type="text" name="nok_letter" id="nok_letter" value="<?=$_GET['nok_letter'];?>"/>
			<label for="format_word">Recherche de mots (utiliser <kbd>?</kbd> pour les lettres inconnues)</label> <input type="text" name="format_word" id="format_word" value="<?=$_GET['format_word'];?>"/>
			<input type="submit">
			</form>
			<p><?php echo 'Il y a '.$count.' résultat(s).'; ?></p>
			<samp><?php echo $resultats; ?></samp>
		</main>
		<footer>
			<p><a href="https://github.com/words/">dictionnaires</a></p>
			</footer>
	</body>
</html>

